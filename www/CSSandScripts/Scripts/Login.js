﻿var Login = {

    FirstLoginTime: true,
    Username: "",
    Password: '',
    PinCode: '',
    NeedBusinessAfterLogin: false,
    Businessfunction: '',
    IsChangePassword: false,
    ResetFromEmail: true,

    LoadLoginPage: function () {

        if (localStorage.getItem("Username") == undefined || localStorage.getItem("Username") == null) {
            LoadPageContent("Login", 'Login.HandleLoginDocReady()', true, true, true);
            Login.FirstLoginTime = true;
        }
        else {
            LoadPageContent("Login2", 'Login.HandleLogin2DocReady()', true, true, true);
            Login.FirstLoginTime = false;
        }
    },

    HandleLoginDocReady: function () {
        SetHeaderTitle("Login", "الدخول");

        $("#Login_Username").val("");
        $("#Login_Password").val("");

        BindEvents('#Login_LoginBtn', 'click', 'Login.HandleLoginBtnClicked()');
        BindEvents('#Login_ForgetPassword', 'click', 'Login.ForgetPasswordBtnClicked()');
        BindEvents('#Login_ChangePassword', 'click', 'Login.ChangePasswordBtnClicked()');
    },

    ForgetPasswordBtnClicked: function () {
        Login.IsChangePassword = false;
        LoadPageContent("Login4", 'Login.HandleLogin4DocReady()', true, true, true, false, 'LoadPageContent("Login", "Login.HandleLoginDocReady()", true, true, true)');
    },

    ChangePasswordBtnClicked: function () {
        Login.IsChangePassword = true;
        LoadPageContent("Login4", 'Login.HandleLogin4DocReady()', true, true, true, false, 'LoadPageContent("Login", "Login.HandleLoginDocReady()", true, true, true)');
    },

    HandleLogin4DocReady: function () {

        $(".Login4Inputs").hide();

        $('#Login4_NewPassword').pStrength({
            'changeBackground': false,
            'onPasswordStrengthChanged': function (passwordStrength, strengthPercentage) {
                if ($(this).val()) {
                    $.fn.pStrength('changeBackground', this, passwordStrength);
                } else {
                    $.fn.pStrength('resetStyle', this);
                }
                $('#' + $(this).data('display')).html(strengthPercentage + '%');
            },
            'onValidatePassword': function (strengthPercentage) {
                $('#' + $(this).data('display')).html(
                    $('#' + $(this).data('display')).html()
                    );
            }
        });

        if (Login.IsChangePassword == true) {
            $(".ChangePassword").show();
            SetHeaderTitle("Change Password", "تغيير كلمة المرور");
            BindEvents('#Login4_ConfirmChangePassword', 'click', 'Login.HandleChangePassword()');
        }
        else {
            $(".ForgetPassword").show();
            SetHeaderTitle("Forget Password", "إسترجاع كلمة المرور");
            BindEvents('#Login4_ResetPassword', 'click', 'Login.HandleResetPassword()');
        }
    },

    HandleChangePassword: function () {
        try {

            if ($.trim($("#Login4_Username").val()) == "") {
                AlertFunction("Please enter Username", "من فضلك ادخل اسم المستخدم", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($("#Login4_OldPassword").val() == "") {
                AlertFunction("Please enter Old Password", "من فضلك ادخل كلمة المرور الحالية", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($("#Login4_NewPassword").val() == "") {
                AlertFunction("Please enter New Password", "من فضلك ادخل كلمة المرور الجديدة", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($("#Login4_NewPasswordConfirmation").val() == "") {
                AlertFunction("Please enter Password Confirmation", "من فضلك ادخل تأكيد كلمة المرور الجديدة", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($("#Login4_NewPasswordConfirmation").val() != $("#Login4_NewPassword").val()) {
                AlertFunction("Sorry Password not matched", "عفوا كلمة المرور غير متطابقة", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (ValidatePassLength($.trim($("#Login4_NewPassword").val()))) {
                AlertFunction("Password must be more than 7 charachter", "يجب ان تكون كلمه المرور اكبر من 7 حروف", "Error", "خطأ", "OK", "موافق");
                return;
            }

            var URL = Globals.ServicesURI_Test + "update/gpssauser/changepassword/language/#Lang#";
            URL = URL.replace("#Lang#", Language);
            var ReqObj = ChangePasswordClass;
            ReqObj.UserName = $("#Login4_Username").val();
            ReqObj.OldPassword = $("#Login4_OldPassword").val();
            ReqObj.NewPassword = $("#Login4_NewPassword").val();
            var data = JSON.stringify(ReqObj);
            CallWebServiceWithOauth(URL, 'Login.HandleResetAndChangePasswordCallSuccess(response)', "", data, "POST", true, true);
        } catch (e) {

        }
    },

    HandleResetPassword: function () {
        try {

            var Val = $("#Login4_ContactMethod").val();
            var email = $("#Login4_email").val();

            if (Val == -1) {
                AlertFunction("Please Select Contact Method", "من فضلك ادخل طريقة التواصل", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim(email) == "") {
                AlertFunction("Please enter Email", "من فضلك ادخل البريد الإلكترونى", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (!ValidateEmail(email)) {
                AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صحيح", "Error", "خطأ", "OK", "موافق");
                return;
            }

            //var URL = Globals.ServicesURI_Test + "update/resetpassword/email/#email#/preferedcommunication/#Val#/language/#Lang#";
            // var URL = "http://mgovws.gpssa.ae:8080/SPREST.svc/update/resetpassword/language/#Lang#";
            var URL = Globals.ServicesURI_Test+"update/resetpassword/language/#Lang#";
            var data = {
                Email: email,
                Preferedcommunication:Val
            };

            URL = URL.replace("#Lang#", Language);
            var ReqObj = JSON.stringify(data);

            //URL = URL.replace("#Lang#", Language).replace("#email#", email).replace("#Val#", Val);
            CallWebServiceWithOauth(URL, "Login.HandleResetAndChangePasswordCallSuccess(response)", "", ReqObj, "POST", true, true);
        } catch (e) {

        }
    },

    HandleResetAndChangePasswordCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AlertFunction(response.Message.Body, response.Message.Body, "Success", "نجاح", "OK", "موافق");
                try {
                    $("#Login4_email").val("");
                    $("#Login4_ContactMethod").val(-1).trigger("change");
                } catch (e) {

                }

                try {
                    $("#Login4_Username").val("");
                    $("#Login4_OldPassword").val("");
                    $("#Login4_NewPassword").val("");
                    $("#Login4_NewPasswordConfirmation").val("");
                    $("#Login4_PasswordStrength").html();
                } catch (e) {

                }
            }
            else {
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    HandleLoginBtnClicked: function () {
        try {

            if ($.trim($("#Login_Username").val()) == "") {
                AlertFunction("Please enter Username", "من فضلك ادخل اسم المستخدم", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#Login_Password").val()) == "") {
                AlertFunction("Please enter Password", "من فضلك ادخل كلمة المرور", "Error", "خطأ", "OK", "موافق");
                return;
            }

            Login.Username = $("#Login_Username").val();
            Login.Password = $("#Login_Password").val();

            //var URL = Globals.ServicesURI_Test + "validate/gpssauser/username/#Username#/password/#Pass#/pincode/@@/language/#Lang#";
            // var URL = "http://mgovws.gpssa.ae:8080/SPREST.svc/validate/gpssauser/language/#Lang#";
            var URL = Globals.ServicesURI_Test+"validate/gpssauser/language/#Lang#";
            //URL = URL.replace("#Username#", Login.Username).replace("#Pass#", Login.Password).replace("#Lang#", Language);
            URL = URL.replace("#Lang#", Language);
            var Obj = UserValidation.constructor();
            Obj.UserName = Login.Username;
            Obj.Password = Login.Password;
            Obj.PinCode = "@@";
            var ReqObj = JSON.stringify(Obj);
            CallWebService(URL, "Login.HandleLoginSuccess(response)", "", ReqObj, "POST", true, true);

        } catch (e) {

        }

    },

    HandleLoginSuccess: function (response) {
        try {

            if (response != null) {
                var ResponseObj = response;
                var RespObj = response;//JSON.parse(response);

                if (RespObj.Message.Code == 0) {
                    Globals.LoginRspObj = RespObj;
                    Globals.EmployerRowID = RespObj.GPSSAUser.EmployerRowId;
                    Globals.ContactRowId = RespObj.GPSSAUser.ContactRowId;
                    Globals.UserName = RespObj.GPSSAUser.UserName;
                    Globals.FullName = RespObj.GPSSAUser.FullName;


                  //case loginSucess set text to username from Globals (fix tamer oldusername issue)
                  $("#Login2_Username").html(Globals.UserName);


                  Login.LoadLogin2Page();

                    var SecretKey = "Mohamedfekry007@gmail.com";//jsinterface.GetLookup("4");

                    // var EncryptedUsername = CryptoJS.AES.encrypt(Login.Username, SecretKey);
                    // var EncryptedPassword = CryptoJS.AES.encrypt(Login.Password, SecretKey);
                    var EncryptedUsername = CryptoJS.AES.encrypt(Globals.UserName, SecretKey);
                    var EncryptedPassword = CryptoJS.AES.encrypt(Login.Password, SecretKey);

                    localStorage.removeItem("Username");
                    localStorage.removeItem("Password");

                    localStorage.setItem("Username", EncryptedUsername);
                    localStorage.setItem("Password", EncryptedPassword);

                    var LoginObj = CryptoJS.AES.encrypt(ResponseObj, SecretKey);
                    localStorage.setItem("LoginObj", LoginObj);



                }
                else {
                    AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }

        } catch (e) {

        }
    },

    LoadLogin2Page: function () {
        LoadPageContent("Login2", 'Login.HandleLogin2DocReady()', true, true, true, false, 'LoadPageContent("Login", "Login.HandleLoginDocReady()", true, true, true)');
    },

    HandleLogin2DocReady: function () {

        SetHeaderTitle("Login", "الدخول");
        // Login.Username=null;
        BindEvents('#Login2_LoginBtn', 'click', 'Login.HandleBtnLogin2Clicked()');
        BindEvents('#Login2_LoginWithAnotherUsername', 'click', 'Login.LoginWithAnotherUserClicked()');
        BindEvents('#Login2_ForgetPin', 'click', 'Login.LoginWithAnotherUserClicked()');
        BindEvents('#Login2_ChangePassword', 'click', 'Login.ChangePasswordBtnClicked()');
        if (Login.FirstLoginTime == true) {
            $("#LoginFirstTimeTxt").show();
            $("#Login2_ForgetPin").hide();

        }
        else {
            $("#LoginFirstTimeTxt").hide();
            $("#Login2_ForgetPin").show();
        }

        var SecretKey = "Mohamedfekry007@gmail.com";//jsinterface.GetLookup("4");

        var DecryptedUsername = CryptoJS.AES.decrypt(localStorage.getItem("Username"), SecretKey);
        var DecryptedPass = CryptoJS.AES.decrypt(localStorage.getItem("Password"), SecretKey);
        Login.Username = DecryptedUsername.toString(CryptoJS.enc.Utf8);
        
        Login.Password = DecryptedPass.toString(CryptoJS.enc.Utf8);

        $("#Login2_Username").html("");

        $("#Login2_Username").html(Login.Username);
    },

    LoginWithAnotherUserClicked: function () {
        LoadPageContent("Login", 'Login.HandleLoginDocReady()', true, true);
        Login.FirstLoginTime = true;
        CleanPage('Login,Login2');
        localStorage.removeItem("Username");
        localStorage.removeItem("Password");
        localStorage.removeItem("LoginObj");
        localStorage.removeItem("UserServicesArr");
        Globals.UserServicesArr = null;
        Globals.UserLoggedIn = false;
    },

    HandleBtnLogin2Clicked: function () {

        if ($.trim($("#Login_PinCode").val()) == "") {
            AlertFunction("Please enter Pin Code", "من فضلك ادخل الرقم السرى", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            if (Login.FirstLoginTime == true) {
                var ReqObj = Globals.LoginRspObj.GPSSAUser;
                ReqObj.PinCode = $("#Login_PinCode").val();
                var URL = Globals.ServicesURI_Test + "update/gpssauser/language/#Lang#";
                URL = URL.replace("#Lang#", Language);
                var data = JSON.stringify(ReqObj);

                CallWebService(URL, 'Login.HandleLogin2Success(response)', "", data, "POST", true, false);
            }
            else {
                //var URL = Globals.ServicesURI_Test + "validate/gpssauser/username/#Username#/password/#Pass#/pincode/@@/language/#Lang#";
                // var URL = "http://mgovws.gpssa.ae:8080/SPREST.svc/validate/gpssauser/language/#Lang#";
                var URL = Globals.ServicesURI_Test+"validate/gpssauser/language/#Lang#";
                //                URL = URL.replace("#Username#", Login.Username).replace("#Pass#", Login.Password).replace("#Lang#", Language);
                URL = URL.replace("#Lang#", Language);
                var Obj = UserValidation.constructor();
                Obj.UserName = Login.Username;
                Obj.Password = Login.Password;
                Obj.PinCode = $("#Login_PinCode").val();
                var ReqObj = JSON.stringify(Obj);
                CallWebService(URL, "Login.HandleLoginatStep2Success(response)", "", ReqObj, "POST", true, false);
            }
        }
    },

    HandleLoginatStep2Success: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                Globals.LoginRspObj = RespObj;
                Globals.EmployerRowID = RespObj.GPSSAUser.EmployerRowId;
                Globals.ContactRowId = RespObj.GPSSAUser.ContactRowId;
                Globals.UserName = RespObj.GPSSAUser.UserName;
                Globals.FullName = RespObj.GPSSAUser.FullName;
                //var URL = Globals.ServicesURI_Test + "validate/gpssauser/username/#Username#/password/#Pass#/pincode/#Pin#/language/#Lang#";
                // var URL = "http://mgovws.gpssa.ae:8080/SPREST.svc/validate/gpssauser/language/#Lang#";
                var URL = Globals.ServicesURI_Test+"validate/gpssauser/language/#Lang#";
                var Obj = UserValidation.constructor();
                Obj.UserName = Login.Username;
                Obj.Password = Login.Password;
                Obj.PinCode = $("#Login_PinCode").val();
                var ReqObj = JSON.stringify(Obj);
                //URL = URL.replace("#Username#", Login.Username).replace("#Pass#", Login.Password).replace("#Lang#", Language).replace("#Pin#", $("#Login_PinCode").val());
                URL = URL.replace("#Lang#", Language);
                CallWebService(URL, "Login.HandleLogin2Success(response)", "", ReqObj, "POST", false, false);
            }
            else {
                AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                return;
            }
        }
    },

    HandleLogin2Success: function (response) {
        try {
            if (response != null) {

                var RespObj;

                if (Login.FirstLoginTime == true) {
                    if (response.Message.Code == 0) {
                        Globals.LoginRspObj.GPSSAUser.PinCode = $("#Login_PinCode").val();
                        $("#Login_PinCode").val("");
                        if (Globals.LoginRspObj.GPSSAUser.Roles.length == 1) {
                            Globals.UserType = Globals.LoginRspObj.GPSSAUser.Roles[0];
                            Globals.LoginRspObj.Role = Globals.UserType;
                            var ReqObj = Globals.LoginRspObj;
                            var URL = Globals.ServicesURI_Test + "update/gpssauser/assign/authorizedservices/language/#Lang#";
                            URL = URL.replace("#Lang#", Language);
                            var data = JSON.stringify(ReqObj);
                            CallWebService(URL, 'Login.HandleLogin3CallSuccess(response)', "", data, "POST", true, false);
                        }
                        else {
                            Login.LoadLogin3Page();
                        }
                    }
                    else {
                        AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                    }
                }
                else {

                    RespObj = response;//JSON.parse(response);

                    if (RespObj.Message.Code == 0) {
                        Globals.LoginRspObj.GPSSAUser.PinCode = $("#Login_PinCode").val();
                        $("#Login_PinCode").val("");

                        if (Globals.LoginRspObj.GPSSAUser.Roles.length == 1) {
                            Globals.UserType = Globals.LoginRspObj.GPSSAUser.Roles[0];
                            Globals.LoginRspObj.Role = Globals.UserType;
                            var ReqObj = Globals.LoginRspObj;
                            var URL = Globals.ServicesURI_Test + "update/gpssauser/assign/authorizedservices/language/#Lang#";
                            URL = URL.replace("#Lang#", Language);
                            var data = JSON.stringify(ReqObj);
                            CallWebService(URL, 'Login.HandleLogin3CallSuccess(response)', "", data, "POST", true, false);
                        }
                        else {
                            Login.LoadLogin3Page();
                        }
                    }
                    else {
                        AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                    }
                }
            }
        } catch (e) {
        }
    },

    LoadLogin3Page: function () {
        LoadPageContent("Login3", 'Login.HandleLogin3DocReady()', true, true, true, false, 'Login.LoadLogin2Page()');
    },

    HandleLogin3DocReady: function () {

        var temp = "<option>#Val#</option>";
        var AllHTML = "";

        for (var i = 0; i < Globals.LoginRspObj.GPSSAUser.Roles.length; i++) {
            AllHTML += temp.replace("#Val#", Globals.LoginRspObj.GPSSAUser.Roles[i]);
            temp = "<option>#Val#</option>";
        }

        $("#Login3_RolesDropDown").html("");
        if (Language == "en") {
            $("#Login3_RolesDropDown").html("<option value='-1'>Select Role</option>");
        }
        else {
            $("#Login3_RolesDropDown").html("<option value='-1'>اختر دور</option>");
        }
        $("#Login3_RolesDropDown").append(AllHTML);

        BindEvents('#Login3_ContinueBtn', 'click', 'Login.HandleBtnContinueLogin3Clicked()');
    },

    HandleBtnContinueLogin3Clicked: function () {

        var val = $("#Login3_RolesDropDown").val();

        if (val == "-1") {
            AlertFunction("Please select role", "من فضلك اختر دور", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            Globals.UserType = val;
            Globals.LoginRspObj.Role = val;
            var ReqObj = Globals.LoginRspObj;
            var URL = Globals.ServicesURI_Test + "update/gpssauser/assign/authorizedservices/language/#Lang#";
            URL = URL.replace("#Lang#", Language);
            var data = JSON.stringify(ReqObj);
            CallWebService(URL, 'Login.HandleLogin3CallSuccess(response)', "", data, "POST", true, true);
        }
    },

    HandleLogin3CallSuccess: function (response) {

        if (response != null) {
            if (response.Message.Code == 0) {
                localStorage.setItem("UserLoggedInLocalStorage", true);

                Globals.UserLoggedIn = true;
                Globals.UserServicesArr = response.GPSSAUser;
                localStorage.setItem("UserServicesArr", JSON.stringify(Globals.UserServicesArr));
                $("#LoginTR").hide();
                $("#LogOutTR").show();
                if (!Login.NeedBusinessAfterLogin)
                    ServicesList.LoadServicesPage();
                    //LoadPageContent("StartUp", 'StartUp.HandleStartUpDocReady()');
                    else
                        if (Login.Businessfunction) Login.Businessfunction();

                    Login.NeedBusinessAfterLogin = false;
                }
                else {
                    AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
        }
    };
