﻿var ComplaintsModule = {
    CurrentUUID: '',
    didUpload: false,
    uploadedFileName: null,

    LoadComplaints: function (isFromServices) {
        // $("#NewsDetails_Content").scrollTop(0) ;
        // $("#NewsDetails").scrollTop(0);
        // $("#News").scrollTop(0) ;
        // $("#ALLPageContent").scrollTop(0) ;
        // $("#Complaints").scrollTop(0) ;

        if (isFromServices == true) {
            LoadPageContent("Complaints", 'ComplaintsModule.HandleComplaintsDocReady()', true, true, true, false, 'ServicesList.LoadServicesPage()');
        } else {
            LoadPageContent("Complaints", 'ComplaintsModule.HandleComplaintsDocReady()', true, true, true, true);
        }
    },
    resetCountsandVars: function () {

        camera_success_callback = "ComplaintsModule.HandleCameraUploadSuccess(name,r)";
        camera_fail_callback = "ComplaintsModule.HandleCameraUploadFail()";
        ComplaintsModule.didUpload = false;
        $("#Complaints_CheckMark").css("display", "none");
        ComplaintsModule.uploadedFileName=null;

    },
    HandleComplaintsDocReady: function () {


        //set cam upload n fail funs
        ComplaintsModule.resetCountsandVars();
        //set cam upload n fail funs


        //alert("HandleComplaintsDocReady");
        SetHeaderTitle("Complaints and Feedack", "شكاوي و مقترحات");
        BindEvents('#SendComplaintsBtn', 'click', 'ComplaintsModule.HandleComplaintsSendBtnClicked()');
        BindEvents('#UploadComplaintsBtn', 'click', 'ComplaintsModule.HandleComplaintsUploadBtnClicked()');
        BindEvents('#ComplaintsTypeDropDown', 'change', 'ComplaintsModule.HandleComplainTypesDropDownChanged()');
        $("#ComplaintsTypeDropDown").val('-1').trigger('change');
        $("#ComplaintsSubTypeDropDown_Complaint").val('-1').trigger('change');
        $("#ComplaintsSubTypeDropDown_Complaint").show();
        $("#ComplaintsSubTypeDropDown_Feedback").hide();
        $("#ComplaintsContent").val('');
        AddCountryCodesOptionsToDropDown("Complaints_Mobile_CountryCodeDDL");

        if (Globals.UserLoggedIn) {
            var FullName = Globals.LoginRspObj.GPSSAUser.FullName;
            var Mobile = SetInitialPhoneValue(Globals.LoginRspObj.GPSSAUser.Mobile);
            var Email = Globals.LoginRspObj.GPSSAUser.Email;
            $('#Complaints_FullName').val(FullName).prop('disabled', true);
            $('#Complaints_Mobile_CountryCodeDDL').prop('disabled', true);
            SetInitialPhoneNoValue("Complaints_Mobile_CountryCodeDDL", "Complaints_Mobile", Mobile);
            $('#Complaints_Mobile').prop('disabled', true);
            $('#Complaints_Mobile_Perfix').val(Mobile).prop('disabled', true);
            $('#Complaints_Email').val(Email).prop('disabled', true);
        } else {
            $('#Complaints_FullName').val('').prop('disabled', false);
            $('#Complaints_Mobile').val('').prop('disabled', false);
            $("#Complaints_Mobile_CountryCodeDDL").val("+971").trigger('change');
            $('#Complaints_Mobile_CountryCodeDDL').prop('disabled', false);
            $("#Complaints_Mobile").val('');
            $('#Complaints_Email').val('').prop('disabled', false);
        }
        var cordova = window.PhoneGap || window.Cordova || window.cordova;
        //if (cordova) { alert("cordova"); cordova.exec(function(data) {}, function(data) {}, 'FileChooser', 'open', [{}]);}

        if (device.platform.toLowerCase() === 'android' && (device.version.indexOf('4.4.2') === 0 || device.version.indexOf('4.4.3') === 0)) {
            //alert("Yes I'm KitKat");

            if(!complaints_filechooser_event_bound){
                complaints_filechooser_event_bound=true;
                $('#Complaints_upload').click(function (e) {

                // fileChooser.open(function(uri) {
                    fileChooser.pick("*", function (uri) {
                        theJSON = uri;

                        if (!validateExtension(uri.filename)) {
                            AlertFunction("Please upload following files (pdf,doc,docx,jpg,jpeg,png) only", "يجب تحميل الملفات التاليه (pdf,doc,docx,jpg,jpeg,png) فقط", "Error", "خطأ", "OK", "موافق");
                            return;
                        }
                        if (uri.size / 1024 / 1024 > 7) {
                            AlertFunction("Max Size for documents is 7 MB", "الحد الاقصى للملفات 7 م ب", "Error", "خطأ", "OK", "موافق");
                            return;
                        }

                        if ($("#Complaints_CheckMark").css("display") == "block") {
                            AlertFunction("File replaced", "لقد تم استبدال الملف", "warning", "تحذير", "OK", "موافق");
                        }



                        fileChooser_success_callback = "ComplaintsModule.HandleUploadDocSuccessForFileChooser(name,r)";
                        fileChooser_fail_callback = "ComplaintsModule.HandleUploadDocFail(name)";

                        onUploadFromFileChooser(uri, "");


                    });
});
}

}

        // Detect changes
        var uploader = document.getElementsByName('Complaints_upload');

        for (item in uploader) {
            // Detect changes
            uploader[item].onchange = function () {

                if ($("#Complaints_CheckMark").css("display") == "block") {
                    AlertFunction("File replaced", "لقد تم استبدال الملف", "warning", "تحذير", "OK", "موافق");
                }
                // $("#Complaints_CheckMark").css("display", "block");
                //upload on selection
                // ComplaintsModule.uploadDoc("Complaints_upload");

                uploadDoc("Complaints_upload","ComplaintsModule.HandleUploadDocSuccess(name,r)", null);


            }
        }

        if(!complaints_cam_event_bound){
            complaints_cam_event_bound=true;
        $('#Complaints_CamUpload').click(function () {
            if ($("#Complaints_CheckMark").css("display") == "block") {
                AlertFunction("File replaced", "لقد تم استبدال الملف", "warning", "تحذير", "OK", "موافق");
            }

            camera_success_callback = "ComplaintsModule.HandleCameraUploadSuccess(name,r)";
            camera_fail_callback = "ComplaintsModule.HandleCameraUploadFail()";

            var uuid = guid();            
            getPhoto(uuid);
            // return false;
        });

    }

    },



    HandleComplainTypesDropDownChanged: function () {

        var val = $("#ComplaintsTypeDropDown").val();

        if (val == -1) {
            $("#ComplaintsSubTypeDropDown_Complaint").show();
            $("#ComplaintsSubTypeDropDown_Feedback").hide();
        } else if (val == "Complaint") {
            $("#ComplaintsSubTypeDropDown_Complaint").show();
            $("#ComplaintsSubTypeDropDown_Feedback").hide();

            $('#ComplaintsSubTypeDropDown_Complaint').val('Complaint').trigger('change');
        } else {
            $("#ComplaintsSubTypeDropDown_Complaint").hide();
            $("#ComplaintsSubTypeDropDown_Feedback").show();
            $('#ComplaintsSubTypeDropDown_Feedback').val('Feedback').trigger('change');
        }
    },

    HandleComplaintsSendBtnClicked: function () {

        if ($("#ComplaintsTypeDropDown").val() == "-1") {
            AlertFunction("Please Select Type", "من فضلك اختر النوع", "Error", "خطأ", "OK", "موافق");
            return;
        } else if ($("#ComplaintsSubTypeDropDown_Complaint").val() == "-1" && $("#ComplaintsSubTypeDropDown_Feedback").val() == "-1") {
            AlertFunction("Please Select Sub-Type", "من فضلك اختر النوع الفرعى", "Error", "خطأ", "OK", "موافق");
            return;
        } else if ($.trim($("#Complaints_FullName").val()) == "") {
            AlertFunction("Please enter FullName", "من فضلك ادخل الاسم بالكامل", "Error", "خطأ", "OK", "موافق");
            return;
        } else if (GetFormatedPhoneNoValue("Complaints_Mobile_CountryCodeDDL", "Complaints_Mobile") == "") {
            AlertFunction("Please enter valid Mobile Number", "من فضلك ادخل هاتف محمول صحيح", "Error", "خطأ", "OK", "موافق");
            return;
        } else if ($.trim($("#Complaints_Email").val()) == "") {
            AlertFunction("Please enter E-mail", "من فضلك ادخل البريد الإلكترونى", "Error", "خطأ", "OK", "موافق");
            return;
        } else if (!ValidateEmail($("#Complaints_Email").val())) {
            AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صحيح", "Error", "خطأ", "OK", "موافق");
            return;
        } else if ($.trim($("#ComplaintsContent").val()) == "") {
            AlertFunction("Please enter Content", "من فضلك ادخل المحتوى", "Error", "خطأ", "OK", "موافق");
            return;
        } else {
            ComplaintsModule.HandleComplaintsValidationSuccess();
        }
    }
    ,

    //uploads Callbacks


    HandleUploadDocSuccess: function (name, r) {
        try {
            if (r.Message.Code == 0) {
                ComplaintsModule.uploadedFileName = name;
                $("#Complaints_CheckMark").css("display", "block");
            } else {
                AlertFunction("Error Code : " + r.Message.Code + " Error Details : " + r.Message.Body, "كود الخطأ : " + r.Message.Code + " تفاصيل الخطأ : " + r.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        } catch (e) {}
    },


    HandleCameraUploadSuccess: function (name, r) {
        var data = JSON.parse(r.response);
        if (data.Message.Code == 0) {
            $("#Complaints_CheckMark").css("display", "block");
            ComplaintsModule.uploadedFileName = name;
        } else {
            AlertFunction("Error Code : " + data.Message.Code + " Error Details : " + data.Message.Body, "كود الخطأ : " + data.Message.Code + " تفاصيل الخطأ : " + data.Message.Body, "Error", "خطأ", "OK", "موافق");
        }
    },

    HandleUploadDocSuccessForFileChooser: function (name, r) {
        try {

            var data = JSON.parse(r.response);
            if (data.Message.Code == 0) {
                ComplaintsModule.uploadedFileName = name;
                $("#Complaints_CheckMark").css("display", "block");

            } else {
                AlertFunction("Error Code : " + data.Message.Code + " Error Details : " + data.Message.Body, "كود الخطأ : " + data.Message.Code + " تفاصيل الخطأ : " + data.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        } catch (e) {}
    },
    HandleUploadDocFail: function (name) {
        try {
            console.log('upload form kitkat chooser failed');
        } catch (e) {}
    },

    HandleCameraUploadFail: function () {
        console.log('cam failed');
    },



    //uploads Callbacks



    HandleComplaintsValidationSuccess: function () {

        try {
            ShowLoadingSpinner();
                // POST Method
                var URL = Globals.ServicesURI_Test + "add/complainsuggestionfeedback/language/#Lang#";
                URL = URL.replace("#Lang#", Language);
                var ComplaintsObj = {
                    Header: {
                        AttachedFiles: [],
                        UserName: '',
                        UserType: '',
                        ContactRowId: '',
                        EmployerRowId: '',
                        FullName: ''
                    }
                };
                if (ComplaintsModule.uploadedFileName) {
                    ComplaintsObj.Header.AttachedFiles[0] = ComplaintsModule.uploadedFileName;
                }
                ComplaintsObj.RequestType = $("#ComplaintsTypeDropDown").val();

                if ($("#ComplaintsSubTypeDropDown_Complaint").is(":visible")) {
                    ComplaintsObj.RequestSubType = $("#ComplaintsSubTypeDropDown_Complaint").val();
                } else {
                    ComplaintsObj.RequestSubType = $("#ComplaintsSubTypeDropDown_Feedback").val();
                }

                ComplaintsObj.Comment = $("#ComplaintsContent").val();

                if (Globals.UserLoggedIn == true) {
                    ComplaintsObj.ContactRowId = Globals.ContactRowId;
                } else {
                    ComplaintsObj.ContactRowId = "@@";
                }
                ComplaintsObj.FullName = $("#Complaints_FullName").val();
                //
                var ClearMobNum = GetFormatedPhoneNoValue("Complaints_Mobile_CountryCodeDDL", "Complaints_Mobile");
                ComplaintsObj.Mobile = ClearMobNum;
                //
                ComplaintsObj.Email = $("#Complaints_Email").val();

                var data = JSON.stringify(ComplaintsObj);
                Log(data, "DATA");
                CallWebService(URL, 'ComplaintsModule.HandleSendComplaintsCallSuccess(response)', "", data, "POST", false, true);
            } catch (e) {

            }
        },

        HandleSendComplaintsCallSuccess: function (response) {
            try {
                HideLoadingSpinner();
                if (response.Message.Code == 0) {

                    //reset
                    ComplaintsModule.resetCountsandVars();



                    var msg;
                    if (Language == 'en') {
                        msg = "Your request has been registered successfully, in order to track the status of your application, use this case number: ";
                    } else {
                        Globals.CamSuccess = false;
                        msg = "لقد تم تسجيل طلبك بنجاح, من أجل متابعة حالة طلبك، استخدم هذا الرقم :";
                    }
                    AlertFunction(msg + response.CaseNumber, msg + response.CaseNumber, "Success", "نجاح", "OK", "موافق");

                    if (Globals.UserLoggedIn == false) {
                        CleanPage('Complaints');
                        $("#Complaints_Mobile_CountryCodeDDL").val("+971").trigger('change');
                        $("#Complaints_Mobile").val('');

                    } else {
                        $("#ComplaintsTypeDropDown").val("-1").trigger("change");
                        $("#ComplaintsSubTypeDropDown").val("-1").trigger("change");
                        $("#ComplaintsContent").val("");

                        //clearn  dropdowns

                        $("#Complaints select").val("-1").trigger("change");



                    }
                    $("#Complaints_CheckMark").hide();
                } else {
                    HideLoadingSpinner();
                    AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                }

            } catch (e) {}
        }

    };