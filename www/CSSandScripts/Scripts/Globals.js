﻿var Globals = {
    // ServicesURI_Test: 'http://mgovws.gpssa.ae/SPREST.svc/',
    // UploadServiceURI: 'http://mgovws.gpssa.ae/upload/index.aspx',
    ServicesURI_Test: 'https://mw.gpssa.ae/SPREST.svc/',
    UploadServiceURI: 'https://mw.gpssa.ae/upload/index.aspx',
    EmployerRowID: '',
    UserLoggedIn: false,
    MyLongitude: 0,
    MyLatitude: 0,
    LoginRspObj: null,
    UserName: '',
    UserType: '',
    ContactRowId: '',
    FullName: '',
    UserServicesArr: null,
    CamSuccess: false
};