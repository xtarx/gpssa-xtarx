﻿var mobileOS;
var Language;
var DeviceHeight;
var DeviceWidth;
var CurrentActivePage = "";
var ISEmirateSGotten = false;
var EmiratesHTML = "";
var CurrentActiveFooterTab = "";
var GPSSAOfficeResponseGotten = false;
var GPSSAOfficeResponse = null;
var WidgetsArr = new Array();
var CurrentView = null;
var NoofShownSpinner = 0;
var CallTimeOut;
var retries = 0;
var CurrentFileUDID;
///camera callback funs
var camera_success_callback;
var camera_fail_callback;

var fileChooser_success_callback;
var fileChooser_fail_callback;
var theJSON;
//is event bound ( onClick multiple listerns cam case)
var complaints_cam_event_bound=false;
var complaints_filechooser_event_bound=false;
var registration_cam_event_bound=false;
var registration_filechooser_event_bound=false;
var employer_cam_event_bound=false;
var employer_filechooser_event_bound=false;

var datepicker_event_bound=false;


$.ajaxPrefilter(function (options, originalOptions, jqXHR) { });

//$(window).on("orientationchange", function (event) {
//    HandleOrientationChanged();
//});

$(document).ready(function () {
    try {


        //ios keyboard overlay fix
        // document.body.style.height = screen.availHeight + 'px';


        if( !Globals.UserLoggedIn&&localStorage.getItem("FLASH_UserServicesArr") != null&& localStorage.getItem("FLASH_EmployerRowID") != null) {

            // alert("in cond");

            if(localStorage.getItem("FLASH_ContactRowId") != null){
                Globals.ContactRowId=localStorage.getItem("FLASH_ContactRowId");
            }
            if(localStorage.getItem("FLASH_EmployerRowID") != null){
                Globals.EmployerRowID=localStorage.getItem("FLASH_EmployerRowID");
            }
            if(localStorage.getItem("FLASH_FullName") != null){
                Globals.FullName=localStorage.getItem("FLASH_FullName");
            }
            if(localStorage.getItem("FLASH_UserName") != null){
                Globals.UserName=localStorage.getItem("FLASH_UserName");
            }
            if(localStorage.getItem("FLASH_UserType") != null){
                Globals.UserType=localStorage.getItem("FLASH_UserType");
            }

            if(localStorage.getItem("FLASH_LoginRspObj") != null){
                Globals.LoginRspObj=JSON.parse(localStorage.getItem("FLASH_LoginRspObj"));
            }


            if(localStorage.getItem("FLASH_UserServicesArr") != null){
                Globals.UserServicesArr=JSON.parse(localStorage.getItem("FLASH_UserServicesArr"));
            }

   //remove after transfer to Globals
   localStorage.removeItem("FLASH_UserServicesArr");
   localStorage.removeItem("FLASH_ContactRowId");
   localStorage.removeItem("FLASH_EmployerRowID");
   localStorage.removeItem("FLASH_FullName");
   localStorage.removeItem("FLASH_LoginRspObj");
   localStorage.removeItem("FLASH_UserName");
   localStorage.removeItem("FLASH_UserType");

   Globals.UserLoggedIn=true;


}

        //






        // //date picker
        // //if ios change type to date
        // if (device.platform.toLowerCase() === 'ios' ) {
        //     $('.datepicker').prop('type', 'date');
        // }else{

        //     $(document).on('click', '.datepicker', function () {
        //         showDatePicker($(this), 'date');
        //     });
        // }
        // //date picker

        DeviceHeight = window.innerHeight;
        DeviceWidth = window.innerWidth;
        $(".PageClass").height(DeviceHeight - 100);
        $(".ContentClass").height(DeviceHeight - 100);
        $(".DivContainerClass").height(DeviceHeight - 100);
        $(".DivContainerClass").css("overflow", "scroll");
        $("#SpinnerContentDiv").css('margin-top', (DeviceHeight / 2) - (DeviceHeight * .1));
        $("#ALLPageContent").width(DeviceWidth - 20);
        $("#FooterDiv").show();
        DisableWindowScroll();
        Language = localStorage.getItem("Lang");
        if (Language != "en") {
            Language = "ar"
        }
        LoadPageContent("StartUp", 'StartUp.HandleStartUpDocReady()', true, true);
        BindEvents("#HomeTR", "click", "StartUp.LoadStartuUp();ClearFooterTab()");
        BindEvents("#ComplaintsTR", "click", "ComplaintsModule.LoadComplaints();ClearFooterTab()");
        BindEvents("#RegisterationTR", "click", "AutoRegisteration.LoadAutoRegistrationContent();ClearFooterTab()");
        BindEvents("#RegisterNewEmployerTR", "click", "RegistrationModule.LoadRegistration()");
        BindEvents('#ServicesTR', 'click', 'ServicesList.LoadServicesPage(); ClearFooterTab()');
        BindEvents('#AboutGAPSSATR', 'click', 'AboutGPSSA.LoadAboutGPSSAPage(); ClearFooterTab()');
        BindEvents('#NewsTR', 'click', 'News.LoadNewsPage(); ClearFooterTab()');
        BindEvents('#LoginTR', 'click', 'Login.LoadLoginPage(); ClearFooterTab()');
        BindEvents('#NearestGAPSSATR', 'click', 'NearestOffice.LoadNearestOfficeContent()');
        BindEvents('#LogOutTR', 'click', 'Logout.CallLogout(); ClearFooterTab()');
        BindEvents('#termsTR', 'click', 'Terms.LoadTermsContent();ClearFooterTab()');
        BindEvents('#privacypolicyTR', 'click', 'Privacy.LoadPrivacyContent();ClearFooterTab()');
        BindEvents('#PensionsGuideTR', 'click', 'PensionsGuide.LoadPensionsGuideContent();ClearFooterTab()');
        BindEvents('#preferenceTR', 'click', 'Preferences.LoadPreferencesContent();ClearFooterTab()');
        BindEvents('#HelpTR', 'click', 'Help.LoadHelpContent();ClearFooterTab()');
        BindEvents('#ShareTR', 'click', 'Share();ClearFooterTab()');
        HandleInputsFn();
        

          //if logged in
          if(Globals.UserLoggedIn){
            $("#LoginTR").hide();
            $("#LogOutTR").show();
        }else{
            $("#LoginTR").show();
            $("#LogOutTR").hide();
        }
        //if logged in



        var URL = Globals.ServicesURI_Test + "view/lov/type/GPSSA_EMIRATE/language/#Lang#";
        URL = URL.replace("#Lang#", Language);
        CallWebServiceWithOauth(URL, 'GetEmiratesCallSuccess(response)', "", "", "", false, false, false);
        try {
            var PageHeight;
            $("#MenuPanel").panel({
                beforeopen: function (event, ui) {
                    PageHeight = $(".PageClass").height();
                    //$(".PageClass").height(DeviceHeight * 1.3)
                    if (Globals.UserLoggedIn) {
                        $(".PageClass").height(45 * 16);
                    }
                    else {
                        $(".PageClass").height(45 * 15);
                    }
                    //setTimeout(function () {
                    //    $(".ui-panel-wrapper").css('position', 'fixed');
                    //}, 1000);
        }
    });
            $("#MenuPanel").panel({
                beforeclose: function (event, ui) {
                    $(".PageClass").height(PageHeight)
                    //setTimeout(function () {
                    //    $(".ui-panel-wrapper").css('position', 'relative');
                    //}, 500);
        }
    })
        } catch (e) { }

    } catch (e) {
        Log(e, "Doc Ready")
    }
});

function Share() {
    try {
        //jsinterface.Share();
        window.plugins.socialsharing.share("", null, null, 'https://play.google.com/store/apps/details?id=com.phonegap.gpssaMGov&hl=en');
    } catch (e) {
        alert("Share" + e.toString());
    }
}

function GetEmiratesCallSuccess(response) {
    try {
        if (response != null) {
            var OptionTemp = "<option value='#Val#'>#text#</option>";
            var DefaultOption = "";
            var AllHTML = "";
            if (response.LOVs != null && response.LOVs.length > 0) {
                if (Language == "en") {
                    AllHTML += "<option value='-1'>Select Emirate</option>"
                } else {
                    AllHTML += "<option value='-1'>اختر الإمارة</option>"
                }
                for (var i = 0; i < response.LOVs.length; i++) {
                    OptionTemp = "<option value='#Val#'>#text#</option>";
                    AllHTML += OptionTemp.replace("#Val#", response.LOVs[i]).replace("#text#", response.LOVs[i])
                }
                ISEmirateSGotten = true;
                EmiratesHTML = AllHTML
            }
        }
    } catch (e) { }
}

function GetEmiratesCallFailure() {
    AlertFunction("Communication error, please make sure of internet connection", "حدث خطأ اثناء الاتصال من فضلك تأكد من الاتصال بالانترنت", "Error", "خطأ", "OK", "موافق")
}

function Log(msg, caller) {
    if (caller == undefined) {
        caller = ""
    }
    console.log("------------------ Start Logs at " + caller + " ---------------");
    console.log(msg);
    console.log("------------------ end ---------------")
}

function SetHeaderTitle(Header_EN, Header_AR) {
    if (Language == "en") {
        $("#HeaderTitle").html(Header_EN)
    } else {
        $("#HeaderTitle").html(Header_AR)
    }
}

function OpenMenu() {
    $("#MenuPanel").panel("open")
}

function CloseMenu() {
    $("#MenuPanel").panel("close")
}

function ClearFooterTab() {
    CurrentActiveFooterTab = ""
}

function HandleFooterTabsClicked(id) {
    $("#ALLPageContent").scrollTop(0);
    $('.FooterTabs').removeClass("FooterActiveTabClass");
    $('.FooterTabs').addClass("FooterTabClass");
    $('#' + id).addClass("FooterActiveTabClass");
    if (id == CurrentActiveFooterTab) {
        return
    }
    CurrentActiveFooterTab = id;
    if (id == "Footer_ContactUs") {
        ContactUs.LoadContactUsPage()
    } else if (id == "Footer_NearestOffice") {
        NearestOffice.LoadNearestOfficeContent()
    } else if (id == "Footer_FeedBacK") {
        ComplaintsModule.LoadComplaints()
    }
}

function HandleBtnLangClicked() {

    if (localStorage.getItem("Lang") == "en") {
        localStorage.setItem("Lang", "ar");
        Language = "ar";


//set globals b4 swithcing
        // ContactRowId
        // EmployerRowID
        // FullName
        // LoginRspObj
        // UserName
        // UserType

        if(Globals.UserLoggedIn) {

            localStorage.setItem("FLASH_UserServicesArr", JSON.stringify(Globals.UserServicesArr));
            localStorage.setItem("FLASH_ContactRowId", (Globals.ContactRowId));
            localStorage.setItem("FLASH_EmployerRowID", (Globals.EmployerRowID));
            localStorage.setItem("FLASH_FullName", (Globals.FullName));
            localStorage.setItem("FLASH_LoginRspObj", JSON.stringify(Globals.LoginRspObj));
            localStorage.setItem("FLASH_UserName", (Globals.UserName));
            localStorage.setItem("FLASH_UserType", (Globals.UserType));


        }

        //set globals b4 swithcing
        // setTimeout(function() { window.location = "index_AR.html"; }, 500);

        window.location = "index_AR.html"
        //window.location = "index_AR_preload.html";
        // location.replace("index_AR.html");

    } else {
        Language = "en";
        localStorage.setItem("Lang", "en");


        //set globals b4 swithcing
        // ContactRowId
        // EmployerRowID
        // FullName
        // LoginRspObj
        // UserName
        // UserType

        // localStorage.setItem("UserServicesArr", JSON.stringify(Globals.UserServicesArr));
        // localStorage.setItem("ContactRowId", (Globals.ContactRowId));
        // localStorage.setItem("EmployerRowID", (Globals.EmployerRowID));
        // localStorage.setItem("FullName", (Globals.FullName));
        // localStorage.setItem("UserServicesArr", JSON.stringify(Globals.UserServicesArr));
        // localStorage.setItem("LoginRspObj", JSON.stringify(Globals.LoginRspObj));

        // localStorage.setItem("UserName", (Globals.UserName));
        // localStorage.setItem("UserType", (Globals.UserType));


        if(Globals.UserLoggedIn) {

            localStorage.setItem("FLASH_UserServicesArr", JSON.stringify(Globals.UserServicesArr));
            localStorage.setItem("FLASH_ContactRowId", (Globals.ContactRowId));
            localStorage.setItem("FLASH_EmployerRowID", (Globals.EmployerRowID));
            localStorage.setItem("FLASH_FullName", (Globals.FullName));
            localStorage.setItem("FLASH_LoginRspObj", JSON.stringify(Globals.LoginRspObj));
            localStorage.setItem("FLASH_UserName", (Globals.UserName));
            localStorage.setItem("FLASH_UserType", (Globals.UserType));


        }

        
        //set globals b4 swithcing
        // setTimeout(function() { window.location = "index_EN.html"; }, 500);

        //window.location = "index_EN_preload.html";
        window.location = "index_EN.html";

        // window.location = "https://www.google.com.eg"

    }

    try {
        //window.plugins.spinnerDialog.show(null, null, true);
    } catch (e) {

    }
}

function HandleOrientationChanged() {
    if (Language == "ar") {
        localStorage.setItem("Lang", "ar");
        window.location = "index_AR.html"
    } else {
        Language = "en";
        localStorage.setItem("Lang", "en");
        window.location = "index_EN.html"
    }
}

function LoadPageContent(PageID, successFunction, ShowSpinner, HideSpinner, CallSuccessAfterGettingFromCache, RootPage, FunctionStr) {
    try {

        var url = "";

        if (CurrentActivePage == PageID) {
            CloseMenu();
            return
        }

        if (ShowSpinner == undefined || ShowSpinner == null) {
            ShowSpinner = false
        }
        if (HideSpinner == undefined || HideSpinner == null) {
            HideSpinner = false
        }
        if (CallSuccessAfterGettingFromCache == undefined || CallSuccessAfterGettingFromCache == null) {
            CallSuccessAfterGettingFromCache = true
        }

        if (RootPage == undefined || RootPage == null) {
            RootPage = true;
        }

        if (FunctionStr == undefined) {
            FunctionStr = "";
        }

        if (RootPage == true) {
            WidgetsArr = new Array();
            if (PageID == "StartUp") {
                WidgetsArr.push("");
            }
            else {
                WidgetsArr.push("StartUp.LoadStartuUp()");
            }
        }
        else {
            WidgetsArr.push(FunctionStr);
        }

        $('.FooterTabs').removeClass("FooterActiveTabClass");
        $('.FooterTabs').addClass("FooterTabClass");

        if (PageID == "ContactUS") {
            $('#Footer_ContactUs').addClass("FooterActiveTabClass")
        } else if (PageID == "Complaints") {
            $('#Footer_FeedBacK').addClass("FooterActiveTabClass")
        } else if (PageID == "NearestOffice") {
            $('#Footer_NearestOffice').addClass("FooterActiveTabClass")
        } else if (PageID == "ContactusMap") {
            $('#Footer_ContactUs').addClass("FooterActiveTabClass")
            PageID = "NearestOffice";
        }

        ClearFooterTab();

        if ($("#" + PageID).length > 0) {
            setTimeout(function () {
                CloseMenu()
            }, 100);
            $(".DivContainerClass").hide();
            $("#" + PageID).show();
            $("#" + PageID).scrollTop(0);
            if (CallSuccessAfterGettingFromCache) {
                eval(successFunction)
            }
            if (HideSpinner) {
                HideLoadingSpinner();
            }
            CurrentActivePage = PageID;
            Log("Load content from cache", "")
        } else {
            Log("Load new content", "");
            if (ShowSpinner) {
                ShowLoadingSpinner();
            }
            CloseMenu();
            CurrentActivePage = PageID;
            if (Language == "en") {
                url = PageID + "_EN.html"
            } else {
                url = PageID + "_AR.html"
            }
            $.ajax({
                type: "GET",
                url: 'Templates/' + url,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                dataType: "html",
                data: '',
                success: function (response, textStatus, XMLHttpResponse) {
                    $("#ALLPageContent").append(response);
                    $(".DivContainerClass").hide();
                    $("#" + PageID).show();
                    $("#" + PageID).scrollTop(0);
                    EnbleWindowScroll();
                    BindEvents('#MenuIconTD', 'click', 'OpenMenu()');
                    eval(successFunction);
                    if (HideSpinner) {
                        HideLoadingSpinner()
                    }
                },
                error: function (xhr, status, error) {
                    EnbleWindowScroll();
                    AlertFunction("Communication error, please make sure of internet connection", "حدث خطأ اثناء الاتصال من فضلك تأكد من الاتصال بالانترنت", "Error", "خطأ", "OK", "موافق");
                    HideLoadingSpinner()
                }
            })
}
} catch (e) {
    Log(e, "LoadPageContent")
}
}

function BindEvents(ID, event, functionName) {
    try {
        $(ID).unbind(event);
        $(ID).on(event, function () {
            eval(functionName)
        })
    } catch (e) {
        Log(e, "BindEvents")
    }
}

function CallWebServiceWithOauth(url, SuccessFunction, FailureFunction, Data, Type, ShowSpinner, HideSpinner, showDefaultErrorAlert, timeout) {
    try {

        var request_data;
        var ProcessData = true;
        var DataType = "json";
        if (ShowSpinner == undefined || ShowSpinner == null) {
            ShowSpinner = false
        }
        if (HideSpinner == undefined || HideSpinner == null) {
            HideSpinner = false
        }
        if (showDefaultErrorAlert == undefined || showDefaultErrorAlert == null) {
            showDefaultErrorAlert = true
        }
        if (ShowSpinner) {
            ShowLoadingSpinner()
        }
        if (Data == undefined || Data == "") {
            Data = '{}'
        }
        if (Type == undefined || Type == "") {
            Type = 'GET';
            ProcessData = false;
            DataType = "TEXT";
        }
        try {

            if (!timeout) {
                timeout = 60000;
            }

            CallTimeOut = setTimeout(function () {
                AlertFunction("Connection timeout", "Connection timeout", "Error", "خطأ", "OK", "موافق");
                HideLoadingSpinner();
            }, timeout + timeout / 5);

            var ServicURL = GetOAuthURL(url);
            Log(ServicURL);
            $.ajax({
                url: ServicURL,
                type: Type,
                data: Data,
                timeout: timeout,
                contentType: "application/json",
                success: function (response, textStatus, xhr) {
                    clearTimeout(CallTimeOut);
                    Log(response, "ajax Response");
                    eval(SuccessFunction);
                    if (HideSpinner) {
                        HideLoadingSpinner()
                    }
                },
                error: function (xhr, status, error) {
                    clearTimeout(CallTimeOut);
                    if (showDefaultErrorAlert) {
                        if (status == "timeout" || error == "timeout") {
                            AlertFunction("Connection timeout", "Connection timeout", "Error", "خطأ", "OK", "موافق");
                        }
                        else {
                            // AlertFunction("please make sure that the device is connected to the internet", "من فضلك تأكد ان الجهاز متصل بالانترنت", "Error", "خطأ", "OK", "موافق")
                            AlertFunction("Could not reach server make sure you are connected to the internet and try again", "لا يمكن  الإتصال بالسيرفر تأكد من أنك متصل بالإنترنت وحاول مرة أخرى", "Error", "خطأ", "OK", "موافق")
                        }
                    }
                    eval(FailureFunction);
                    //if (HideSpinner) {
                        HideLoadingSpinner();
                    //}
                    Log(error, "error");
                    Log(status, "status");
                    Log(xhr, "xhr")
                }
            })
} catch (e) { }
} catch (e) {
    Log(e, "CallWebService")
}
}

function GetOAuthURL(I_url) {
    try {
        var mConsumerKey = "6eGR8BzQ0hgjDJDhUuiGBOCLzb";// jsinterface.GetLookup("0");
        var mConsumerSecret = "7phquMfsANaNvUUqHF6w";//jsinterface.GetLookup("1");
        var mOAuthSignatureMethod = "HMAC-SHA1";//jsinterface.GetLookup("2");
        var mOAuthVersion = "1.0";//jsinterface.GetLookup("3");
        var mNormalizedUrl = I_url;

        var accessor =
        {
          consumerSecret: mConsumerSecret,
          tokenSecret: null,
          serviceProvider:
          {
              accessTokenURL: mNormalizedUrl
          },
          consumerKey: mConsumerKey
      };

      var message =
      {
        action: accessor.serviceProvider.accessTokenURL,
        method: "GET",
        parameters: []
    };
    message.parameters.push(["oauth_consumer_key", mConsumerKey]);
    message.parameters.push(["oauth_signature_method", mOAuthSignatureMethod]);
    message.parameters.push(["oauth_version", mOAuthVersion]);
    message.parameters.push(["oauth_timestamp", ""]);
    message.parameters.push(["oauth_nonce", ""]);
    message.parameters.push(["oauth_signature", ""]);

    OAuth.setTimestampAndNonce(message);
    OAuth.SignatureMethod.sign(message, accessor);

    var url = mNormalizedUrl + "?oauth_consumer_key={0}&oauth_signature_method={1}&oauth_timestamp={2}&oauth_nonce={3}&oauth_version={4}&oauth_signature={5}";
    url = url.replace("{0}", mConsumerKey);
    url = url.replace("{1}", mOAuthSignatureMethod);
    url = url.replace("{2}", message.parameters[3][1]);
    url = url.replace("{3}", message.parameters[4][1]);
    url = url.replace("{4}", mOAuthVersion);
    url = url.replace("{5}", message.parameters[5][1]);

    return url;
} catch (e) {
    Log(e);
}
}

function CallWebService(url, SuccessFunction, FailureFunction, Data, Type, ShowSpinner, HideSpinner, showDefaultErrorAlert, timeout) {
    try {

        CallWebServiceWithOauth(url, SuccessFunction, FailureFunction, Data, Type, ShowSpinner, HideSpinner, showDefaultErrorAlert, timeout);

        //var ProcessData = true;
        //var DataType = "json";

        //if (ShowSpinner == undefined || ShowSpinner == null) {
        //    ShowSpinner = false
        //}
        //if (HideSpinner == undefined || HideSpinner == null) {
        //    HideSpinner = false
        //}
        //if (showDefaultErrorAlert == undefined || showDefaultErrorAlert == null) {
        //    showDefaultErrorAlert = true
        //}
        //if (ShowSpinner) {
        //    ShowLoadingSpinner()
        //}
        //if (Data == undefined || Data == "") {
        //    Data = '{}'
        //}
        //if (Type == undefined || Type == "") {
        //    Type = 'GET';
        //    ProcessData = false;
        //    DataType = "TEXT"
        //}

        //if (!timeout) {
        //    timeout = 60000;
        //}

        //$.ajax({
        //    type: Type,
        //    url: url,
        //    contentType: "application/json",
        //    dataType: DataType,
        //    processData: ProcessData,
        //    data: Data,
        //    timeout: timeout,
        //    success: function (response) {
        //        Log(response, "ajax Response");
        //        eval(SuccessFunction);
        //        if (HideSpinner) {
        //            HideLoadingSpinner()
        //        }
        //    },
        //    error: function (xhr, status, error) {
        //        if (showDefaultErrorAlert) {
        //            HideLoadingSpinner();
        //            AlertFunction("please make sure that the device is connected to the internet", "من فضلك تأكد ان الجهاز متصل بالانترنت", "Error", "خطأ", "OK", "موافق")
        //        }
        //        eval(FailureFunction);
        //        //if (HideSpinner) {
        //        HideLoadingSpinner()
        //        //}
        //        Log(error, "error");
        //        Log(status, "status");
        //        Log(xhr, "xhr")
        //    }
        //})

} catch (e) {
    Log(e, "CallWebService")
}
}

function getOS() {
    try {
        var ua = navigator.userAgent;
        var uaindex;
        if (ua.match(/iPad/i) || ua.match(/iPhone/i)) {
            mobileOS = 'iOS';
            uaindex = ua.indexOf('OS ')
        } else if (ua.match(/Android/i)) {
            mobileOS = 'Android';
            uaindex = ua.indexOf('Android ')
        } else {
            mobileOS = 'BlackBerry'
        }
        if (mobileOS === 'iOS' && uaindex > -1) {
            mobileOSver = ua.substr(uaindex + 3, 3).replace('_', '.')
        } else if (mobileOS === 'Android' && uaindex > -1) {
            mobileOSver = ua.substr(uaindex + 8, 3)
        } else {
            mobileOSver = 'BlackBerry'
        }
    } catch (e) {
        Log(e, "getOS")
    }
}

function EnbleWindowScroll() {
    $('html').css('overflow', 'auto');
    $(document).unbind("touchmove")
}

function DisableWindowScroll() {
    $('html').css('overflow', 'hidden');
    $(document).bind("touchmove", function (event) {
        event.preventDefault()
    })
}

function AlertFunction(textEN, textAR, titelEN, titelAR, OK_Tetx_EN, OK_Text_AR, CallBAckFunction) {
    try {
        HideLoadingSpinner();
        jAlert(Language == "en" ? textEN : textAR, Language == "en" ? titelEN : titelAR, "", Language == "en" ? OK_Tetx_EN : OK_Text_AR, CallBAckFunction);
        $('#popup_overlay').bind('click', function () {
            return
        })
    } catch (e) { }
}

function ConfirmationAlert(textEN, textAR, titelEN, titelAR, OK_Tetx_EN, OK_Text_AR, Cancel_Text_EN, Cancel_Text_AR, CallBAckFunction) {
    try {
        HideLoadingSpinner();
        jConfirm(Language == "en" ? textEN : textAR, Language == "en" ? titelEN : titelAR, true, Language == "en" ? OK_Tetx_EN : OK_Text_AR, Language == "en" ? Cancel_Text_EN : Cancel_Text_AR, CallBAckFunction);
        $('#popup_overlay').bind('click', function () {
            return
        })
    } catch (e) { }
}

function ShowLoadingSpinner() {
    try {
        $("#LoadingSpinner").show();
        NoofShownSpinner += 1;
        //window.plugins.spinnerDialog.show(null, null, true);
    } catch (e) {

    }
}

function HideLoadingSpinner() {
    try {

        $("#LoadingSpinner").hide();

        //if (localStorage.getItem("Lang") == "en" || localStorage.getItem("Lang") == "ar") {
        //    NoofShownSpinner += 1;
        //}

        //if (NoofShownSpinner > 0) {
        //    //  window.plugins.spinnerDialog.hide();
        //    NoofShownSpinner -= 1;
        //    HideLoadingSpinner();
        //}
    } catch (e) {
        //alert(e);
    }
}

function isNumberWithDot(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
        return false
    }
    return true
}

function isNumberWithDash(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 45) {
        return false
    }
    return true
}

function isNumber(evt) {
    Log(evt);
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false
    }
    return true
}

function isNumeric(inputText) {
    try {
        var numberformat = /^\-{0,1}(?:[0-9-]+){0,1}(?:\.[0-9-]+){0,1}$/i;
        if (inputText.value.length > 0) {
            if (!inputText.value.match(numberformat)) {
                AlertFunction("Numbers only!", "أرقام فقط!", "Error", "خطأ ", "Ok", "موافق", "");
                inputText.value = ""
            }
        }
    } catch (e) { }
}

function CleanPage(PagesIDs) {
    try {
        var temp = PagesIDs.split(',');
        for (var i = 0; i < temp.length; i++) {
            try {
                $("#" + temp[i] + " input:not(:button)").val("")
            } catch (e) { }
            try {
                $("#" + temp[i] + " textarea").val("")
            } catch (e) { }
            try {
                $("#" + temp[i] + " select").val("-1").trigger("change")
            } catch (e) { }
        }
    } catch (e) { }
}

function HandleInputsFn() {
    $(document).on('focus', 'input:not(:checkbox):not(:button),select,textarea', function () {
        $('.HeaderClass').css('position', 'absolute')
    });
    $(document).on('blur', 'input:not(:checkbox):not(:button),select,textarea', function () {
        window.scrollTo($.mobile.window.scrollLeft(), $.mobile.window.scrollTop());
        $('.HeaderClass').css('position', 'fixed')
    })
}



function todaysDate() {
   var date = new Date();      

   var day = date.getDate().toString(),
   month = (date.getMonth()+1).toString(),
   year = date.getFullYear().toString();

   return year + '-' + (month[1]?month:"0"+month[0]) + '-' + (day[1]?day:"0"+day[0]);


}

  function  perepareDatePicker() {
        //date picker
        //if ios change type to date
        // if (device.platform.toLowerCase() === 'ios' ) {
        //     $('.datepicker').prop('type', 'date');
        // }else{
        //     $(document).on('click', '.datepicker', function () {
        //         showDatePicker($(this), 'date');
        //     });
        // }
        //date picker

    }

 
     function  prepareDatePickerGeneric() {        
        if (device.platform.toLowerCase() === 'android' ) {
            // $('.datepicker').prop('type', 'date');
            $('.datepicker').removeProp('type');
              if(!datepicker_event_bound){
            $(document).on('click', '.datepicker', function () {
                showDatePicker($(this), 'date');
            });
            datepicker_event_bound=true;
            }
        }
    }

function ConvertDateToUNIX(date) {
  // var parts = date.split('-');
  // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
 // var  dateObj= new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based

 // var myDate=date.split("-");
 // var newDate=myDate[1]+"/"+myDate[0]+"/"+myDate[2];
 var dateObj = new Date(date); 
 return dateObj.getTime() / 1000
}

function GetUploadedDocsSize(InputID) {
    var fileUpload = $("#" + InputID).get(0);
    var files = fileUpload.files;
    var Filessize = 0;
    for (var i = 0; i < files.length; i++) {
        if (files[i].size / 1024 / 1024 > 7) {
            return false
        }
    }
    return true
}
function validateExtension(fileName) {
    var Tempname = fileName.split('.');
    var   ext = Tempname[Tempname.length - 1].toLowerCase();

    if (!(ext === "pdf" || ext === "doc" ||ext === "docx"||ext === "xls"
        ||ext === "jpg" ||ext === "jpeg" ||ext === "png"  ) ) { 
        return false;
}
return true
}


var guid = (function () {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
    }
    return function () {
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()
    }
})();

function UploadFiles(InputID, FileUDID, SuccessFunction, FailureFunction, OneFile, HideSpinner, DisplayAlert) {
    try {

        ShowLoadingSpinner();

        if (OneFile == undefined || OneFile == null) {
            OneFile = true
        }
        if (HideSpinner == undefined || HideSpinner == null) {
            HideSpinner = true
        }
        if (DisplayAlert == undefined || DisplayAlert == null) {
            DisplayAlert = true
        }
        var name;
        var fileUpload;
        var imagefile;
        var formData;
        if (OneFile) {
            formData = new FormData();


            fileUpload = $("#" + InputID).get(0);
            imagefile = fileUpload.files[0];
            var Tempname = imagefile.name.split('.');
            

            name = FileUDID + "." + Tempname[Tempname.length - 1];
            formData.append("fileUploaded", imagefile);
            formData.append("fileName", name);
            formData.append("language", Language)
        } else {
            try {
                fileUpload = $("#" + InputID).get(0);
                imagefile = fileUpload.files;
                name = new Array();
                formData = new FormData();
                for (var i = 0; i < imagefile.length; i++) {
                    var Tempname = imagefile[i].name.split('.');
                    name[i] = FileUDID[i] + "." + Tempname[Tempname.length - 1];
                    formData.append("fileUploaded", imagefile[i]);
                    formData.append("fileName", name[i]);
                    formData.append("language", Language)
                }
            } catch (e) { }
        }
        $.ajax({
            url: GetOAuthURL(Globals.UploadServiceURI),
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function (r, textStatus, jqXHR) {
                if (DisplayAlert) {
                    AlertFunction(r.Message.Body, r.Message.Body, "Success", "نجاح", "OK", "موافق");
                }
                eval(SuccessFunction);
                if (HideSpinner) {
                    HideLoadingSpinner();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                AlertFunction("Server is currently unavailable Please try again later " , "برجاء المحاولة مرة اخري", "Error", "خطأ", "OK", "موافق");
                // AlertFunction("Error: " + textStatus, "Error: " + textStatus, "Error", "خطأ", "OK", "موافق");
                eval(FailureFunction);
                HideLoadingSpinner();
            }
        })
} catch (e) {
    alert("Upload file " + e);
    HideLoadingSpinner();
}
}

function ValidateEmail(email) {
    email=$.trim(email);
    var check = /^[\w\.\+-]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,6}$/;
    if (!check.test(email)) {
        return false
    }
    return true
}

function ReturnImageSrc(String) {
    var m, urls = [],
    str = String,
    rex = /<img.*?src="([^">]*\/([^">]*?))".*?>/g;
    while (m = rex.exec(str)) {
     m[1]= m[1].replace('https://','http://');
     urls.push(m[1])
 }
 return urls
}

function replaceAll(txt, replace, with_this) {
    if (txt == null) {
        txt = ""
    }
    return txt.replace(new RegExp(replace, 'g'), with_this)
}

function ValidateBirthDate(date) {
    var CurentDate = new Date();
    var CurentUnixDate = (CurentDate.getTime() / 1000);
    var BirthDateUnix = ConvertDateToUNIX(date);
    if (BirthDateUnix >= CurentUnixDate) {
        return false
    } else {
        return true
    }
}

function ValidatePassLength(Password) {
    if (Password.length < 7) {
        return true
    } else {
        return false
    }
}

/**
 * replace + with 00 and remove dashes and spaces
 * @param  {[type]} str [description]
 * @return {[type]}     [description]
 */
 function cleanMobileNumber(str) {
    var res = str.replace("+", "00");
    res = res.replace("-", "");
    res=res.replace(/ /g,'');
    return res;
}

function shorten(text, maxLength) {
    var ret = text;
    if (ret.length > maxLength) {
        ret = ret.substr(0,maxLength-3) + "...";
    }
    return ret;
}


function isArabic(inputText) {

    try {

        var pattern = /^[-'\s\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]*$/;
        var pattern2 = /^[-\s']|[-\s']$/;
        var value1 = inputText.value;
        value1=value1.replace(/ /g,'');

        if (value1 == "") {
            AlertFunction("Please enter name in arabic", "من فضلك ادخل الاسم باللغة العربية", "Error", "خطأ ", "Ok", "موافق", "");
            inputText.value  = inputText.value.substring(0, inputText.value.length - 1);
            
        }

        if (pattern.test(value1)) {
            if (pattern2.test(value1)) {
                // args.IsValid = false;
                AlertFunction("Please enter name in arabic", "من فضلك ادخل الاسم باللغة العربية", "Error", "خطأ ", "Ok", "موافق", "");
                inputText.value  = inputText.value.substring(0, inputText.value.length - 1);
                
            }
        }
        else {
            // args.IsValid = false;
            AlertFunction("Please enter name in arabic", "من فضلك ادخل الاسم باللغة العربية", "Error", "خطأ ", "Ok", "موافق", "");
            inputText.value  = inputText.value.substring(0, inputText.value.length - 1);

        }

    } catch (e) { }

}

function isEnglish(inputText)
{
	try{
		var pattern = /^[A-Za-z][A-Za-z0-9]*$/;
        var value1 = inputText.value;
        value1=value1.replace(/ /g,'');

        if (value1 == "") {
            inputText.value  = inputText.value.substring(0, inputText.value.length - 1);

            AlertFunction("Please enter name in english", "من فضلك ادخل الاسم باللغة الانجليزيه", "Error", "خطأ ", "Ok", "موافق", "");
        }


        if (!pattern.test(value1)) {
            inputText.value  = inputText.value.substring(0, inputText.value.length - 1);

            AlertFunction("Please enter name in english", "من فضلك ادخل الاسم باللغة الانجليزيه", "Error", "خطأ ", "Ok", "موافق", "");
        }
        
    }catch(e){

    }
}


function isArabicBool(inputText) {

    try {

        var pattern = /^[-'\s\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]*$/;
        var pattern2 = /^[-\s']|[-\s']$/;
        var value1 = inputText.value;
        value1=value1.replace(/ /g,'');


        if (value1 == "") {

            return false;
        }

        if (pattern.test(value1)) {
            if (pattern2.test(value1)) {
                // args.IsValid = false;

                return false;
            }
        }
        else {
            // args.IsValid = false;
            return false;
        }

        return true;

    } catch (e) { }

}


// function isArabic(inputText) {
//     try {
//         var Arabicformat = /[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]/;
//         if (inputText.value.length > 0) {
//             if (!inputText.value.match(Arabicformat)) {
//                 AlertFunction("Arabic only!", "عربى فقط!", "Error", "خطأ ", "Ok", "موافق", "");
//                 inputText.value = ""
//             }
//         }
//     } catch (e) { }
// }

function FormatChar(value) {
    value = value.replace(/\D/g, '');
    return value
}

function ConvertToHtml(content, id) {
    $('#' + id).html(content);
}

function changeToPassword(id) {
    document.getElementById(id).setAttribute("type", "number");
    setTimeout(function () {
        document.getElementById(id).setAttribute("type", "password")
    }, 0);
}

function changeToNumber(id) {
    setTimeout(function () {
        document.getElementById(id).setAttribute("type", "number")
    }, 500);
}

function ConvertUNIXToDate(unix_timestamp) {
    var date = new Date(unix_timestamp * 1000);
    return formatDateToddDDMMYYYY(date, Language);
}

function formatDateToddDDMMYYYY(inputFormat, lang) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    var year = d.getFullYear();
    var month = pad(d.getMonth() + 1);
    var day = pad(d.getDate());
    if (lang == 'ar') {
        var weekdays = ["الأحد", "الاثنين", "الثلاثاء", "الأربعاء", "الخميس", "الجمعة", "السبت"];
        var Months = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو", "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "يوليو"];
    } else if (lang == 'en') {

        var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        var Months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    }
    var weekday = weekdays[d.getDay()];
    inputFormat = weekday + ' ' + day + '-' + Months[month - 1] + '-' + year;
    return inputFormat;
}

function SortArray(Array, SortAttr) {
    Array.sort(function (Obj1, Obj2) {
        if (Obj1[SortAttr] > Obj2[SortAttr])
            return 1;
        else if (Obj1[SortAttr] < Obj2[SortAttr])
            return -1;
        else
            return 0;
    });
    return Array;
}

function isUndefinedOrNullOrBlank(v) {
    try {
        if (v != null)
            v = v.trim();

        if (v == undefined || v == "undefined" || v == null || v == "")
            result = true;
        else
            result = false;
        return result;
    } catch (e) {
        return false;
    }
}

function HandleBackButton() {

    var functionStr = WidgetsArr.pop();
    if (functionStr != undefined && functionStr != "") {
        eval(functionStr);
    }
    else {
        CloseMenu();
        ConfirmationAlert("Are you sure you want to quit the application ?", "هل انت متأكد من انك تريد الخروج من البرنامج؟", "Confirm", "تأكيد", "Quit", "خروج", "Cancel", "إلغاء", "navigator.app.exitApp();");
    }
    HideLoadingSpinner();
}

// Since the '+' symbol's decimal ASCII code is 43, you can add it to your condition
function isNumberWithPlus(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode != 43 && charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}



var DeviceHeight = $(window).height();
var is_keyboardOpen = false;
window.addEventListener("resize", function () {
    is_keyboardOpen = (window.innerHeight < DeviceHeight);
    if (is_keyboardOpen) {
        $('#FooterDiv').css('display', 'none');
        //setTimeout(function () {
        //    $('#footer').css('display', 'none');
        //}, 105);
} else {
    setTimeout(function () {
        window.scrollTo($.mobile.window.scrollLeft(), $.mobile.window.scrollTop());
        $('#FooterDiv').css('display', 'block');
        setTimeout(function () {
                //$('#FooterDiv').css('position', 'fixed');
            }, 20);
    }, 20);
}
}, false);




function getPhoto(UDID) {
    try {
        CurrentFileUDID = UDID;

        navigator.camera.getPicture(onPhotoDataSuccess, ongetPhotoFail,
        {
            // quality: 30,
            quality: 45,
                //destinationType: Camera.DestinationType.DATA_URL,
                destinationType: navigator.camera.DestinationType.FILE_URI,
                //sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                //encodingType: navigator.camera.EncodingType.JPEG,
                // targetWidth: 100,
                // targetHeight: 100
            });
    } catch (e) {
        //alert("Func: getPhoto Excep, " + e);
    }
}


function onPhotoDataSuccess(imageData) {
    try {
        ShowLoadingSpinner();
        // if(camera_success_callback!=null){
        //     eval(camera_success_callback);    
        // }
        onCapturePhoto(imageData);
    } catch (e) {
        //alert("Func: onPhotoDataSuccess Excep, " + e);
    }
}

function ongetPhotoFail(message) {
    if(camera_fail_callback!=null){
        eval(camera_fail_callback);    
    }
   // alert('Func. ongetPhotoFail Excep: ' + message);
}

function clearCache() {
    //navigator.camera.cleanup();
}

function win(r) {

    try {
        clearCache();
        retries = 0;
        var data = JSON.parse(r.response);
        if (data.Message.Code == 0) {
            AlertFunction(data.Message.Body, data.Message.Body, "Success", "نجاح", "OK", "موافق");
        }
        else {
            AlertFunction("Error Code : " + data.Message.Code + " Error Details : " + data.Message.Body, "كود الخطأ : " + data.Message.Code + " تفاصيل الخطأ : " + data.Message.Body, "Error", "خطأ", "OK", "موافق");
        }
        HideLoadingSpinner();
    } catch (e) {
        alert(e);
    }
}

function fail(error) {
    alert("fail");
    if (retries == 0) {
        retries++
        setTimeout(function () {
            onCapturePhoto(fileURI)
        }, 1000)
    } else {
        retries = 0;
        clearCache();
        HideLoadingSpinner();
        alert('Ups. Something wrong happens!');
    }
}

function CatchError(e, caller) {
    Log(e, caller)
    HideLoadingSpinner();
}

var assetURL = "";
var fileName = "";
var store;

function downloadFiles(ImagePath) {
    try {

        assetURL = ImagePath;
        var temparr = assetURL.toString().split('/');
        fileName = temparr[temparr.length - 1];
        var uri = encodeURI(assetURL);
        store = cordova.file.dataDirectory;
        //alert(window.rootFS.fullPath);
        var ImgLocation = window.rootFS.fullPath + "";//(store + fileName).toString();
        //   window.resolveLocalFileSystemURL(store + fileName, ImageDownLoaded, downloadAsset);
        var fileTransfer = new FileTransfer();
        fileTransfer.download(
          uri,
          ImgLocation,
          function (entry) {
              // alert("complete" + entry.toURL());
              console.log("download complete: " + entry.toURL());
          },
          function (error) {
              // alert("error " + error.source + " dd  " + error.target + " rr " + error.code);
              console.log("download error source " + error.source);
              console.log("download error target " + error.target);
              console.log("upload error code" + error.code);
          },
          false,
          {
              headers: {
                  "Authorization": "Basic dGVzdHVzZXJuYW1lOnRlc3RwYXNzd29yZA=="
              }
          }
          );
    } catch (e) {
        alert(e);
    }
}

function downloadAsset() {
    try {
        var fileTransfer = new FileTransfer();
        console.log("About to start transfer");
        fileTransfer.download(assetURL, store + fileName,
            function (entry) {
                //alert("success");
                console.log("Success!");
            },
            function (err) {
                //alert("fail" + err);
                console.log("Error");
                console.dir(err);
            });
    } catch (e) {
        alert(e);
    }
}

function ImageDownLoaded() {
    // alert("ImageDownLoaded");
}

function onCapturePhoto(fileURI) {
    // alert("Done >>>> " + fileURI);
    ShowLoadingSpinner();

    var name;

    var win = function (r) {
        //alert("win");
        //clearCache();
        retries = 0;
        // UpdateEmployer.UploadedFilesCount++;
        Globals.CamSuccess = true;
        //image uploaded succsfuly
        //alert & hide spinner
        // AlertFunction("File uploaded Succesfully", "تم رفع الملف بنجاح", "Success", "نجاح", "OK", "موافق");

        // $("#Complaints_CheckMark").css("display", "block");

        HideLoadingSpinner();


        if(camera_success_callback!=null){
            eval(camera_success_callback);    
        }
        
    }

    var fail = function (error) {
         //alert(error);
         if (retries == 0) {
            retries++
            setTimeout(function () {
                onCapturePhoto(fileURI)
            }, 1000)
        } else {
            retries = 0;
            clearCache();
            HideLoadingSpinner();
            if(camera_fail_callback!=null){
                eval(camera_fail_callback);    
            }
        }
    }
    var uuid = guid();
    var options = new FileUploadOptions();
    name=uuid + ".jpg";
    options.fileKey = "file";
    options.fileName = uuid + ".jpg";
    options.mimeType = "image/jpeg";
    options.params = {
        type: 'POST',
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        fileName: uuid + ".jpg"
    }; // if we need to send parameters to the server request
    var ft = new FileTransfer();
    var URL = GetOAuthURL(Globals.UploadServiceURI);
    ft.upload(fileURI, encodeURI(URL), win, fail, options);
}

function SetInitialPhoneValue(PhoneValue)
{
    var FormatedPhoneNo = '';
    try {
        if (PhoneValue == "" || PhoneValue == null || PhoneValue == undefined) {
            return "+971-";
        } else {
            FormatedPhoneNo = PhoneValue.replace(/\D/g, ''); // remove all non digit chars
            FormatedPhoneNo = FormatedPhoneNo.replace(' ', '');
            FormatedPhoneNo = "+" + FormatedPhoneNo;
            if (FormatedPhoneNo.length <= 4) {
                FormatedPhoneNo = FormatedPhoneNo + "-";
            } else {
                FormatedPhoneNo = FormatedPhoneNo.substring(0, 4) + "-" + FormatedPhoneNo.substring(4, FormatedPhoneNo.length);
            }
            return FormatedPhoneNo;
        }

    } catch (e) {

    }
}

function uploadDoc(fileId,success,fail) {

    var fileUpload = $("#" + fileId).get(0);
    var imagefile = fileUpload.files;
    if (imagefile.length < 1) {
            // AlertFunction("Please select Documents to upload", "من فضلك اختر الملفات المطلوبة لرفعها", "Error", "خطأ", "OK", "موافق");
            // return;
            console.log('replaced by cam');
        } else if (!validateExtension(imagefile[0].name)) {
            AlertFunction("Please upload following files (pdf,doc,docx,jpg,jpeg,png) only", "يجب تحميل الملفات التاليه (pdf,doc,docx,jpg,jpeg,png) فقط", "Error", "خطأ", "OK", "موافق");
            return;
        }else if (!GetUploadedDocsSize(fileId)) {
            AlertFunction("Max Size for documents is 7 MB", "الحد الاقصى للملفات 7 م ب", "Error", "خطأ", "OK", "موافق");
            return;
        }

        else {
            var uuid = guid();
            // UploadFiles(fileId, uuid, success,fail);
            UploadFiles(fileId, uuid, success,fail,null,null,false);
        }



    }
    function onUploadFromFileChooser(fileObject,InputID) {

        ShowLoadingSpinner();

        var win = function (r) {
            clearCache();
            retries = 0;
            HideLoadingSpinner();


            if(fileChooser_success_callback!=null){
                eval(fileChooser_success_callback);    
            }


        }

        var fail = function (error) {
        // alert(error);
        if (retries == 0) {
            retries++
            setTimeout(function () {
                onUploadFromFileChooser(fileObject,InputID)
            }, 1000)
        } else {
            retries = 0;
            clearCache();
            HideLoadingSpinner();
            if(fileChooser_fail_callback!=null){
                eval(fileChooser_fail_callback);    
            }
        }
    }
    var uuid = guid();
    var options = new FileUploadOptions();

    var Tempname = fileObject.filename.split('.');
    var   name = uuid + "." + Tempname[Tempname.length - 1];

    options.fileKey = "file";
    options.fileName = name;
    options.params = {
        type: 'POST',
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        fileName: name
    }; // if we need to send parameters to the server request
    var ft = new FileTransfer();
    var URL = GetOAuthURL(Globals.UploadServiceURI);
    ft.upload(fileObject.url, encodeURI(URL), win, fail, options);
}

function SetInitialPhoneValue(PhoneValue)
{
    var FormatedPhoneNo = '';
    try {
        if (PhoneValue == "" || PhoneValue == null || PhoneValue == undefined) {
            return "+971-";
        } else {
            FormatedPhoneNo = PhoneValue.replace(/\D/g, ''); // remove all non digit chars
            FormatedPhoneNo = FormatedPhoneNo.replace(' ', '');
            FormatedPhoneNo = "+" + FormatedPhoneNo;
            if (FormatedPhoneNo.length <= 4) {
                FormatedPhoneNo = FormatedPhoneNo + "-";
            } else {
                FormatedPhoneNo = FormatedPhoneNo.substring(0, 4) + "-" + FormatedPhoneNo.substring(4, FormatedPhoneNo.length);
            }
            return FormatedPhoneNo;
        }

    } catch (e) {

    }
}

function GetFormatedPhoneValue(PhoneValue) {
    // Value that should be sent to BE should be in the format +XXX-XXXXXXXX IF length is between (10 & 20)

    var FormatedPhoneNo = '';
    try {
        if (PhoneValue == "" || PhoneValue == null || PhoneValue == undefined) {
            return "";
        } else {
            FormatedPhoneNo = PhoneValue.replace(/\D/g, ''); // remove all non digit chars
            FormatedPhoneNo = FormatedPhoneNo.replace(' ', '');
            if (FormatedPhoneNo.length < 10 || FormatedPhoneNo.length > 18) {
                return "";
            } else {
                FormatedPhoneNo = "+" + FormatedPhoneNo;
                FormatedPhoneNo = FormatedPhoneNo.substring(0, 4) + "-" + FormatedPhoneNo.substring(4, FormatedPhoneNo.length);
            }
            
            return FormatedPhoneNo;
        }

    } catch (e) {

    }
}

function SetInitialPhoneNoValue(CountryCodeDDLID,PhoneNoInputID,PhoneValue) {
    try {
        if (PhoneValue == "" || PhoneValue == null || PhoneValue == undefined) {
            $("#" + CountryCodeDDLID).val("+971").trigger('change');
        } else {
            var CountryCode = "";
            var PhoneNo = "";
            if (PhoneValue.indexOf('-') > -1) {
                CountryCode = PhoneValue.split('-')[0];
                PhoneNo = PhoneValue.split('-')[1];
            } else {
                CountryCode = PhoneValue.substring(0, 4);
                PhoneNo = FormatedPhoneNo.substring(4, PhoneValue.length);
            }
            
            $("#" + CountryCodeDDLID).val(CountryCode).trigger('change');
            $("#" + PhoneNoInputID).val(PhoneNo.replace(/\D/g, ''));
        }

    } catch (e) {
        $("#" + CountryCodeDDLID).val("+971").trigger('change');
        $("#" + PhoneNoInputID).val('');
        // alert("SetInitialPhoneNoValue >>>>>>>" + e);
    }
}

function GetFormatedPhoneNoValue(CountryCodeDDLID, PhoneNoInputID) {

    var PhoneNo = '';
    var FullPhoneNo = '';
    try {
        PhoneNo = $("#" + PhoneNoInputID).val();

        if (PhoneNo == "" || PhoneNo == null || PhoneNo == undefined) {
            return "";
        } else {
            PhoneNo = PhoneNo.replace(/\D/g, '').replace(' ', '');
            if (PhoneNo.length < 8 || PhoneNo.length > 16) {
                return "";
            } else {
                FullPhoneNo = $("#" + CountryCodeDDLID).val() + "-" + PhoneNo; // remove all non digit chars
            }
            return FullPhoneNo;
        }

    } catch (e) {
        alert("GetFormatedPhoneNoValue >>>>" + e);
        return "";
    }
}

function AddCountryCodesOptionsToDropDown(CountryCodeDDLID) {

    var TempOptionsHTML = "<option value='+1'>+1</option>" +
    "<option value='+20'>+20</option>" +
    "<option value='+212'>+212</option>" +
    "<option value='+213'>+213</option>" +
    "<option value='+216'>+216</option>" +
    "<option value='+218'>+218</option>" +
    "<option value='+220'>+220</option>" +
    "<option value='+221'>+221</option>" +
    "<option value='+222'>+222</option>" +
    "<option value='+223'>+223</option>" +
    "<option value='+224'>+224</option>" +
    "<option value='+225'>+225</option>" +
    "<option value='+226'>+226</option>" +
    "<option value='+227'>+227</option>" +
    "<option value='+228'>+228</option>" +
    "<option value='+229'>+229</option>" +
    "<option value='+230'>+230</option>" +
    "<option value='+231'>+231</option>" +
    "<option value='+232'>+232</option>" +
    "<option value='+233'>+233</option>" +
    "<option value='+234'>+234</option>" +
    "<option value='+235'>+235</option>" +
    "<option value='+236'>+236</option>" +
    "<option value='+237'>+237</option>" +
    "<option value='+238'>+238</option>" +
    "<option value='+239'>+239</option>" +
    "<option value='+240'>+240</option>" +
    "<option value='+241'>+241</option>" +
    "<option value='+242'>+242</option>" +
    "<option value='+243'>+243</option>" +
    "<option value='+244'>+244</option>" +
    "<option value='+245'>+245</option>" +
    "<option value='+248'>+248</option>" +
    "<option value='+249'>+249</option>" +
    "<option value='+250'>+250</option>" +
    "<option value='+251'>+251</option>" +
    "<option value='+252'>+252</option>" +
    "<option value='+253'>+253</option>" +
    "<option value='+254'>+254</option>" +
    "<option value='+255'>+255</option>" +
    "<option value='+256'>+256</option>" +
    "<option value='+257'>+257</option>" +
    "<option value='+258'>+258</option>" +
    "<option value='+260'>+260</option>" +
    "<option value='+261'>+261</option>" +
    "<option value='+262'>+262</option>" +
    "<option value='+263'>+263</option>" +
    "<option value='+264'>+264</option>" +
    "<option value='+265'>+265</option>" +
    "<option value='+266'>+266</option>" +
    "<option value='+267'>+267</option>" +
    "<option value='+268'>+268</option>" +
    "<option value='+269'>+269</option>" +
    "<option value='+27'>+27</option>" +
    "<option value='+290'>+290</option>" +
    "<option value='+291'>+291</option>" +
    "<option value='+297'>+297</option>" +
    "<option value='+298'>+298</option>" +
    "<option value='+299'>+299</option>" +
    "<option value='+30'>+30</option>" +
    "<option value='+31'>+31</option>" +
    "<option value='+32'>+32</option>" +
    "<option value='+33'>+33</option>" +
    "<option value='+34'>+34</option>" +
    "<option value='+350'>+350</option>" +
    "<option value='+351'>+351</option>" +
    "<option value='+352'>+352</option>" +
    "<option value='+353'>+353</option>" +
    "<option value='+354'>+354</option>" +
    "<option value='+355'>+355</option>" +
    "<option value='+356'>+356</option>" +
    "<option value='+357'>+357</option>" +
    "<option value='+358'>+358</option>" +
    "<option value='+359'>+359</option>" +
    "<option value='+36'>+36</option>" +
    "<option value='+370'>+370</option>" +
    "<option value='+371'>+371</option>" +
    "<option value='+372'>+372</option>" +
    "<option value='+373'>+373</option>" +
    "<option value='+374'>+374</option>" +
    "<option value='+375'>+375</option>" +
    "<option value='+376'>+376</option>" +
    "<option value='+377'>+377</option>" +
    "<option value='+378'>+378</option>" +
    "<option value='+380'>+380</option>" +
    "<option value='+381'>+381</option>" +
    "<option value='+382'>+382</option>" +
    "<option value='+385'>+385</option>" +
    "<option value='+386'>+386</option>" +
    "<option value='+387'>+387</option>" +
    "<option value='+389'>+389</option>" +
    "<option value='+39'>+39</option>" +
    "<option value='+40'>+40</option>" +
    "<option value='+41'>+41</option>" +
    "<option value='+420'>+420</option>" +
    "<option value='+421'>+421</option>" +
    "<option value='+423'>+423</option>" +
    "<option value='+43'>+43</option>" +
    "<option value='+44'>+44</option>" +
    "<option value='+45'>+45</option>" +
    "<option value='+46'>+46</option>" +
    "<option value='+47'>+47</option>" +
    "<option value='+48'>+48</option>" +
    "<option value='+49'>+49</option>" +
    "<option value='+500'>+500</option>" +
    "<option value='+501'>+501</option>" +
    "<option value='+502'>+502</option>" +
    "<option value='+503'>+503</option>" +
    "<option value='+504'>+504</option>" +
    "<option value='+505'>+505</option>" +
    "<option value='+506'>+506</option>" +
    "<option value='+507'>+507</option>" +
    "<option value='+508'>+508</option>" +
    "<option value='+509'>+509</option>" +
    "<option value='+51'>+51</option>" +
    "<option value='+52'>+52</option>" +
    "<option value='+53'>+53</option>" +
    "<option value='+54'>+54</option>" +
    "<option value='+55'>+55</option>" +
    "<option value='+56'>+56</option>" +
    "<option value='+57'>+57</option>" +
    "<option value='+58'>+58</option>" +
    "<option value='+590'>+590</option>" +
    "<option value='+591'>+591</option>" +
    "<option value='+592'>+592</option>" +
    "<option value='+593'>+593</option>" +
    "<option value='+595'>+595</option>" +
    "<option value='+597'>+597</option>" +
    "<option value='+598'>+598</option>" +
    "<option value='+599'>+599</option>" +
    "<option value='+60'>+60</option>" +
    "<option value='+61'>+61</option>" +
    "<option value='+62'>+62</option>" +
    "<option value='+63'>+63</option>" +
    "<option value='+64'>+64</option>" +
    "<option value='+65'>+65</option>" +
    "<option value='+66'>+66</option>" +
    "<option value='+670'>+670</option>" +
    "<option value='+672'>+672</option>" +
    "<option value='+673'>+673</option>" +
    "<option value='+674'>+674</option>" +
    "<option value='+675'>+675</option>" +
    "<option value='+676'>+676</option>" +
    "<option value='+677'>+677</option>" +
    "<option value='+678'>+678</option>" +
    "<option value='+679'>+679</option>" +
    "<option value='+680'>+680</option>" +
    "<option value='+681'>+681</option>" +
    "<option value='+682'>+682</option>" +
    "<option value='+683'>+683</option>" +
    "<option value='+685'>+685</option>" +
    "<option value='+686'>+686</option>" +
    "<option value='+687'>+687</option>" +
    "<option value='+688'>+688</option>" +
    "<option value='+689'>+689</option>" +
    "<option value='+690'>+690</option>" +
    "<option value='+691'>+691</option>" +
    "<option value='+692'>+692</option>" +
    "<option value='+7'>+7</option>" +
    "<option value='+81'>+81</option>" +
    "<option value='+82'>+82</option>" +
    "<option value='+84'>+84</option>" +
    "<option value='+850'>+850</option>" +
    "<option value='+852'>+852</option>" +
    "<option value='+853'>+853</option>" +
    "<option value='+855'>+855</option>" +
    "<option value='+856'>+856</option>" +
    "<option value='+86'>+86</option>" +
    "<option value='+870'>+870</option>" +
    "<option value='+880'>+880</option>" +
    "<option value='+886'>+886</option>" +
    "<option value='+90'>+90</option>" +
    "<option value='+91'>+91</option>" +
    "<option value='+92'>+92</option>" +
    "<option value='+93'>+93</option>" +
    "<option value='+94'>+94</option>" +
    "<option value='+95'>+95</option>" +
    "<option value='+960'>+960</option>" +
    "<option value='+961'>+961</option>" +
    "<option value='+962'>+962</option>" +
    "<option value='+963'>+963</option>" +
    "<option value='+964'>+964</option>" +
    "<option value='+965'>+965</option>" +
    "<option value='+966'>+966</option>" +
    "<option value='+967'>+967</option>" +
    "<option value='+968'>+968</option>" +
    "<option value='+970'>+970</option>" +
    "<option value='+971' selected>+971</option>" +
    "<option value='+972'>+972</option>" +
    "<option value='+973'>+973</option>" +
    "<option value='+974'>+974</option>" +
    "<option value='+975'>+975</option>" +
    "<option value='+976'>+976</option>" +
    "<option value='+977'>+977</option>" +
    "<option value='+98'>+98</option>" +
    "<option value='+992'>+992</option>" +
    "<option value='+993'>+993</option>" +
    "<option value='+994'>+994</option>" +
    "<option value='+995'>+995</option>" +
    "<option value='+996'>+996</option>" +
    "<option value='+998'>+998</option>";

    try {
        $("#" + CountryCodeDDLID).html(TempOptionsHTML);
    } catch (e) {
        alert("AddCountryCodesOptionsToDropDown >>>>" + e);
    }
}
