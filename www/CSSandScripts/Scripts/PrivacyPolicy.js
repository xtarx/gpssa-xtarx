﻿var Privacy = {

    PrivacyResp: '',

    LoadPrivacyContent: function () {

        if (localStorage.getItem("Privacy" + Language) == null || localStorage.getItem("Privacy" + Language) == undefined) {
            var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
            URL = URL.replace("#PageName#", 'Privacy').replace("#Lang#", Language);

            CallWebService(URL, "Privacy.HandlePrivacyCallSuccess(response)", "", "", "", false, true);
        }
        else {
            var Resp = localStorage.getItem("Privacy" + Language);
            var Obj = JSON.parse(Resp);
            Privacy.PrivacyResp = Obj.Page;

            var URL = Globals.ServicesURI_Test + "validate/item/module/pages/itemid/#PageName#/lastupdate/#LastUpdate#/language/#Lang#";
            URL = URL.replace("#PageName#", 'Privacy').replace("#LastUpdate#", Privacy.PrivacyResp.Modified).replace("#Lang#", Language);
            CallWebService(URL, "Privacy.HandleValidatePrivacyCallSuccess(response)", "Privacy.HandleValidatePrivacyCallFailure()", "", "", true, false, false);
        }
    },

    HandlePrivacyDocReady: function () {
        SetHeaderTitle("Privacy Policy", "سياسة الخصوصية");
        $("#PrivacyPolicyContent").html(Privacy.PrivacyResp.Content);
    },


    HandleValidatePrivacyCallSuccess: function (response) {
        if (response != null) {
            HideLoadingSpinner();
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("PrivacyPolicy", 'Privacy.HandlePrivacyDocReady()', false, true);
                }
                else {
                    localStorage.removeItem("Privacy" + Language);
                    var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
                    URL = URL.replace("#PageName#", 'Privacy').replace("#Lang#", Language);

                    CallWebService(URL, "Privacy.HandlePrivacyCallSuccess(response)", "", "", "", true, false);
                }
            }
        }
    },

    HandleValidatePrivacyCallFailure: function () {
        var Resp = localStorage.getItem("Privacy" + Language);
        var obj = JSON.parse(Resp);
        Privacy.PrivacyResp = obj.Page;
        LoadPageContent("PrivacyPolicy", 'Privacy.HandlePrivacyDocReady()', false, true);
    },

    HandlePrivacyCallSuccess: function (response) {
        try {
            if (response != null) {
                HideLoadingSpinner();
                localStorage.setItem("Privacy" + Language, JSON.stringify(response));
                var PrivacyResp = response;//JSON.parse(response);
                if (PrivacyResp.Message.Code == 0) {
                    Privacy.PrivacyResp = PrivacyResp.Page;
                    $("#PrivacyPolicyContent").html(PrivacyResp.Page.Content);
                    $("#PrivacyPolicyContent").height($(".ContentClass").height() - 60);
                }
                else {
                    AlertFunction(PrivacyResp.Message.Body, PrivacyResp.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
        } catch (e) {
        }
    }
};