﻿var AboutGPSSA = {
    AboutGPSSAResp: "",
    CurrentSelectedTab: '',
    CurrentSelectedPageName: '',

    LoadAboutGPSSAPage: function () {

        if (localStorage.getItem("AboutGPSSA" + Language) == null || localStorage.getItem("AboutGPSSA" + Language) == undefined) {
            var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
            URL = URL.replace("#PageName#", 'AboutGPSSA').replace("#Lang#", Language);

            CallWebService(URL, "AboutGPSSA.HandleGetAboutUsContentSuccess(response)", "", "", "", true, false);
        }
        else {
            var Resp = localStorage.getItem("AboutGPSSA" + Language);
            var Obj = JSON.parse(Resp);
            AboutGPSSA.AboutGPSSAResp = Obj.Page;

            var URL = Globals.ServicesURI_Test + "validate/item/module/pages/itemid/#PageName#/lastupdate/#LastUpdate#/language/#Lang#";
            URL = URL.replace("#PageName#", 'AboutGPSSA').replace("#LastUpdate#", AboutGPSSA.AboutGPSSAResp.Modified).replace("#Lang#", Language);
            CallWebService(URL, "AboutGPSSA.HandleValidateAboutusCallSuccess(response)", "AboutGPSSA.HandleValidateAboutusCallFailure()", "", "", true, false, false);
        }
    },


    HandleValidateAboutusCallSuccess: function (response) {
        if (response != null) {
            HideLoadingSpinner();
            var RespObj = response; //JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("AboutGPSSA", 'AboutGPSSA.HandleAboutGPSSADocReady()', false, true, true, true);
                }
                else {
                    localStorage.removeItem("AboutGPSSA" + Language);
                    var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
                    URL = URL.replace("#PageName#", "AboutGPSSA").replace("#Lang#", Language);

                    CallWebService(URL, "AboutGPSSA.HandleGetAboutUsContentSuccess(response)", "", "", "", true, false);
                }
            }
        }
    },

    HandleValidateAboutusCallFailure: function () {
        var Resp = localStorage.getItem("AboutGPSSA" + Language);
        var obj = JSON.parse(Resp);
        AboutGPSSA.AboutGPSSAResp = obj.Page;
        LoadPageContent("AboutGPSSA", 'AboutGPSSA.HandleAboutGPSSADocReady()', false, true, true, true);
    },


    HandleGetAboutUsContentSuccess: function (response) {
        if (response != null) {
            HideLoadingSpinner();
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                localStorage.setItem("AboutGPSSA" + Language, JSON.stringify(response));
                AboutGPSSA.AboutGPSSAResp = RespObj.Page;
                LoadPageContent("AboutGPSSA", 'AboutGPSSA.HandleAboutGPSSADocReady()', false, true, true, true);
            }
            else {
                AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    HandleAboutGPSSADocReady: function () {

        SetHeaderTitle("About GPSSA", "عن الهيئة");
        BindEvents('#AddResponsiblePerson_AddBtn', 'click', 'AddResponsiblePerson.HandleAddResponsiblePersonBtnClicked()');

        $("#AboutGPSSA_OverViewTab_Content").height($(".ContentClass").height() - 60);
        $("#AboutGPSSA_PresidentStatementTab_Content").height($(".ContentClass").height() - 60);
        $("#AboutGPSSA_VisionTab_Content").height($(".ContentClass").height() - 60);

        $('.AboutGPSSAContent').hide();
        $(".AboutGPSSATabs").removeClass("ActiveTabClass");
        $(".AboutGPSSATabs").addClass("TabClass");
        $("#AboutGPSSA_OverViewTab").addClass("ActiveTabClass");

        $("#AboutGPSSA_OverViewTab_Content").html(AboutGPSSA.AboutGPSSAResp.Content);
        $("#AboutGPSSA_OverViewTab_Content").show();

    },

    HandleAboutGPSSATabClicked: function (id) {

        AboutGPSSA.CurrentSelectedTab = id;

        $('.AboutGPSSAContent').hide();
        $(".AboutGPSSATabs").removeClass("ActiveTabClass");
        $(".AboutGPSSATabs").addClass("TabClass");
        $("#" + id).addClass("ActiveTabClass");

        if (id == "AboutGPSSA_OverViewTab") {
            AboutGPSSA.CurrentSelectedPageName = "AboutGPSSA";
        }
        else if (id == "AboutGPSSA_PresidentStatementTab") {
            AboutGPSSA.CurrentSelectedPageName = "PresidentStatement";
        }
        else {
            AboutGPSSA.CurrentSelectedPageName = "Vision";
        }

        var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";

        if (localStorage.getItem(AboutGPSSA.CurrentSelectedPageName + Language) == null || localStorage.getItem(AboutGPSSA.CurrentSelectedPageName + Language) == undefined) {
            URL = URL.replace("#PageName#", AboutGPSSA.CurrentSelectedPageName).replace("#Lang#", Language);

            CallWebService(URL, "AboutGPSSA.HandleAboutusTabsCallSuccess(response)", "", "", "", true, false);
        }
        else {
            var Resp = localStorage.getItem(AboutGPSSA.CurrentSelectedPageName + Language);
            var Obj = JSON.parse(Resp);
            AboutGPSSA.AboutGPSSAResp = Obj.Page;

            var URL = Globals.ServicesURI_Test + "validate/item/module/pages/itemid/#PageName#/lastupdate/#LastUpdate#/language/#Lang#";
            URL = URL.replace("#PageName#", AboutGPSSA.CurrentSelectedPageName).replace("#LastUpdate#", AboutGPSSA.AboutGPSSAResp.Modified).replace("#Lang#", Language);
            CallWebService(URL, "AboutGPSSA.HandleValidateAboutusTabsCallSuccess(response)", "AboutGPSSA.HandleValidateAboutusTabsCallFailure()", "", "", true, false, false);
        }
    },

    HandleAboutusTabsCallSuccess: function (response) {
        try {
            if (response != null) {
                HideLoadingSpinner();
                var AbouGPSArespObj = response;//JSON.parse(response);
                if (AbouGPSArespObj.Message.Code == 0) {
                    localStorage.setItem(AboutGPSSA.CurrentSelectedPageName + Language, JSON.stringify(response));
                    AboutGPSSA.AboutGPSSAResp = AbouGPSArespObj.Page;
                    if (AboutGPSSA.CurrentSelectedTab == "AboutGPSSA_PresidentStatementTab") {
                        $("#" + AboutGPSSA.CurrentSelectedTab + "_Content").html($('#PresidentStatementImg').html());
                        $("#" + AboutGPSSA.CurrentSelectedTab + "_Content").append(AbouGPSArespObj.Page.Content);
                    }
                    else {
                        $("#" + AboutGPSSA.CurrentSelectedTab + "_Content").html(AbouGPSArespObj.Page.Content);
                    }
                    $("#" + AboutGPSSA.CurrentSelectedTab + "_Content").show();
                    HideLoadingSpinner();
                }
                else {
                    AlertFunction(AbouGPSArespObj.Message.Body, AbouGPSArespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
        } catch (e) {
            //alert(e);
        }
    },

    HandleValidateAboutusTabsCallSuccess: function (response) {
        HideLoadingSpinner();
        if (response != null) {
            HideLoadingSpinner();
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    var obj = JSON.parse(localStorage.getItem(AboutGPSSA.CurrentSelectedPageName + Language));
                    AboutGPSSA.HandleAboutusTabsCallSuccess(obj);
                }
                else {
                    localStorage.removeItem(AboutGPSSA.CurrentSelectedPageName + Language);
                    var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
                    URL = URL.replace("#PageName#", AboutGPSSA.CurrentSelectedPageName).replace("#Lang#", Language);

                    CallWebService(URL, "AboutGPSSA.HandleAboutusTabsCallSuccess(response)", "", "", "", true, false);
                }
            }
        }
    },

    HandleValidateAboutusTabsCallFailure: function () {
        var obj = JSON.parse(localStorage.getItem(AboutGPSSA.CurrentSelectedPageName + Language));
        AboutGPSSA.HandleAboutusTabsCallSuccess(obj);
        HideLoadingSpinner();
    }
};
