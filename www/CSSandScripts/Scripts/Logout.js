﻿var Logout = {

  CallLogout: function () {

    ConfirmationAlert("Are you sure you want to logout?", "هل انت متأكد من انك تريد الخروج؟", "Attention", "تنبيه", "OK", "موافق", "Cancel", "إلغاء", "Logout.HandleLogout()");
  },
  clearPinCodeSession: function () {


   Globals.UserLoggedIn = false;
   Globals.UserServicesArr = null;
   localStorage.removeItem("UserServicesArr");
   localStorage.setItem("UserLoggedInLocalStorage", false);
   localStorage.removeItem("UserServicesArr");


 },
 HandleLogout: function () {

        // Globals.UserLoggedIn = false;
        localStorage.setItem("UserLoggedInLocalStorage", false);

        Globals.UserLoggedIn = false;
        Globals.UserName = '';
        StartUp.LoadStartuUp();
        Globals.UserServicesArr = null; 
        localStorage.removeItem("UserServicesArr");
        localStorage.removeItem("Username");
        localStorage.removeItem("username");
        localStorage.removeItem("Password");
        localStorage.removeItem("LoginObj");
        //tamer presistent login user name case (old username)
        Login.Username=null;
        $("#Login2_Username").html("");

        $("#LoginTR").show();
        $("#LogOutTR").hide();
        

      }
    };