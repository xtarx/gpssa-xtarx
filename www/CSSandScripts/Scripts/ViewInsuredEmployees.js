﻿var InsuredEmployees = {

    GetInsuredEmployeesResp: null,
    PageNoDropDownDrawn: false,
    InsuredEmployeesReqObj: null,
    SearchBy: 1,
    isFromServices: false,

    LoadInsuredEmployees: function (isFromServices) {
        InsuredEmployees.isFromServices = isFromServices;
        var data = {};
        data.PageNo = 0;
        data.SuccessFunction = "InsuredEmployees.HandleGetInsuredEmployeersCallSuccess(response)";
        InsuredEmployees.GetInsuredData(data);
    },

    GetInsuredData: function (Reqdata) {
        var URL = Globals.ServicesURI_Test + "view/employees/language/#Lang#";
        URL = URL.replace("#Lang#", Language);
        InsuredEmployees.InsuredEmployeesReqObj = InsuredEmployeerClass;
        InsuredEmployees.InsuredEmployeesReqObj.PageNumber = Reqdata.PageNo;
        InsuredEmployees.InsuredEmployeesReqObj.EmployerRowId = Globals.EmployerRowID;
        if ($.trim($("#ViewInsuredEmployees_Name").val()) == "") {
            InsuredEmployees.InsuredEmployeesReqObj.View = "ViewInsuredForMobile";
        }
        else {
            InsuredEmployees.InsuredEmployeesReqObj.View = "SearchInsuredForMobile";
        }

        var data = JSON.stringify(InsuredEmployees.InsuredEmployeesReqObj);
        Log(data, "GetInsuredData");
        CallWebService(URL, Reqdata.SuccessFunction, "", data, "POST", true, true);
    },

    HandleGetInsuredEmployeersCallSuccess: function (response) {
        try {
            Log(response, "response");
            if (response != null) {
                if (response.Message != null) {
                    if (response.Message.Code == 0) {
                        InsuredEmployees.GetInsuredEmployeesResp = null;
                        InsuredEmployees.GetInsuredEmployeesResp = response;
                        if (InsuredEmployees.isFromServices == true)
                        {
                            LoadPageContent("ViewInsuredEmployees", 'InsuredEmployees.HandleInsuredEmployeesDocReady()', true, true, true, false, 'ServicesList.LoadServicesPage()');
                        }
                        else
                        {
                            LoadPageContent("ViewInsuredEmployees", 'InsuredEmployees.HandleInsuredEmployeesDocReady()', true, true, true);
                        }
                    }
                    else {
                        AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                    }
                }
            }
        } catch (e) {
            Log(e, "Exception");
        }
    },

    HandleInsuredEmployeesDocReady: function () {
        try {

            SetHeaderTitle("View Insured Employees", "عرض الموظفين المؤمن عليهم");
            BindEvents('#SearchByDropDown', 'change', 'InsuredEmployees.HandleSearchByDropDownChanged()');

            
            if (!InsuredEmployees.PageNoDropDownDrawn) {
                BindEvents('#ViewInsuredEmployees_PageNoDropDown', 'change', 'InsuredEmployees.HandlePageNoDropDownChanged()');
                $("#ViewInsuredEmployees_PageNoDropDown").html(InsuredEmployees.CreateNumbersDropDown(InsuredEmployees.GetInsuredEmployeesResp.RecordSetTotalCount));
            }

            BindEvents('#SearchBtn', 'click', 'InsuredEmployees.Search()');
            BindEvents('#GetAllData', 'click', 'InsuredEmployees.GetAllData()');
            
            $("#InsuredEmployeesList").html();
            Log(JSON.stringify(InsuredEmployees.GetInsuredEmployeesResp), "GetInsuredEmployeesResp");
            
            $("#InsuredEmployeesList").html($("#InsuredEmployeesTempl").tmpl(InsuredEmployees.GetInsuredEmployeesResp.Contacts));
            $("#InsuredEmployeesList tr:last").remove();
           // $("#InsuredEmployeesList").height(((DeviceHeight / 3) * 2) - 30);
            InsuredEmployees.PageNoDropDownDrawn = true;

        } catch (e) {
        }
    },
    Search: function () {
        InsuredEmployees.HandleSearchIconClicked();
    },
    GetAllData: function () {
        InsuredEmployees.SearchBy = 0;
        $("#ViewInsuredEmployees_Name").val('');
        InsuredEmployees.HandleSearchIconClicked();
    },
    HandleSearchByDropDownChanged: function () {
        InsuredEmployees.SearchBy = $('#SearchByDropDown').val();
        $("#ViewInsuredEmployees_Name").val('');
        
    },
    HandleSearchIconClicked: function () {
        try { 
            var URL = Globals.ServicesURI_Test + "view/employees/language/#Lang#";
            URL = URL.replace("#Lang#", Language);
            InsuredEmployees.InsuredEmployeesReqObj.PageNumber = "0";
            InsuredEmployees.PageNoDropDownDrawn = false;
            InsuredEmployees.InsuredEmployeesReqObj.View = "ViewInsuredForMobile";
            if ($.trim($("#ViewInsuredEmployees_Name").val()) == "") {   
                InsuredEmployees.InsuredEmployeesReqObj.FullName = null;
                InsuredEmployees.InsuredEmployeesReqObj.EmiratesId = null;
            }
            else { 
                if (InsuredEmployees.SearchBy == 1) {
                    InsuredEmployees.InsuredEmployeesReqObj.FullName = $.trim($("#ViewInsuredEmployees_Name").val());
                    InsuredEmployees.InsuredEmployeesReqObj.EmiratesId = null;
                }
                else if (InsuredEmployees.SearchBy == 2) {
                    InsuredEmployees.InsuredEmployeesReqObj.EmiratesId = $.trim($("#ViewInsuredEmployees_Name").val());
                    InsuredEmployees.InsuredEmployeesReqObj.FullName = null;
                }
                else {
                    InsuredEmployees.InsuredEmployeesReqObj.FullName = $.trim($("#ViewInsuredEmployees_Name").val());
                }
            }

            var data = JSON.stringify(InsuredEmployees.InsuredEmployeesReqObj);
            Log(data, "data");
            CallWebService(URL, 'InsuredEmployees.HandlePagingSuccess(response)', "", data, "POST", true, true);

        } catch (e) {

        }
    },

    CreateNumbersDropDown: function (NO) {

        var Temp = "<option>#NO#</option>";
        var AllHTML = "";
        var counter = Math.ceil(NO / 10);

        for (var i = 0; i < counter; i++) {
            AllHTML += Temp.replace("#NO#", i+1);
            Temp = "<option>#NO#</option>";
        }

        return AllHTML;
    },

    HandlePageNoDropDownChanged: function () {

        var val = $("#ViewInsuredEmployees_PageNoDropDown").val();
        var data = {};

        // data.PageNo = val-1;
        data.PageNo = val;
        data.SuccessFunction = "InsuredEmployees.HandlePagingSuccess(response)";
        InsuredEmployees.GetInsuredData(data);
    },

    HandlePagingSuccess: function (response) {
        try {
            Log(response, "response");
            if (response != null) {
                if (response.Message != null) {
                    if (response.Message.Code == 0) {
                        InsuredEmployees.GetInsuredEmployeesResp = null;
                        InsuredEmployees.GetInsuredEmployeesResp = response;
                        InsuredEmployees.HandleInsuredEmployeesDocReady();
                    }
                    else {
                        AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                    }
                }
            }
        } catch (e) {
            Log(e, "Exception");
        }
    }
};