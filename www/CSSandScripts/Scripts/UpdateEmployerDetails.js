﻿var UpdateEmployer = {

    EmployerDetailsObj: null,
    SelectedVal: 0,
    RequiredDocLength: 0,
    UploadedFilesName: new Array(),
    UploadedFilesCount: 0,
    uploadButtonPassed: false,
    UploadCamFailed: false,
    UploadDocFailed: false,
    didCamUpload: false,
    isFromServices: false,
    currentFilsIDs: [],
    CurrentFileCheckerIDs: [],
    allDocsHTML: "",
    SuccessMsg_AR: "لقد تم تسجيل طلبك بنجاح, من أجل متابعة حالة طلبك، استخدم هذا الرقم :",
    SuccessMsg: "Your request has been registered successfully, in order to track the status of your application, use this case number: ",
    LoadUpdateEmployer: function (isFromServices) {
        UpdateEmployer.isFromServices = isFromServices;
        var URL = Globals.ServicesURI_Test + "view/employer/employerrowid/#EmpRowID#/language/#Lang#";
        URL = URL.replace("#EmpRowID#", Globals.EmployerRowID).replace("#Lang#", Language);
        CallWebService(URL, "UpdateEmployer.HandleGetEmployerDetailsCallSuucess(response)", "UpdateEmployer.HandleGetEmployerDetailsCallFailure()", "", "", true, false);
    },

    HandleGetEmployerDetailsCallFailure: function () {

    },
    EmployerDetailsUploadsFormHide: function () {

        $("#reqDocsToHide").hide();
        $("#reqDocsToHide_BR").hide();
        $("#UpdateEmployerDetails_UploadBtn").hide();
        $("#UpdateEmployerDetails_SaveBtn").hide();
        $("#UpdateEmployerDetails_RequiredDoc").hide();
        $("#AllFilesInOne_TR").hide();

    },
    EmployerDetailsUploadsFormShow: function () {

        $("#reqDocsToHide").show();
        $("#reqDocsToHide_BR").show();
        $("#UpdateEmployerDetails_UploadBtn").show();
        $("#UpdateEmployerDetails_SaveBtn").show();
        $("#UpdateEmployerDetails_RequiredDoc").show();
        $("#AllFilesInOne_TR").show();

    },
    resetCountsandVars: function () {
        try {

        // $(".requiredTick").css("visibility", "hidden");
        $(".requiredTick").css("display", "none");
        UpdateEmployer.UploadedFilesCount = 0;
        UpdateEmployer.uploadButtonPassed = false;
        UpdateEmployer.CurrentFileCheckerIDs.length = 0;
        UpdateEmployer.UploadedFilesName.length = 0;
        UpdateEmployer.EmployerDetailsObj.Header.AttachedFiles.length = 0;
        UpdateEmployer.UploadDocFailed = false;
        UpdateEmployer.UploadCamFailed = false;

       } catch (e) {

        }

    },

    HandleGetEmployerDetailsCallSuucess: function (response) {
        if (response != null) {
            var RespObj = response; // JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                UpdateEmployer.EmployerDetailsObj = RespObj.Employer;
                if (UpdateEmployer.EmployerDetailsObj.Header == null) {
                    UpdateEmployer.EmployerDetailsObj.Header = new Object();
                }
                UpdateEmployer.EmployerDetailsObj.Header.AttachedFiles = new Array();
                if (UpdateEmployer.isFromServices == true) {
                    LoadPageContent("UpdateEmployerDetails", 'UpdateEmployer.HandleUpdateEmployerDocReady()', false, true, true, false, 'ServicesList.LoadServicesPage()');
                } else {
                    LoadPageContent("UpdateEmployerDetails", 'UpdateEmployer.HandleUpdateEmployerDocReady()', false, true);
                }
            } else {
                HideLoadingSpinner();
                AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    HandleUpdateEmployerDocReady: function () {
        $(".UpdateNameDropDown").hide();
        UpdateEmployer.EmployerDetailsUploadsFormHide();
        //reset vars n counts
        UpdateEmployer.resetCountsandVars();

        $('#AllFilesInOne').attr('checked', false);
        var val = $("#UpdateEmployerDetails_ReasonsDropDown").val();

        var val_int = parseInt(val);
        if (val_int > -1 && val_int < 6) {
            UpdateEmployer.EmployerDetailsUploadsFormShow();
        }

        SetHeaderTitle("Update Employer’s Details", "طلب تحديث بيانات صاحب العمل");
        BindEvents('#UpdateEmployerDetails_ReasonsDropDown', 'change', 'UpdateEmployer.HandleReasonsDropDownChanged()');

        // alert(UpdateEmployer.EmployerDetailsObj.Contacts[3].FirstName);
        BindEvents('#UpdateEmployerDetails_list_persons_DropDown', 'change', 'UpdateEmployer.HandlePersonsDropDownChanged()');
        BindEvents('#UpdateEmployerDetails_SaveBtn', 'click', 'UpdateEmployer.HandleSaveBtnClicked()');
        BindEvents('#UpdateEmployerDetails_UploadBtn', 'click', 'UpdateEmployer.HandleUploadDocBtnClicked()');

        $('#UpdateEmployerDetails_FirstName').on('input', function (e) {
            isArabic(UpdateEmployerDetails_FirstName);
        });
        $('#UpdateEmployerDetails_SecondName').on('input', function (e) {
            isArabic(UpdateEmployerDetails_SecondName);
        });
        $('#UpdateEmployerDetails_ThirdName').on('input', function (e) {
            isArabic(UpdateEmployerDetails_ThirdName);
        });
        $('#UpdateEmployerDetails_FourthName').on('input', function (e) {
            isArabic(UpdateEmployerDetails_FourthName);
        });

        $('#UpdateEmployerDetails_EntityNameAR').on('input', function (e) {
            // console.log('change with val '+$('#UpdateEmployerDetails_EntityNameAR').val());
            isArabic(UpdateEmployerDetails_EntityNameAR);
        });

        $('#UpdateEmployerDetails_EntityName').on('input', function (e) {
            isEnglish(UpdateEmployerDetails_EntityName);
        });

        /*$("#UpdateEmployerDetails_FaxNumber").mask("+XXX-X-XXXXXXX", {
            autoclear: false
        }).val("+971");*/
        //$("#UpdateEmployerDetails_FaxNumber").val("+971-");

        /*$("#UpdateEmployerDetails_PhoneNumber").mask("+XXX-XX-XXXXXXX", {
            autoclear: false
        }).val("+971");*/
        //$("#UpdateEmployerDetails_PhoneNumber").val("+971-");

        /*$("#UpdateEmployerDetails_EntityMobile").mask("+XXX-XX-XXXXXXX", {
            autoclear: false
        }).val("+971");*/
        //$("#UpdateEmployerDetails_EntityMobile").val("+971-");

        /*$("#UpdateEmployerDetails_LandLine").mask("+XXX-XX-XXXXXXX", {
            autoclear: true
        }).val("+971");*/

        //$("#UpdateEmployerDetails_LandLine").val("+971-");

        AddCountryCodesOptionsToDropDown("UpdateEmployer_Mobile_CountryCodeDDL");
        $("#UpdateEmployerDetails_EntityMobile").val("");
        AddCountryCodesOptionsToDropDown("UpdateEmployer_Phone_CountryCodeDDL");
        $("#UpdateEmployerDetails_PhoneNumber").val("");
        AddCountryCodesOptionsToDropDown("UpdateEmployer_Fax_CountryCodeDDL");
        $("#UpdateEmployerDetails_FaxNumber").val("");
        AddCountryCodesOptionsToDropDown("UpdateEmployer_LandLine_CountryCodeDDL");
        $("#UpdateEmployerDetails_LandLine").val("");

        $("#UpdateEmployerDetails_EntityEmiratesID").mask("784-XXXX-XXXXXXX-X");
        // Detect changes
        // $("#allFilesInOne_CheckMark").css("display", "hide");


        try {

            for (var i = 0; i < UpdateEmployer.CurrentFileCheckerIDs.length; i++) {
                $(UpdateEmployer.CurrentFileCheckerIDs[i]).hide();
            }
        } catch (e) {

        }
    },

    smartUploadsValidation: function () {

        //smart validation
        if (document.getElementById("AllFilesInOne").checked) {
                if ($("#allFilesInOne_CheckMark").css("display") == "block") {


                    console.log('tamam');
                } else {
                //fail
                // AlertFunction("Please upload all required documents", "من فضلك ارفع جميع الملفات المطلوبة", "Error", "خطأ", "OK", "موافق");
                return false;
            }


        } else {

            if (UpdateEmployer.CurrentFileCheckerIDs.length == 0) {
                return false;
            }
            for (var i = 0; i < UpdateEmployer.CurrentFileCheckerIDs.length; i++) {

                    if ($(UpdateEmployer.CurrentFileCheckerIDs[i]).css("display") == "block") {
                        console.log('tamam');
                    } else {
                    //fail
                    // AlertFunction("Please Upload all required documents", "من فضلك ارفع جميع الملفات المطلوبة", "Error", "خطأ", "OK", "موافق");
                    return false;
                }
            }

        }
        return true;

        //smart validation




    },
    HandlePersonsDropDownChanged: function () {
        // alert("drop down changed");
        var i = parseInt($("#UpdateEmployerDetails_list_persons_DropDown").val());


        if (i > -1) {
            //disable all vals
            $(".UpdateName").show();
            UpdateEmployer.EmployerDetailsUploadsFormShow();

            // $("#reqDocsToHide").show();
            // $("#UpdateEmployerDetails_UploadBtn").show();
            // $("#UpdateEmployerDetails_SaveBtn").show();        
            // $("#UpdateEmployerDetails_RequiredDoc").show();
            // $("#AllFilesInOne_TR").show();    
            // //disable all vals


        } else {

            //disable all vals
            $(".UpdateName").hide();

            UpdateEmployer.EmployerDetailsUploadsFormHide();
            // $("#reqDocsToHide").hide();
            // $("#UpdateEmployerDetails_UploadBtn").hide();
            // $("#UpdateEmployerDetails_SaveBtn").hide();        
            // $("#UpdateEmployerDetails_RequiredDoc").hide();
            // $("#AllFilesInOne_TR").hide();    
            //disable all vals
        }

        //show person with the selected ID


        try {

            $("#UpdateEmployerDetails_FirstName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName);
            $("#UpdateEmployerDetails_SecondName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName);
            $("#UpdateEmployerDetails_ThirdName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName);
            $("#UpdateEmployerDetails_FourthName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName);
            //          $("#UpdateEmployerDetails_EntityBirthDate").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].BirthDate);

            SetInitialPhoneNoValue("UpdateEmployer_Mobile_CountryCodeDDL", "UpdateEmployerDetails_EntityMobile", UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile);
            SetInitialPhoneNoValue("UpdateEmployer_LandLine_CountryCodeDDL", "UpdateEmployerDetails_LandLine", UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline);

            //$("#UpdateEmployerDetails_EntityMobile").val(SetInitialPhoneValue(UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile));
            $("#UpdateEmployerDetails_EntityEmiratesID").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId);
            //$("#UpdateEmployerDetails_LandLine").val(SetInitialPhoneValue(UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline));
            $("#UpdateEmployerDetails_Email").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Email);
        } catch (e) {
            // alert("in catch"+e.toSource());
            // console.log("in catch"+e.toSource());
            $("#UpdateEmployerDetails_FirstName").val("");
            $("#UpdateEmployerDetails_SecondName").val("");
            $("#UpdateEmployerDetails_ThirdName").val("");
            $("#UpdateEmployerDetails_FourthName").val("");
            //        $("#UpdateEmployerDetails_EntityBirthDate").val("");
            $("#UpdateEmployer_Mobile_CountryCodeDDL").val("+971").trigger('change');
            $("#UpdateEmployerDetails_EntityMobile").val("");
            $("#UpdateEmployerDetails_EntityEmiratesID").val("");
            $("#UpdateEmployerDetails_LandLine").val("");
            $("#UpdateEmployer_LandLine_CountryCodeDDL").val("+971").trigger('change');
            $("#UpdateEmployerDetails_Email").val("");
        }




        //show person with the selected ID

    },
    HandleReasonsDropDownChanged: function () {

        var val = $("#UpdateEmployerDetails_ReasonsDropDown").val();
        var I_Type = "";
        var I_SubType = "";
        //reset vars n counts
        // UpdateEmployer.UploadedFilesCount = 0;
        // UpdateEmployer.uploadButtonPassed=false;
        // UpdateEmployer.CurrentFileCheckerIDs.length = 0;
        UpdateEmployer.resetCountsandVars();

        $('#AllFilesInOne').attr('checked', false);
        $(".UpdateEmployerContentClass").hide();
        $(".UpdateNameDropDown").hide();


        UpdateEmployer.SelectedVal = val;

        var val_int = parseInt(val);
        if (val_int > -1 && val_int < 6) {
            UpdateEmployer.EmployerDetailsUploadsFormShow();
        }

        //var URL = Globals.ServicesURI_Test + "view/requireddocuments/casetype/#Type#/caseSubType/#SubType#/language/#Lang#";
        // var URL = "http://mgovws.gpssa.ae:8080/SPREST.svc/view/requireddocuments/language/#Lang#";
        var URL = Globals.ServicesURI_Test+"view/requireddocuments/language/#Lang#";

        try {
            $("#UpdateEmployerDetails_FirstName").val("");
            $("#UpdateEmployerDetails_SecondName").val("");
            $("#UpdateEmployerDetails_ThirdName").val("");
            $("#UpdateEmployerDetails_FourthName").val("");
            $("#UpdateEmployerDetails_EntityMobile").val("");
            $("#UpdateEmployer_Mobile_CountryCodeDDL").val("+971").trigger('change');
            $("#UpdateEmployerDetails_LandLine").val("");
            $("#UpdateEmployer_LandLine_CountryCodeDDL").val("+971").trigger('change');
            $("#UpdateEmployerDetails_EntityEmiratesID").val("");
            $("#UpdateEmployerDetails_Email").val("");
            $("#UpdateEmployerDetails_EntityName").val("");
            $("#UpdateEmployerDetails_EntityNameAR").val("");
        } catch (e) {

        }

        if (val == -1) {
            CleanPage("UpdateEmployerDetails");
            return;
        } else if (val == "0") {

            $("#UpdateEmployerDetails_EntityName_TR").show();
            $("#UpdateEmployerDetails_EntityName_TR_Space").show();
            $("#UpdateEmployerDetails_EntityNameAR_TR").show();
            $("#UpdateEmployerDetails_EntityNameAR_TR_Space").show();
            try {
                $("#UpdateEmployerDetails_EntityName").val(UpdateEmployer.EmployerDetailsObj.EmployerName);
                $("#UpdateEmployerDetails_EntityNameAR").val(UpdateEmployer.EmployerDetailsObj.EmployerArabicName);

                $('#UpdateEmployerDetails_EntityNameAR').on('input', function (e) {

                    isArabic(UpdateEmployerDetails_EntityNameAR);
                });


            } catch (e) {
                $("#UpdateEmployerDetails_EntityName").val("");
                $("#UpdateEmployerDetails_EntityNameAR").val("");
            }

            I_Type = "Update Employer";
            I_SubType = "Change Entity Name";
            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Change Entity Name").replace("#Lang#", Language);
        } else if (val == "1") {

            $(".UpdateName").show();

            if (UpdateEmployer.EmployerDetailsObj.Contacts.length > 0) {
                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {

                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsOwner == true) {
                        try {
                            $("#UpdateEmployerDetails_FirstName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName);
                            $("#UpdateEmployerDetails_SecondName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName);
                            $("#UpdateEmployerDetails_ThirdName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName);
                            $("#UpdateEmployerDetails_FourthName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName);
                            //$("#UpdateEmployerDetails_EntityBirthDate").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].BirthDate);
                            //$("#UpdateEmployerDetails_EntityMobile").val(SetInitialPhoneValue(UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile));
                            $("#UpdateEmployerDetails_EntityEmiratesID").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId);
                            //$("#UpdateEmployerDetails_LandLine").val(SetInitialPhoneValue(UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline));
                            $("#UpdateEmployerDetails_Email").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Email);
                            SetInitialPhoneNoValue("UpdateEmployer_Mobile_CountryCodeDDL", "UpdateEmployerDetails_EntityMobile", UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile);
                            SetInitialPhoneNoValue("UpdateEmployer_LandLine_CountryCodeDDL", "UpdateEmployerDetails_LandLine", UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline);

                        } catch (e) {
                            $("#UpdateEmployerDetails_FirstName").val("");
                            $("#UpdateEmployerDetails_SecondName").val("");
                            $("#UpdateEmployerDetails_ThirdName").val("");
                            $("#UpdateEmployerDetails_FourthName").val("");
                            //$("#UpdateEmployerDetails_EntityBirthDate").val("");
                            $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                            $("#UpdateEmployerDetails_Email").val("");
                            $("#UpdateEmployerDetails_EntityMobile").val("");
                            $("#UpdateEmployer_Mobile_CountryCodeDDL").val("+971").trigger('change');
                            $("#UpdateEmployerDetails_LandLine").val("");
                            $("#UpdateEmployer_LandLine_CountryCodeDDL").val("+971").trigger('change');
                        }
                    }
                }
            } else {
                $("#UpdateEmployerDetails_FirstName").val("");
                $("#UpdateEmployerDetails_SecondName").val("");
                $("#UpdateEmployerDetails_ThirdName").val("");
                $("#UpdateEmployerDetails_FourthName").val("");
                //$("#UpdateEmployerDetails_EntityBirthDate").val("");
                $("#UpdateEmployerDetails_EntityMobile").val("");
                $("#UpdateEmployer_Mobile_CountryCodeDDL").val("+971").trigger('change');
                $("#UpdateEmployerDetails_LandLine").val("");
                $("#UpdateEmployer_LandLine_CountryCodeDDL").val("+971").trigger('change');
                $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                $("#UpdateEmployerDetails_Email").val("");
            }

            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Change Entity Owner").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Change Entity Owner";
        } else if (val == "2") {
            $(".UpdateName").show();
            if (UpdateEmployer.EmployerDetailsObj.Contacts.length > 0) {
                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsManager == true) {
                        try {
                            $("#UpdateEmployerDetails_FirstName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName);
                            $("#UpdateEmployerDetails_SecondName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName);
                            $("#UpdateEmployerDetails_ThirdName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName);
                            $("#UpdateEmployerDetails_FourthName").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName);
                            //   $("#UpdateEmployerDetails_EntityBirthDate").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].BirthDate);
                            $("#UpdateEmployerDetails_EntityEmiratesID").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId);
                            $("#UpdateEmployerDetails_Email").val(UpdateEmployer.EmployerDetailsObj.Contacts[i].Email);
                            SetInitialPhoneNoValue("UpdateEmployer_Mobile_CountryCodeDDL", "UpdateEmployerDetails_EntityMobile", UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile);
                            SetInitialPhoneNoValue("UpdateEmployer_LandLine_CountryCodeDDL", "UpdateEmployerDetails_LandLine", UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline);
                        } catch (e) {
                            $("#UpdateEmployerDetails_FirstName").val("");
                            $("#UpdateEmployerDetails_SecondName").val("");
                            $("#UpdateEmployerDetails_ThirdName").val("");
                            $("#UpdateEmployerDetails_FourthName").val("");
                            // $("#UpdateEmployerDetails_EntityBirthDate").val("");
                            $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                            $("#UpdateEmployerDetails_Email").val("");
                            $("#UpdateEmployerDetails_EntityMobile").val("");
                            $("#UpdateEmployer_Mobile_CountryCodeDDL").val("+971").trigger('change');
                            $("#UpdateEmployerDetails_LandLine").val("");
                            $("#UpdateEmployer_LandLine_CountryCodeDDL").val("+971").trigger('change');
                        }
                    }
                }
            } else {
                $("#UpdateEmployerDetails_FirstName").val("");
                $("#UpdateEmployerDetails_SecondName").val("");
                $("#UpdateEmployerDetails_ThirdName").val("");
                $("#UpdateEmployerDetails_FourthName").val("");
                //$("#UpdateEmployerDetails_EntityBirthDate").val("");
                $("#UpdateEmployerDetails_EntityEmiratesID").val("");
                $("#UpdateEmployerDetails_Email").val("");
                $("#UpdateEmployerDetails_EntityMobile").val("");
                $("#UpdateEmployer_Mobile_CountryCodeDDL").val("+971").trigger('change');
                $("#UpdateEmployerDetails_LandLine").val("");
                $("#UpdateEmployer_LandLine_CountryCodeDDL").val("+971").trigger('change');
            }

            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Admin Details").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Admin Details";
        } else if (val == "3") {
            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Entity Cancellation").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Entity Cancellation";
        } else if (val == "4") {
            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Admin Details").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Admin Details";
        } else if (val == "5") {

            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Admin Details").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Admin Details";

            $("#UpdateEmployerDetails_EntityAddress1_TR").show();
            $("#UpdateEmployerDetails_EntityAddress1_TR_Space").show();
            $("#UpdateEmployerDetails_EntityAddress2_TR").show();
            $("#UpdateEmployerDetails_EntityAddress2_TR_Space").show();
            $("#UpdateEmployerDetails_EntityDistrict_TR").show();
            $("#UpdateEmployerDetails_EntityDistrict_TR_Space").show();
            $("#UpdateEmployerDetails_EntityEmirate_TR").show();
            $("#UpdateEmployerDetails_EntityEmirate_TR_Space").show();
            $("#UpdateEmployerDetails_PhoneNumber_TR").show();
            $("#UpdateEmployerDetails_PhoneNumber_TR_Space").show();
            $("#UpdateEmployerDetails_FaxNumber_TR").show();
            $("#UpdateEmployerDetails_FaxNumber_TR_Space").show();
            $("#UpdateEmployerDetails_POBox_TR").show();
            $("#UpdateEmployerDetails_POBox_TR_Space").show();
            $("#UpdateEmployerDetails_EntityEmirate").html(EmiratesHTML);

            try {
                SetInitialPhoneNoValue("UpdateEmployer_Phone_CountryCodeDDL", "UpdateEmployerDetails_PhoneNumber", UpdateEmployer.EmployerDetailsObj.Phone);
                SetInitialPhoneNoValue("UpdateEmployer_Fax_CountryCodeDDL", "UpdateEmployerDetails_FaxNumber", UpdateEmployer.EmployerDetailsObj.Fax);

                $("#UpdateEmployerDetails_POBox").val(UpdateEmployer.EmployerDetailsObj.POBox);

                if (UpdateEmployer.EmployerDetailsObj.Address.length > 0) {
                    $("#UpdateEmployerDetails_EntityAddress1").val(UpdateEmployer.EmployerDetailsObj.Address[0].AddressLine1);
                    $("#UpdateEmployerDetails_EntityAddress2").val(UpdateEmployer.EmployerDetailsObj.Address[0].AddressLine2);
                    $("#UpdateEmployerDetails_EntityDistrict").val(UpdateEmployer.EmployerDetailsObj.Address[0].District);
                    $("#UpdateEmployerDetails_EntityEmirate").val(UpdateEmployer.EmployerDetailsObj.Address[0].Emirate).trigger('change');
                } else {
                    $("#UpdateEmployerDetails_EntityAddress1").val("");
                    $("#UpdateEmployerDetails_EntityAddress2").val("");
                    $("#UpdateEmployerDetails_EntityDistrict").val("");
                    $("#UpdateEmployerDetails_EntityEmirate").val("-1").trigger('change');
                }
            } catch (e) {
                $("#UpdateEmployerDetails_EntityAddress1").val("");
                $("#UpdateEmployerDetails_EntityAddress2").val("");
                $("#UpdateEmployerDetails_EntityDistrict").val("");
                $("#UpdateEmployerDetails_EntityEmirate").val("-1").trigger('change');
                $("#UpdateEmployerDetails_PhoneNumber").val("");
                $("#UpdateEmployer_Phone_CountryCodeDDL").val("+971").trigger('change');
                $("#UpdateEmployerDetails_FaxNumber").val("");
                $("#UpdateEmployer_Fax_CountryCodeDDL").val("+971").trigger('change');
            }
        } else if (val == "6") {
            $("#UpdateEmployerDetails_list_persons_DropDown").html("");


            var AuthorizedPersonsDefault = "اختر";

            if (Language == "en") {
                AuthorizedPersonsDefault = "choose";
            }

            $("#UpdateEmployerDetails_list_persons_DropDown").append('<option value="-1">' + AuthorizedPersonsDefault + ' </option>"');

            $(".UpdateNameDropDown").show();

            UpdateEmployer.EmployerDetailsUploadsFormHide();

            //disable all vals
            // $(".UpdateName").hide();
            // $("#reqDocsToHide").hide();
            // $("#UpdateEmployerDetails_UploadBtn").hide();
            // $("#UpdateEmployerDetails_SaveBtn").hide();        
            // $("#UpdateEmployerDetails_RequiredDoc").hide();
            // $("#AllFilesInOne_TR").hide();    
            //disable all vals


            if (UpdateEmployer.EmployerDetailsObj.Contacts.length > 0) {


                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {

                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsAuthorizedPerson == true) {
                        try {
                            //update dropdown list
                            $("#UpdateEmployerDetails_list_persons_DropDown").append('<option value=" ' + i + '">' + UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName + " " + UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName + "</option>");
                            //update dropdown list
                        } catch (e) {

                        }


                    }
                }
            }
            // else {
            //disable all vals


            // }

            //URL = URL.replace("#Type#", "Update Employer").replace("#SubType#", "Change in Authorized Persons").replace("#Lang#", Language);
            I_Type = "Update Employer";
            I_SubType = "Change in Authorized Persons";
        }
        Log(URL);
        var data = {
            Type: I_Type,
            SubType: I_SubType
        };

        var ReqObj = JSON.stringify(data);
        URL = URL.replace("#Lang#", Language);
        CallWebService(URL, "UpdateEmployer.HandleRequiredDocCallSuccess(response)", "", ReqObj, "POST", true, true);
    },

    HandleRequiredDocCallSuccess: function (response) {
        UpdateEmployer.currentFilsIDs = [];
        $("#UpdateEmployerDetails_RequiredDoc").html("");
        if (response != null) {
            var RespObj = response; //JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                var Temp = RespObj.RequiredDocuments.split(',');
                UpdateEmployer.RequiredDocLength = Temp.length;
                var AllHtml = "";
                for (var i = 0; i < Temp.length; i++) {
                    var HtmlTemp = UpdateEmployer.fileElement(Temp[i], i);
                    //var HtmlTemp = $('#UpdateEmployerDetails_File_Temp').html().replace('$RequireDoc$', Temp[i]);
                    var HTMLFooter = ' <tr class="PensionCalcResultTR3">' +
                    '<td colspan="3"></td>' +
                    '</tr>' +
                    '</table>';
                    if (i == (Temp.length - 1)) {
                        AllHtml += (HtmlTemp + '</table>');
                    } else {
                        AllHtml += (HtmlTemp + HTMLFooter);
                    }

                }
                $("#UpdateEmployerDetails_RequiredDoc").append(AllHtml);
            }
            try {
                for (var i = 0; i < UpdateEmployer.currentFilsIDs.length > 0; i++) {
                    UpdateEmployer.addFileEvents(UpdateEmployer.currentFilsIDs[i], i);
                }
            } catch (att) {
                alert(att);
            }
            UpdateEmployer.allDocsHTML = $("#UpdateEmployerDetails_RequiredDoc").html();
        }
    },

    HandleRequiredUploads: function () {

        var failed = false;
        if (UpdateEmployer.RequiredDocLength > 0) {

            if (!document.getElementById("AllFilesInOne").checked) {

                for (var i = 0; i < UpdateEmployer.currentFilsIDs.length; i++) {

                    var fileUpload = $("#" + UpdateEmployer.currentFilsIDs[i]).get(0);
                    var imagefile = fileUpload.files;
                    if (imagefile.length < 1) {
                        // AlertFunction("Please select Documents to upload", "من فضلك اختر الملفات المطلوبة لرفعها", "Error", "خطأ", "OK", "موافق");
                        failed = true;
                        // return;
                    }
                }
                UpdateEmployer.HandleUploadDocBtnClicked();

            } else {
                if ($("#allFilesInOne_upload").get(0).files.length < 1) {
                    AlertFunction("Please select Documents to upload", "من فضلك اختر الملفات المطلوبة لرفعها", "Error", "خطأ", "OK", "موافق");
                    failed = true;
                    // return;
                }
                UpdateEmployer.HandleUploadDocBtnClicked();
            }

            return failed;
        }


    },
    //Uploads Callbacks


    HandleUploadDocSuccess: function (name, fileID, r) {
        try {

            // console.log(r);
            // console.log(JSON.stringify(r));
            if (r.Message.Code == 0) {

                UpdateEmployer.UploadedFilesName[UpdateEmployer.UploadedFilesCount] = name;
                UpdateEmployer.EmployerDetailsObj.Header.AttachedFiles[UpdateEmployer.UploadedFilesCount] = name;
                UpdateEmployer.UploadedFilesCount += 1;
                //if all in one checked
                if (document.getElementById("AllFilesInOne").checked) {
                    $("#allFilesInOne_CheckMark").css("display", "block");
                } else {
                    $("#" + fileID + '_CheckMark').css("display", "block");
                }

            } else {
                //else if not uploaded success (ie code!=0)
                AlertFunction("Error Code : " + r.Message.Code + " Error Details : " + r.Message.Body, "كود الخطأ : " + r.Message.Code + " تفاصيل الخطأ : " + r.Message.Body, "Error", "خطأ", "OK", "موافق");
            }

        } catch (e) {}
    },

    HandleUploadDocSuccessForFileChooser: function (name, fileID, r) {
        try {

            var data = JSON.parse(r.response);

            if (data.Message.Code == 0) {

                //if status = true and upload success, else alert error            
                UpdateEmployer.UploadedFilesName[UpdateEmployer.UploadedFilesCount] = name;
                UpdateEmployer.EmployerDetailsObj.Header.AttachedFiles[UpdateEmployer.UploadedFilesCount] = name;
                UpdateEmployer.UploadedFilesCount += 1;
                //if all in one checked
                if (document.getElementById("AllFilesInOne").checked) {
                    $("#allFilesInOne_CheckMark").css("display", "block");
                } else {
                    $("#" + fileID + '_CheckMark').css("display", "block");
                }

            } else {
                //else if not uploaded success (ie code!=0)
                AlertFunction("Error Code : " + data.Message.Code + " Error Details : " + data.Message.Body, "كود الخطأ : " + data.Message.Code + " تفاصيل الخطأ : " + data.Message.Body, "Error", "خطأ", "OK", "موافق");
            }

        } catch (e) {}
    },
    HandleUploadDocFail: function (name) {
        // alert("name is "+ name)
        try {
            UpdateEmployer.UploadDocFailed = true;

        } catch (e) {}
    },

    HandleCameraUploadSuccess: function (field_id, name, r) {

        var data = JSON.parse(r.response);
        if (data.Message.Code == 0) {

            if ($(field_id).css("display") == "block") {
                AlertFunction("File replaced", "لقد تم استبدال الملف", "warning", "تحذير", "OK", "موافق");
            }

            UpdateEmployer.UploadedFilesName[UpdateEmployer.UploadedFilesCount] = name;
            UpdateEmployer.EmployerDetailsObj.Header.AttachedFiles[UpdateEmployer.UploadedFilesCount] = name;
            UpdateEmployer.UploadedFilesCount += 1;


            $(field_id).css("display", "block");
            UpdateEmployer.didCamUpload = true;
        } else {
            //else if not uploaded success (ie code!=0)
            AlertFunction("Error Code : " + data.Message.Code + " Error Details : " + data.Message.Body, "كود الخطأ : " + data.Message.Code + " تفاصيل الخطأ : " + data.Message.Body, "Error", "خطأ", "OK", "موافق");
        }

    },
    HandleCameraUploadFail: function (field_id) {
        UpdateEmployer.UploadCamFailed = true;
    },


    //Uploads Callbacks

    HandleSaveBtnClicked: function () {
        try {

            ///file uploads validation
            if (!UpdateEmployer.smartUploadsValidation()) {
                UpdateEmployer.uploadButtonPassed = false;
                AlertFunction("Please Upload all required documents", "من فضلك ارفع جميع الملفات المطلوبة", "Error", "خطأ", "OK", "موافق");
                return;
            }

            ///file uploads validation


            var CleanUpdateEmployerDetails_EntityMobile = GetFormatedPhoneNoValue("UpdateEmployer_Mobile_CountryCodeDDL", "UpdateEmployerDetails_EntityMobile");
            var CleanWithPlusEntityMobile = CleanUpdateEmployerDetails_EntityMobile;

            var CleanUpdateEmployerDetails_LandLine = GetFormatedPhoneNoValue("UpdateEmployer_LandLine_CountryCodeDDL", "UpdateEmployerDetails_LandLine");
            var CleanWithPlusLandLine = CleanUpdateEmployerDetails_LandLine;

            var FaxNO = GetFormatedPhoneNoValue("UpdateEmployer_Fax_CountryCodeDDL", "UpdateEmployerDetails_FaxNumber");
            var PhoneNO = GetFormatedPhoneNoValue("UpdateEmployer_Phone_CountryCodeDDL", "UpdateEmployerDetails_PhoneNumber");
            //alert("CleanWithPlusEntityMobile  " + CleanWithPlusEntityMobile);
            //alert("CleanWithPlusLandLine  " + CleanWithPlusLandLine);
            if (UpdateEmployer.SelectedVal == -1) {
                AlertFunction("Please select reason for update", "من فضلك اختر سبب التحديث", "Error", "خطأ", "OK", "موافق");
                return;
            } else if (UpdateEmployer.SelectedVal == 0) {

                //validations
                if ($.trim($("#UpdateEmployerDetails_EntityName").val()) == "") {
                    AlertFunction("Please enter Entity Name", "من فضلك ادخل اسم المنشأة", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_EntityNameAR").val()) == "") {
                    AlertFunction("Please enter Entity Name AR", "من فضلك ادخل اسم المنشأة ", "Error", "خطأ", "OK", "موافق");
                    return;
                }
                //validations

                UpdateEmployer.EmployerDetailsObj.EmployerName = $("#UpdateEmployerDetails_EntityName").val();
                UpdateEmployer.EmployerDetailsObj.EmployerArabicName = $("#UpdateEmployerDetails_EntityNameAR").val();
            } else if (UpdateEmployer.SelectedVal == 1) {

                //validations
                if ($.trim($("#UpdateEmployerDetails_FirstName").val()) == "") {
                    AlertFunction("Please enter First Name", "من فضلك ادخل الأسم الأول", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_SecondName").val()) == "") {
                    AlertFunction("Please enter Second Name", "من فضلك ادخل الأسم الثانى", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_ThirdName").val()) == "") {
                    AlertFunction("Please enter Second Name", "من فضلك ادخل الأسم الثانى", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_FourthName").val()) == "") {
                    AlertFunction("Please enter Fourth Name", "من فضلك ادخل الأسم الرابع", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if (CleanWithPlusEntityMobile == "") {
                    AlertFunction("Please enter valid Mobile No.", "من فضلك ادخل رقم هاتف محمول صحيح", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if (CleanWithPlusLandLine == "") {
                    AlertFunction("Please enter valid Land Line No.", "من فضلك ادخل رقم هاتف ارضى صحيح", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_EntityEmiratesID").val()) == "") {
                    AlertFunction("Please enter Emirates ID", "من فضلك ادخل رقم الهوية", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_Email").val()) == "") {
                    AlertFunction("Please enter E-mail", "من فضلك ادخل  البريد الالكتروني", "Error", "خطأ", "OK", "موافق");
                    return;
                }

                //validations


                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsOwner == true) {
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName = $("#UpdateEmployerDetails_FirstName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName = $("#UpdateEmployerDetails_SecondName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName = $("#UpdateEmployerDetails_ThirdName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName = $("#UpdateEmployerDetails_FourthName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile = CleanWithPlusEntityMobile;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId = FormatChar($("#UpdateEmployerDetails_EntityEmiratesID").val());
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline = CleanWithPlusLandLine;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Email = $("#UpdateEmployerDetails_Email").val();
                    }
                }
            } else if (UpdateEmployer.SelectedVal == 2) {

                //validations
                if ($.trim($("#UpdateEmployerDetails_FirstName").val()) == "") {
                    AlertFunction("Please enter First Name", "من فضلك ادخل الأسم الأول", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_SecondName").val()) == "") {
                    AlertFunction("Please enter Second Name", "من فضلك ادخل الأسم الثانى", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_ThirdName").val()) == "") {
                    AlertFunction("Please enter Second Name", "من فضلك ادخل الأسم الثانى", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_FourthName").val()) == "") {
                    AlertFunction("Please enter Fourth Name", "من فضلك ادخل الأسم الرابع", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if (CleanWithPlusEntityMobile == "") {

                    AlertFunction("Please enter valid Mobile No.", "من فضلك ادخل رقم هاتف محمول صحيح", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_EntityEmiratesID").val()) == "") {
                    AlertFunction("Please enter Emirates ID", "من فضلك ادخل رقم الهوية", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if (CleanWithPlusLandLine == "") {
                    AlertFunction("Please enter valid Land Line No.", "من فضلك ادخل رقم هاتف ارضى صحيح", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_Email").val()) == "") {
                    AlertFunction("Please enter E-mail", "من فضلك ادخل  البريد الالكتروني", "Error", "خطأ", "OK", "موافق");
                    return;
                }

                //validations


                for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsManager == true) {
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName = $("#UpdateEmployerDetails_FirstName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName = $("#UpdateEmployerDetails_SecondName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName = $("#UpdateEmployerDetails_ThirdName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName = $("#UpdateEmployerDetails_FourthName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile = CleanWithPlusEntityMobile;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId = FormatChar($("#UpdateEmployerDetails_EntityEmiratesID").val());
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline = CleanWithPlusLandLine;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Email = $("#UpdateEmployerDetails_Email").val();
                    }
                }
            } else if (UpdateEmployer.SelectedVal == 3) {} else if (UpdateEmployer.SelectedVal == 4) {} else if (UpdateEmployer.SelectedVal == 5) {

                //validations
                if ($.trim($("#UpdateEmployerDetails_EntityAddress1").val()) == "") {
                    AlertFunction("Please enter Entity Address Line 1", "من فضلك ادخل عنوان المنشأة 1", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_EntityAddress2").val()) == "") {
                    AlertFunction("Please enter Entity Address Line 2", "من فضلك ادخل عنوان المنشأة 2", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_EntityDistrict").val()) == "") {
                    AlertFunction("Please enter Entity District", "من فضلك ادخل المنطقة", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_EntityEmirate").val()) == "") {
                    AlertFunction("Please enter Entity Emirate", "من فضلك ادخل الامارة", "Error", "خطأ", "OK", "موافق");
                    return;
                }

                /*else if ($.trim($("#UpdateEmployerDetails_PhoneNumber").val()) == "") {
    AlertFunction("Please enter Phone Number", "من فضلك ادخل رقم هاتف", "Error", "خطأ", "OK", "موافق");
    return;
}*/

else if (FaxNO == "") {
    AlertFunction("Please enter Valid Fax No.", "من فضلك ادخل رقم فاكس صحيح", "Error", "خطأ", "OK", "موافق");
    return;
} else if ($.trim($("#UpdateEmployerDetails_POBox").val()) == "") {
    AlertFunction("Please enter P.O. Box", "من فضلك ادخل صندوق بريد", "Error", "خطأ", "OK", "موافق");
    return;
}

                //validations

                UpdateEmployer.EmployerDetailsObj.Address[0].AddressLine1 = $("#UpdateEmployerDetails_EntityAddress1").val();
                UpdateEmployer.EmployerDetailsObj.Address[0].AddressLine2 = $("#UpdateEmployerDetails_EntityAddress2").val();
                UpdateEmployer.EmployerDetailsObj.Address[0].District = $("#UpdateEmployerDetails_EntityDistrict").val();
                UpdateEmployer.EmployerDetailsObj.Address[0].Emirate = $("#UpdateEmployerDetails_EntityEmirate").val();
                UpdateEmployer.EmployerDetailsObj.Phone = PhoneNO;
                UpdateEmployer.EmployerDetailsObj.Fax = FaxNO;
                UpdateEmployer.EmployerDetailsObj.POBox = $("#UpdateEmployerDetails_POBox").val();

            } else if (UpdateEmployer.SelectedVal == 6) {


                //validations
                if ($.trim($("#UpdateEmployerDetails_FirstName").val()) == "") {
                    AlertFunction("Please enter First Name", "من فضلك ادخل الأسم الأول", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_SecondName").val()) == "") {
                    AlertFunction("Please enter Second Name", "من فضلك ادخل الأسم الثانى", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_ThirdName").val()) == "") {
                    AlertFunction("Please enter Second Name", "من فضلك ادخل الأسم الثانى", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_FourthName").val()) == "") {
                    AlertFunction("Please enter Fourth Name", "من فضلك ادخل الأسم الرابع", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if (CleanWithPlusEntityMobile == "") {
                    AlertFunction("Please enter valid Mobile No.", "من فضلك ادخل رقم هاتف محمول صحيح", "Error", "خطأ", "OK", "موافق");
                    return;
                } else if ($.trim($("#UpdateEmployerDetails_EntityEmiratesID").val()) == "") {
                    AlertFunction("Please enter Emirates ID", "من فضلك ادخل رقم الهوية", "Error", "خطأ", "OK", "موافق");
                    return;
                }
                /*else if (CleanWithPlusLandLine== "") {
    AlertFunction("Please enter Land Line", "من فضلك ادخل الهاتف الارضى", "Error", "خطأ", "OK", "موافق");
    return;
}*/
else if ($.trim($("#UpdateEmployerDetails_Email").val()) == "") {
    AlertFunction("Please enter E-mail", "من فضلك ادخل  البريد الالكتروني", "Error", "خطأ", "OK", "موافق");
    return;
}

                //validations




                // for (var i = 0; i < UpdateEmployer.EmployerDetailsObj.Contacts.length; i++) {
                    var i = parseInt($("#UpdateEmployerDetails_list_persons_DropDown").val());

                    if (UpdateEmployer.EmployerDetailsObj.Contacts[i].IsAuthorizedPerson == true) {
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FirstName = $("#UpdateEmployerDetails_FirstName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].SecondName = $("#UpdateEmployerDetails_SecondName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].ThirdName = $("#UpdateEmployerDetails_ThirdName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].FourthName = $("#UpdateEmployerDetails_FourthName").val();
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Mobile = CleanWithPlusEntityMobile;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].EmiratesId = FormatChar($("#UpdateEmployerDetails_EntityEmiratesID").val());
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Landline = CleanWithPlusLandLine;
                        UpdateEmployer.EmployerDetailsObj.Contacts[i].Email = $("#UpdateEmployerDetails_Email").val();
                    }
                // }
            }


            if ($.trim($("#UpdateEmployerDetails_Email").val()) != "") {
                if (!ValidateEmail($("#UpdateEmployerDetails_Email").val())) {
                    AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صحيح", "Error", "خطأ", "OK", "موافق");
                    return;
                }
            }

            var URL = Globals.ServicesURI_Test + "update/employer/language/#Lang#";
            URL = URL.replace("#Lang#", Language);

            if (isUndefinedOrNullOrBlank(UpdateEmployer.EmployerDetailsObj.ExpirationDate) || UpdateEmployer.EmployerDetailsObj.ExpirationDate == "01/01/2020") {
                UpdateEmployer.EmployerDetailsObj.ExpirationDate = "1577829600"; //1-1-2020
            }

            UpdateEmployer.EmployerDetailsObj.ExpirationDate = ConvertDateToUNIX(UpdateEmployer.EmployerDetailsObj.ExpirationDate);

            var data = JSON.stringify(UpdateEmployer.EmployerDetailsObj);

            CallWebService(URL, 'UpdateEmployer.HandleUpdateCallSuccess(response)', "", data, "POST", true, true, true, 120000);

        } catch (e) {}
    },

    HandleUpdateCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AlertFunction(UpdateEmployer.SuccessMsg + response.CaseNumber, UpdateEmployer.SuccessMsg_AR + response.CaseNumber, "Success", "نجاح", "OK", "موافق");
                UpdateEmployer.SelectedVal = 0;
                UpdateEmployer.RequiredDocLength = 0;
                UpdateEmployer.UploadedFilesName = new Array();
                //reset vars n counts
                // UpdateEmployer.UploadedFilesCount = 0;
                // UpdateEmployer.uploadButtonPassed=false;
                // UpdateEmployer.CurrentFileCheckerIDs.length = 0;
                UpdateEmployer.resetCountsandVars();

                $("#UpdateEmployerDetails_RequiredDoc").html("");
                CleanPage("UpdateEmployerDetails");
            } else {
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    OpenUploadFile: function () {
        $("#UpdateEmployerDetails_File").css("display", "block");
        $("#UpdateEmployerDetails_File").css("visibility", "visible");
        setTimeout(function () {
            $("#UpdateEmployerDetails_File").trigger("click");
        }, 300);
        $("#UpdateEmployerDetails_File").css("display", "none");
        $("#UpdateEmployerDetails_File").css("visibility", "hidden");
    },
    fileElement: function (title, id) {
        var currnetID = "UpdateEmployerDetails" + id;
        UpdateEmployer.currentFilsIDs.push(currnetID);

        var onClickHTML = 'onclick="UpdateEmployer.addClickEventForFileChooser(';
            onClickHTML += "'" + currnetID + "'";
            onClickHTML += ')"';
var html = ' <table style="width:100%" cellspacing="0" cellpadding="0">' +
'<tr  onclick = "" >' +
' <td  style="color: #862826; height: 40px; ">' + title +
'</td>' +

' <td style="width:50%">' +
' <table>' +
' <tr>' +
' <td>' +
'  <div class="browse-wrap">' +
'  <div>' +
'   <img src="Images/upload.png" style="height:35px; width:35px;" onclick="">' +
'  </div>' +
' <input type="file" ' + onClickHTML + '   id="' + currnetID + '" name="' + currnetID + '" style="height: 40px; width: 40px;" class="upload" title="">' +
'  </div>' +
' </td>' +
' <td></td>' +
' <td>' +
'  <div >' +
' <img src="Images/cam.png" id="' + 'CamUploadUpdateEmployerDetails' + id + '" style="height:35px; width:35px; z-index:2000000" onclick="" />' +
' </div>' +
' </td>' +
' <td class="requiredTick" id="UpdateEmployerDetails' + id + "_CheckMark" + '" style="display:none" >' +
' <img src="Images/CheckMark.png" style="height:35px; width:35px; z-index:2000000">' +
' </td>' +
' </tr>' +
'</table>' +
'</td>' +
'</tr>';

UpdateEmployer.CurrentFileCheckerIDs.push('#UpdateEmployerDetails' + id + "_CheckMark");
return html;
},
addFileEvents: function (currnetID, id) {
        // Detect changes
        var uploader = document.getElementsByName(currnetID);
        for (item in uploader) {
            // Detect changes



            // }else{
            //regular fileINput 
            uploader[item].onchange = function () {

                if ($("#" + "UpdateEmployerDetails" + id + "_CheckMark").css("display") == "block") {
                    AlertFunction("File replaced", "لقد تم استبدال الملف", "warning", "تحذير", "OK", "موافق");
                }
                //upload on selection
                // UpdateEmployer.uploadDoc(currnetID);
                uploadDoc(currnetID,"UpdateEmployer.HandleUploadDocSuccess(name,InputID,r)", "UpdateEmployer.HandleUploadDocFail(name,InputID)");

            }
            //regular fileINput 
            // }

        }
        $('#' + 'CamUploadUpdateEmployerDetails' + id).click(function () {
            var uuid = guid();
            //set cam upload n fail funs
            var field_id = '"#' + 'UpdateEmployerDetails' + id + "_CheckMark" + '"';
            camera_success_callback = "UpdateEmployer.HandleCameraUploadSuccess(" + field_id + ",name,r)";
            camera_fail_callback = "UpdateEmployer.HandleCameraUploadFail(" + field_id + ")";
            getPhoto(uuid);
            return false;
        });
    },


    addClickEventForFileChooser: function (field_id) {
        //kitkat case
        if (device.platform.toLowerCase() === 'android' && (device.version.indexOf('4.4.2') === 0 || device.version.indexOf('4.4.3') === 0)) {
            fileChooser.pick("*", function (uri) {
                theJSON = uri;

                if (!validateExtension(uri.filename)) {
                    AlertFunction("Please upload following files (pdf,doc,docx,jpg,jpeg,png) only", "يجب تحميل الملفات التاليه (pdf,doc,docx,jpg,jpeg,png) فقط", "Error", "خطأ", "OK", "موافق");
                    return;
                }
                //validate sizze
                if (uri.size / 1024 / 1024 > 7) {
                    AlertFunction("Max Size for documents is 7 MB", "الحد الاقصى للملفات 7 م ب", "Error", "خطأ", "OK", "موافق");
                    return;
                }

                  //if all in one checked
                  if (document.getElementById("AllFilesInOne").checked) {
                    if ($("#allFilesInOne_CheckMark").css("display") == "block") {
                        AlertFunction("File replaced", "لقد تم استبدال الملف", "warning", "تحذير", "OK", "موافق");
                    }
                } else {
                  if ($("#" + field_id + '_CheckMark').css("display") == "block") {
                    AlertFunction("File replaced", "لقد تم استبدال الملف", "warning", "تحذير", "OK", "موافق");
                }

            }

            



                fileChooser_success_callback = "UpdateEmployer.HandleUploadDocSuccessForFileChooser(name,InputID,r)";
                fileChooser_fail_callback = "UpdateEmployer.HandleUploadDocFail(name,InputID)";
                onUploadFromFileChooser(uri, field_id);

            });
        } else {
            console.log('I am ok m8 not a kitkat');
        }

    },

    

    //smart validation
    allFilesCheckOnChange: function () {

        //on check change, rest all including array of uploadedfiles

        //reset vars n counts
        // UpdateEmployer.resetCountsandVars();

        //fix hamed case

            try {

        // $(".requiredTick").css("visibility", "hidden");
        $(".requiredTick").css("display", "none");
        UpdateEmployer.UploadedFilesCount = 0;
        UpdateEmployer.uploadButtonPassed = false;
        UpdateEmployer.UploadedFilesName.length = 0;
        UpdateEmployer.EmployerDetailsObj.Header.AttachedFiles.length = 0;
        UpdateEmployer.UploadDocFailed = false;
        UpdateEmployer.UploadCamFailed = false;

        }catch (e) {

        }

        //fix hamed case

        if (document.getElementById("AllFilesInOne").checked) {

            //ALL IN ONE CASE
            var hrmlTemp = $("#UpdateEmployerDetails_File_Temp").html();
            $("#UpdateEmployerDetails_RequiredDoc").html(hrmlTemp);
            // $("#allFilesInOne_CheckMark").hide();
            var uploader = document.getElementsByName('allFilesInOne_upload');
            for (item in uploader) {
                // Detect changes
                uploader[item].onchange = function () {

                    if ($("#allFilesInOne_CheckMark").css("display") == "block") {
                        AlertFunction("File replaced", "لقد تم استبدال الملف", "warning", "تحذير", "OK", "موافق");
                    }
                    //upload on selection
                    uploadDoc("allFilesInOne_upload","UpdateEmployer.HandleUploadDocSuccess(name,InputID,r)", "UpdateEmployer.HandleUploadDocFail(name,InputID)");
                }
            }
            $('#allFilesInOne_CamUpload').click(function () {
                var uuid = guid();
                ComplaintsModule.CurrentUUID = uuid;
                camera_success_callback = "UpdateEmployer.HandleCameraUploadSuccess('#allFilesInOne_CheckMark',name,r)";
                camera_fail_callback = "UpdateEmployer.HandleCameraUploadFail('#allFilesInOne_CheckMark')";
                getPhoto(uuid);
                return false;
            });
        } else {

            //     //LIST OF UPLOADS CASE
            $("#UpdateEmployerDetails_RequiredDoc").html(UpdateEmployer.allDocsHTML);
            try {
                for (var i = 0; i < UpdateEmployer.currentFilsIDs.length > 0; i++) {
                    UpdateEmployer.addFileEvents(UpdateEmployer.currentFilsIDs[i], i);
                }
            } catch (att) {
                // alert(att);
            }
        }
    }
};

function clearCache() {
    navigator.camera.cleanup();
}