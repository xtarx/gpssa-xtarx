﻿//img caching variables
var DATADIR;
var entry;
var fullpath;
var knownfiles = [];
// var package_name="com.yourname.GPSSA_XtarX/files";
var package_name = "com.gpspspa.imgs/";

//img caching variables

var News = {

    NewsRes: null,
    NewsDetailsResp: null,
    CurrentNewsDetailsID: 0,
    MostRecentNews: 0,
    ConnectionLost: false,

    LoadNewsPage: function () {

        try {

            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, onRequestFileSystemSuccess, null);


        } catch (e) {

        }
    },
    LoadNewsPageAfterReadCacheDir: function () {

        try {
            HideLoadingSpinner();



            if (localStorage.getItem("News" + Language) == null || localStorage.getItem("News" + Language) == undefined) {
                var URL = Globals.ServicesURI_Test + "get/news/newsCount/10/language/#Lang#";
                URL = URL.replace("#Lang#", Language);
                CallWebService(URL, 'News.HandleNewsCallSuccess(response)', "", "", "", true, false);
            } else {

                var response = localStorage.getItem("News" + Language);
                News.NewsRes = JSON.parse(response);

                News.MostRecentNews = Number(News.NewsRes.News[0].Modified);

                for (var i = 0; i < News.NewsRes.News.length; i++) {
                    var Temp = Number(News.NewsRes.News[i].Modified);

                    if (News.MostRecentNews > Temp) {
                        News.MostRecentNews = Temp;
                    }
                }
                // alert("loading img in loadNewspage")


                var URL = Globals.ServicesURI_Test + "validate/list/module/news/lastupdate/#LastUpdate#/language/#Lang#";
                URL = URL.replace("#LastUpdate#", News.MostRecentNews).replace("#Lang#", Language);
                CallWebService(URL, "News.HandleValidateNewsCallSuccess(response)", "News.HandleValidateNewsCallFailure()", "", "", true, false, false);
            }

        } catch (e) {

        }
    },

    HandleValidateNewsCallSuccess: function (response) {
        if (response != null) {
            HideLoadingSpinner();
            var RespObj = response; //JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("News", 'News.HandleNewsDocReady()', true, true);
                } else {
                    News.NewsRes = null;
                    localStorage.removeItem("News" + Language);
                    var URL = Globals.ServicesURI_Test + "get/news/newsCount/10/language/#Lang#";
                    URL = URL.replace("#Lang#", Language);
                    CallWebService(URL, 'News.HandleNewsCallSuccess(response)', "", "", "", true, false);
                }
            }
        }
    },

    HandleValidateNewsCallFailure: function () {
        var Resp = localStorage.getItem("News" + Language);
        News.NewsRes = JSON.parse(Resp);
        LoadPageContent("News", 'News.HandleNewsDocReady()', false, true);
        News.ConnectionLost = true;
    },

    HandleNewsCallSuccess: function (response) {
        if (response != null) {
            HideLoadingSpinner();

            localStorage.setItem("News" + Language, JSON.stringify(response));
            News.NewsRes = response; //JSON.parse(response);
        }
        LoadPageContent("News", 'News.HandleNewsDocReady()', true, true);
    },

    HandleNewsDocReady: function () {

        SetHeaderTitle("News", "الأخبار");

        $("#NewsListContent").html("");
        $("#NewsListContent").scrollTop(0);
        if (News.ConnectionLost == true) {

            //if conneciton lost 
            //look for image in cache, if not found  place the default logo


            console.log(" connection is lost ");


            for (var i = 0; i < News.NewsRes.News.length; i++) {
                News.NewsRes.News[i].src = "Images/logo.png";

                for (var j = 0; j < knownfiles.length; j++) {

                    var imgNameArray = knownfiles[j].name.split(".");
                    var imgName = imgNameArray[0];

                    console.log(" connection lost comparing " + imgName + "  & "+News.NewsRes.News[i].Id);

                    if (imgName == News.NewsRes.News[i].Id) {
                        console.log(" img in cache  ++ " + imgName);
                        News.NewsRes.News[i].src = fullpath + knownfiles[j].fullPath;
                    }

                }

            }


        } else {
            for (var i = 0; i < News.NewsRes.News.length; i++) {
                News.NewsRes.News[i].src = ReturnImageSrc(News.NewsRes.News[i].NewsImage);
                //News.NewsRes.News[i].DownloadedSrc = downloadFiles(ReturnImageSrc(News.NewsRes.News[i].NewsImage));


                //if image already in cache do nothing, else download


                var found = false;
                for (var j = 0; j < knownfiles.length; j++) {

                    var imgNameArray = knownfiles[j].name.split(".");
                    var imgName = imgNameArray[0];


                    if (imgName == News.NewsRes.News[i].Id) {
                        console.log(" img in cache not downloading  ++ " + imgName);
                        found = true;
                        break;
                    }


                }

                if (!found) {
                    //if not in cache already download
                    var newsObject = News.NewsRes.News[i];
                    var imgSrcAsString = newsObject.src;
                    imgSrcAsString = imgSrcAsString[0];


                    var extArray = imgSrcAsString.split(".");
                    var ext = extArray[extArray.length - 1];

                    var dlPath = fullpath + package_name + newsObject.Id + '.' + ext; //replace with NEWSID
                    console.log("downloading crap to " + dlPath);
                    var ft = new FileTransfer();

                    ft.download(imgSrcAsString, dlPath, function (e) {
                        console.log("Successful download of " + e.fullPath);
                        knownfiles.push(e);

                    }, onError);


                }

            }

        }
        $("#NewsTemplate").tmpl(News.NewsRes.News).appendTo("#NewsListContent");

    },
    OpenNewsDetails: function (id) {
        $("#NewsDetails_Content").scrollTop(0);
        $("#NewsDetails").scrollTop(0);
        $("#News").scrollTop(0);
        $("#ALLPageContent").scrollTop(0);
        News.CurrentNewsDetailsID = id;
        // check news at cache.
        if (localStorage.getItem("NewsDetails" + Language + id) == null || localStorage.getItem("NewsDetails" + Language + id) == undefined) {
            var URL = Globals.ServicesURI_Test + "get/news/newsid/#id#/language/#Lang#";
            URL = URL.replace("#id#", id).replace("#Lang#", Language);
            CallWebService(URL, "News.HandleNewsDetailsSuccess(response)", "", "", "", true, true);
        } else {
            var response = localStorage.getItem("NewsDetails" + Language + News.CurrentNewsDetailsID);
            News.NewsDetailsResp = JSON.parse(response);

            var URL = Globals.ServicesURI_Test + "validate/item/module/news/itemid/#ID#/lastupdate/#LastUpdate#/language/#Lang#";
            URL = URL.replace("#LastUpdate#", News.NewsDetailsResp.News.Modified).replace("#ID#", News.CurrentNewsDetailsID).replace("#Lang#", Language);
            CallWebService(URL, "News.HandleValidateNewsItemCallSuccess(response)", "News.HandleValidateNewsItemCallFailure()", "", "", true, false, false);
        }
    },

    HandleValidateNewsItemCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response; //JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("NewsDetails", 'News.HandleNewsDetailsDocReady()', true, true, true, false, "LoadPageContent('News', 'News.HandleNewsDocReady()', true, true);");
                } else {
                    News.NewsDetailsResp = null;
                    localStorage.removeItem("NewsDetails" + Language + News.CurrentNewsDetailsID);
                    var URL = Globals.ServicesURI_Test + "get/news/newsid/#id#/language/#Lang#";
                    URL = URL.replace("#id#", id).replace("#Lang#", Language);
                    CallWebService(URL, "News.HandleNewsDetailsSuccess(response)", "", "", "", true, true);
                }
            }
        }
    },

    HandleValidateNewsItemCallFailure: function () {
        var Resp = localStorage.getItem("NewsDetails" + Language + News.CurrentNewsDetailsID);
        News.NewsDetailsResp = JSON.parse(Resp);
        LoadPageContent("NewsDetails", 'News.HandleNewsDetailsDocReady()', false, true, true, false, "LoadPageContent('News', 'News.HandleNewsDocReady()', true, true);");
    },

    HandleNewsDetailsSuccess: function (response) {
        if (response != null) {
            HideLoadingSpinner();

            News.NewsDetailsResp = response; //JSON.parse(response);
            localStorage.setItem("NewsDetails" + Language + News.CurrentNewsDetailsID, JSON.stringify(response));
        }
        LoadPageContent("NewsDetails", 'News.HandleNewsDetailsDocReady()', true, true, true, false, "LoadPageContent('News', 'News.HandleNewsDocReady()', true, true);");
    },

    HandleNewsDetailsDocReady: function () {
        // $("#NewsDetails_Content").html("");
        SetHeaderTitle("News Details", "تفاصيل الأخبار");
        $("#NewsDetailsImg").attr("src", "");
        $("#NewsDetailsImg").width(DeviceWidth - 50);
        $("#NewsDetailsImg").height(DeviceHeight * .3);
        console.log(News.NewsDetailsResp.News.Details);
        $("#NewsTitle").html(News.NewsDetailsResp.News.Title);
        $("#NewsDate").html(ConvertUNIXToDate(News.NewsDetailsResp.News.NewsDate));

        BindEvents('#NewsDetails_BackBtn', 'click', 'News.HandleNewsDetailsBackBtn()');
        $("#NewsListContent").scrollTop(0);
        $("#NewsDetails_Content").scrollTop(0);
        $("#NewsDetails_Content").html(News.NewsDetailsResp.News.Details);

        var src = "";

        if (this.ConnectionLost == true) {
            //

            // var reader2 = DATADIR.createReader();
            // reader2.readEntries(function(d){
            //     findSingleImageinCache(d,News.NewsDetailsResp.News.Id);
            // },onReadDirectoryErrorSingleImageCase);
src = "Images/logo.png";

for (var j = 0; j < knownfiles.length; j++) {

    var imgNameArray = knownfiles[j].name.split(".");
    var imgName = imgNameArray[0];


    if (imgName == News.NewsDetailsResp.News.Id) {
        console.log("single img img in cache  ++ " + imgName);
        src = fullpath + knownfiles[j].fullPath;
        break;
    }


}




            // src = "Images/logo.png";
        } else {
            src = ReturnImageSrc(News.NewsDetailsResp.News.NewsImage);
        }

        $("#NewsDetailsImg").attr("src", src)
    },

    HandleNewsDetailsBackBtn: function () {
        LoadPageContent("News", 'News.HandleNewsDocReady()', true, true);
    }
};



//start of Image caching

function onError(e) {
    console.log("ERROR in DOWNLODS" + e.code);

    console.log("download error source " + e.source);
    console.log("download e target " + e.target);
    // alert("ERROR in DOWNLODS"+e.source);
    // alert("ERROR in DOWNLODS"+e.code);

}

//in case readdir fails 
//load default lgoo img
function onReadDirectoryError(e) {

    for (var i = 0; i < News.NewsRes.News.length; i++) {
        News.NewsRes.News[i].src = "Images/logo.png";
    }

    $("#NewsTemplate").tmpl(News.NewsRes.News).appendTo("#NewsListContent");

}

function onReadDirectoryErrorSingleImageCase(e) {
    console.log("is ERORORO");

    $("#NewsDetailsImg").attr("src", "Images/logo.png")

}




function onRequestFileSystemSuccess(fileSystem) {
    fullpath = fileSystem.root.toURL();
    entry = fileSystem.root;
    // entry.getDirectory("Android/data/"+package_name, {create: true, exclusive: false}, onGetDirectorySuccess, onGetDirectoryFail);
    entry.getDirectory(package_name, {
        create: true,
        exclusive: false
    }, onGetDirectorySuccess, onGetDirectoryFail);

}


function onGetDirectorySuccess(dir) {
    DATADIR = dir;
    //read dir and add known files
    var reader = DATADIR.createReader();
    reader.readEntries(function (d) {
        gotFiles(d);
        News.LoadNewsPageAfterReadCacheDir();
        // appReady();
    }, onError);



}

//Result of reading my directory
function gotFiles(entries) {
    console.log("The dir has " + entries.length + " entries.");
    for (var i = 0; i < entries.length; i++) {
        console.log(entries[i].name + ' dir? ' + entries[i].isDirectory);
        knownfiles.push(entries[i]);
    }
}


function onGetDirectoryFail(error) {
    console.log("Error creating directory " + error.code);
}

//Result of reading my directory
function findImageinCache(entries) {
    // alert("The dir has "+entries.length+" entries.");
    for (var i = 0; i < News.NewsRes.News.length; i++) {
        News.NewsRes.News[i].src = "Images/logo.png";
        // News.NewsRes.News[i].src = "file:///storage/emulated/0//Android/data/com.gpspspa.imgs/myfile.jpg";
    }

    // alert("SAMP"+fullpath+entries[0].fullPath);
    for (var i = 0; i < entries.length; i++) {

        for (var j = 0; j < News.NewsRes.News.length; j++) {
            //if image is in localStorage
            //get image name and remove ext
            var imgNameArray = entries[i].name.split(".");
            var imgName = imgNameArray[0];

            // console.log("img name "+imgName);
            if (imgName == News.NewsRes.News[j].Id) {
                News.NewsRes.News[j].src = fullpath + entries[i].fullPath;
            }
        }
    }
    $("#NewsTemplate").tmpl(News.NewsRes.News).appendTo("#NewsListContent");

}

//Result of reading my directory
function findSingleImageinCache(entries, newsObjectId) {
    console.log("is img in cache ? " + newsObjectId);
    // alert("is img in cache ? " );
    // alert("is img in cache ? " +newsObject.Id);

    var src = "Images/logo.png";

    for (var i = 0; i < entries.length; i++) {
        //if image is in localStorage
        //get image name and remove ext
        var imgNameArray = entries[i].name.split(".");
        var imgName = imgNameArray[0];

        console.log("img name " + imgName);
        if (imgName == newsObjectId) {
            //if image already downloaded
            imageInCache = true;
            src = fullpath + entries[i].fullPath;
            break;
            // News.NewsRes.News[j].src = fullpath+entries[i].fullPath;
        }
    }

    $("#NewsDetailsImg").attr("src", src)


}
// function isImageinCache(entries,newsObject) {
//     // console.log("is img in cache ? " +newsObject.toSource());
//     // alert("is img in cache ? " );
//     // alert("is img in cache ? " +newsObject.Id);

//     var imageInCache=false;
//     for (var i=0; i<entries.length; i++) {
//             //if image is in localStorage
//             //get image name and remove ext
//             var imgNameArray = entries[i].name.split(".");
//             var imgName = imgNameArray[0];

//             console.log("img name "+imgName);
//             if(imgName == newsObject.Id){
//                 //if image already downloaded
//                 imageInCache=true;
//                 break;
//                     // News.NewsRes.News[j].src = fullpath+entries[i].fullPath;
//                 }
//             }
//             if(!imageInCache){
//                 console.log("Image in NOT in cache " );
//         //if not in cache..donwload             
//            //download img for caching
//                 //img ext
//                 var imgSrcAsString= newsObject.src;
//                 imgSrcAsString= imgSrcAsString[0];

//                 var extArray =imgSrcAsString.split(".");
//                 var ext = extArray[extArray.length - 1];

//                 var dlPath =  fullpath+ package_name+newsObject.Id +'.'+ext; //replace with NEWSID
//                 console.log("downloading crap to " + dlPath);
//                 var ft = new FileTransfer();

//                 ft.download(imgSrcAsString, dlPath, function(e){
//                     console.log("Successful download of "+e.fullPath);
//                 }, onError);
//                 //download img for caching


//             }else{
//                 //image in cache already
//                 console.log("Image in cache already" );


//             }
//         // $("#NewsTemplate").tmpl(News.NewsRes.News).appendTo("#NewsListContent");

//     }

//End of Image caching