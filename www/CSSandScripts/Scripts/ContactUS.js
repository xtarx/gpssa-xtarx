﻿var ContactUs = {

    GPSSAOfficeResp: null,
    MostRecentOffice:0,

    LoadContactUsPage: function () {
        if (GPSSAOfficeResponseGotten == false) {
            if (localStorage.getItem("GPSSAOffices" + Language) == null || localStorage.getItem("GPSSAOffices" + Language) == undefined) {
                var URL = Globals.ServicesURI_Test + "get/office/language/#Lang#";
                URL = URL.replace("#Lang#", Language);
                CallWebService(URL, 'ContactUs.HandleGetGPSSAOfficeCallSuccess(response)', "ContactUs.HandleGetGPSSAOfficeCallFailure()", "", "", true, false)
            }
            else {
                var response = localStorage.getItem("GPSSAOffices" + Language);
                ContactUs.GPSSAOfficeResp = JSON.parse(response);

                ContactUs.MostRecentOffice = Number(ContactUs.GPSSAOfficeResp.GPSSAOffices[0].Modified);

                for (var i = 0; i < ContactUs.GPSSAOfficeResp.GPSSAOffices.length; i++) {
                    var Temp = Number(ContactUs.GPSSAOfficeResp.GPSSAOffices[i].Modified);

                    if (ContactUs.MostRecentOffice > Temp) {
                        ContactUs.MostRecentOffice = Temp;
                    }
                }
                var URL = Globals.ServicesURI_Test + "validate/list/module/gpssaoffices/lastupdate/#LastUpdate#/language/#Lang#";
                URL = URL.replace("#LastUpdate#", NearestOffice.MostRecentOffice).replace("#Lang#", Language);
                CallWebService(URL, "ContactUs.HandleValidateOfficesCallSuccess(response)", "ContactUs.HandleValidateOfficesCallFailure()", "", "", true, false, false);
            }
        } else {
            ContactUs.GPSSAOfficeResp = GPSSAOfficeResponse;
            LoadPageContent("ContactUS", 'ContactUs.HandleContactUsDocReady()', true, true);
        }
    },

    HandleValidateOfficesCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("ContactUS", 'ContactUs.HandleContactUsDocReady()', true, true);
                }
                else {
                    ContactUs.GPSSAOfficeResp = null;
                    localStorage.removeItem("GPSSAOffices" + Language);
                    var URL = Globals.ServicesURI_Test + "get/office/language/#Lang#";
                    URL = URL.replace("#Lang#", Language);
                    CallWebService(URL, 'ContactUs.HandleGetGPSSAOfficeCallSuccess(response)', "ContactUs.HandleGetGPSSAOfficeCallFailure()", "", "", true, false);
                }
            }
        }
    },

    HandleValidateOfficesCallFailure: function () {
        var Resp = localStorage.getItem("GPSSAOffices" + Language);
        ContactUs.GPSSAOfficeResp = JSON.parse(Resp);;
        LoadPageContent("ContactUS", 'ContactUs.HandleContactUsDocReady()', false, true);
    },

    HandleGetGPSSAOfficeCallSuccess: function (response) {
        try {
            if (response != null) {
                var RespObj = response;//JSON.parse(response);
                if (RespObj.Message.Code == 0) {
                    localStorage.setItem("GPSSAOffices" + Language, JSON.stringify(response));
                    ContactUs.GPSSAOfficeResp = RespObj;
                    GPSSAOfficeResponse = ContactUs.GPSSAOfficeResp;
                    LoadPageContent("ContactUS", 'ContactUs.HandleContactUsDocReady()', false, true);
                } else {
                    GPSSAOfficeResponseGotten = false;
                    AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق")
                }
            }
        } catch (e) { }
    },

    HandleGetGPSSAOfficeCallFailure: function () {
        GPSSAOfficeResponseGotten = false
    },

    HandleContactUsDocReady: function () {
        SetHeaderTitle("Contact Us", "الاتصال بنا");
        $("#ContactUsContent").html("");
        $("#ContactUsTemplate").tmpl(ContactUs.GPSSAOfficeResp.GPSSAOffices).appendTo("#ContactUsContent");
    },

    OpenMapForAddress: function (Lat, Long,ID) {
        ID--;
        localStorage.setItem("PosObject", JSON.stringify( ContactUs.GPSSAOfficeResp.GPSSAOffices[ID]));
        CurrentView = "ContactUs";
        LoadPageContent("ContactusMap", 'ContactUs.HandleContactUsMapDocReady()', true, true);
    },

    HandleContactUsMapDocReady: function () {
        if (navigator.geolocation) {
            SetHeaderTitle("Contact Us", "اتصل بنا");
            document.getElementById('ATMframe').src = null;
            $("#ATMframe").height(DeviceHeight - 150);
            document.getElementById('ATMframe').src = "Templates/Map.html";
            localStorage.setItem("LatLang", "0#0");
         //  var options = { timeout: 6000 };
         //   navigator.geolocation.getCurrentPosition(ContactUs.GetPositionSuccess, ContactUs.GetPositionError,options);
        }
        else {
            AlertFunction("Geolocation is not supported by this browser.", "Geolocation is not supported by this browser.", "Error", "خطأ", "OK", "موافق");
            localStorage.setItem("LatLang", "0#0");
        }
      

    },

    GetPositionSuccess: function (pos) {
        try {
            Globals.MyLongitude = pos.coords.longitude;
            Globals.MyLatitude = pos.coords.latitude;
            localStorage.setItem("LatLang", Globals.MyLatitude + "#" + Globals.MyLongitude);
            document.getElementById('ATMframe').src = null;
            $("#ATMframe").height(DeviceHeight - 150);
            document.getElementById('ATMframe').src = "Templates/Map.html";
            
        } catch (e) {
        }
    },

    GetPositionError: function (err) {

       // AlertFunction(err.message, err.message, "Error", "خطأ", "OK", "موافق");
    }
};

function newWindow() {
    var myLatlng = new google.maps.LatLng(0.7, 40);
    var myOptions =
       {
           zoom: 2,
           center: myLatlng,
           mapTypeId: google.maps.MapTypeId.HYBRID
       };
    map = new google.maps.Map(document.getElementById("map_canvas"),
    myOptions);
}