﻿var AutoRegisteration = {
    NoOfTrails: 0,
    NoOfResendTrails: 0,
    EmployerRowID: '',
    EmployerName: '',
    Code: 0,
    AddGPSSAObj: null,
    IsValideUsername: false,
    IsValidEmail: false,

    LoadAutoRegistrationContent: function (isFromServices) {
        //if (localStorage.getItem("AutoRegisterationLock") == "true") {
        //    var LockDate = localStorage.getItem("AutoRegisterationLockDate");
        //    var Temp = Number(LockDate);
        //    Temp += 86400;
        //    var CurentDate = new Date();
        //    if (Temp > (CurentDate.getTime() / 1000)) {
        //        AlertFunction("Sorry this service is locked for 24 hours", "عفوا هذه الخدمة غير متاحة لمدة 24 ساعة", "Error", "خطأ", "OK", "موافق");
        //        return;
        //    }
        //    else {
        //        localStorage.setItem("AutoRegisterationLock", false);
        //        localStorage.setItem("AutoRegisterationLockDate", null);
        //    }
        //}
        if (isFromServices == true) {
            LoadPageContent("AutoRegisteration", 'AutoRegisteration.HandleAutoRegistrationDocReady()', true, true, true, true, false, 'ServicesList.LoadServicesPage()');
        }
        else {
            LoadPageContent("AutoRegisteration", 'AutoRegisteration.HandleAutoRegistrationDocReady()', true, true, true, true);
        }
    },

    HandleAutoRegistrationDocReady: function () {
        perepareDatePicker();
        SetHeaderTitle("Auto-Registration Wizard for Existing Employer", "تسجيل دخول صاحب عمل في بوابة الخدمات الالكترونية");
        BindEvents('#AutoRegisteration_NextBtn', 'click', 'AutoRegisteration.HandleAutoRegisterationNextBtnClicked()');
        AutoRegisteration.NoOfTrails = 0;
        AutoRegisteration.NoOfResendTrails = 0;
        AutoRegisteration.EmployerRowID = '';
        AutoRegisteration.EmployerName = '';
        AutoRegisteration.Code = 0;
        $("#AutoRegisterationStep2_EmiratesID").mask("05X-XXXXXXX");

        //$('#AutoRegisterationStep2_MobNo').mask("+XXX-XX-XXXXXXX", { autoclear: false });
        $('#AutoRegisterationStep2_MobNo').val("");

        //$('#AutoRegisterationStep2_LandLine').mask("+XXX-XX-XXXXXXX", { autoclear: false });
        $('#AutoRegisterationStep2_LandLine').val("");
    },

    HandleAutoRegisterationNextBtnClicked: function () {
        if ($("#AutoRegisteration_UserType").val() == "-1") {
            AlertFunction("Please select User Type", "من فضلك اختر نوع المستخدم", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#AutoRegisteration_AuthinticationCode").val()) == "") {
            AlertFunction("Please enter Authorization Code", "من فضلك ادخل كود الترخيص", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#AutoRegisteration_EmployerNumber").val()) == "") {
            AlertFunction("Please enter Employer Number", "من فضلك ادخل رقم صاحب العمل", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            var url = Globals.ServicesURI_Test + "validate/employer/authorizationcode/#Code#/employernumber/#EmpNo#/language/#Lang#";
            url = url.replace("#Code#", $("#AutoRegisteration_AuthinticationCode").val()).replace("#EmpNo#", $("#AutoRegisteration_EmployerNumber").val()).replace("#Lang#", Language);

            CallWebService(url, 'AutoRegisteration.HandleAuthinticationCallSuccess(response)', "AutoRegisteration.HandleAuthinticationCallFailure()", "", "", true, true);
        }
    },

    HandleAuthinticationCallSuccess: function (response) {
        try {
            if (response != null) {
                var RespObj = response;//JSON.parse(response);
                if (RespObj.Message != null) {
                    if (RespObj.Message.Code == 0) {
                        localStorage.setItem("AutoRegisterationLock", false);
                        localStorage.setItem("AutoRegisterationLockDate", null);
                        AutoRegisteration.EmployerName = RespObj.EmployerName;
                        AutoRegisteration.EmployerRowID = RespObj.EmployerRowId;
                        AutoRegisteration.AddGPSSAObj = GPSSAUser1;
                        AutoRegisteration.AddGPSSAObj.EmployerRowId = AutoRegisteration.EmployerRowID;
                        AutoRegisteration.LoadAutoRegisterationStep2Page();
                    }
                    else {
                        HideLoadingSpinner();
                        // AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                        AlertFunction("Wrong authorization code or employer number","خطأ في كود الترخيص أو رقم صاحب العمل" , "Error", "خطأ", "OK", "موافق");

                    }
                }
                else {
                    HideLoadingSpinner();
                    AlertFunction("There was an error in our system , please try again later", "حدث خطاء في النظام من فضلك اعد المحالولة لاحقا", "Error", "خطأ", "OK", "موافق");
                }
            }
        } catch (e) {
            Log(e, "HandleAuthinticationCallSuccess");
        }
    },

    LoadAutoRegisterationStep2Page: function () {
        LoadPageContent("AutoRegisterationStep2", 'AutoRegisteration.HandleAutoRegisterationStep2DocReady()', true, true, true, false, "AutoRegisteration.LoadAutoRegistrationContent()");
    },

    HandleAuthinticationCallFailure: function () {
        HideLoadingSpinner();
        AlertFunction("There was an error in our system , please try again later", "حدث خطاء في النظام من فضلك اعد المحالولة لاحقا", "Error", "خطأ", "OK", "موافق");
    },

    HandleAutoRegisterationStep2DocReady: function () {
        prepareDatePickerGeneric();
        BindEvents('#AutoRegisterationStep2_NextBtn', 'click', 'AutoRegisteration.HandleAutoRegisterationStep2_NextBtnClicked()');
        BindEvents('#AutoRegisterationStep2_BackBtn', 'click', 'AutoRegisteration.LoadAutoRegistrationContent()');

        $("#AutoRegisterationStep2_EmployerName").html(AutoRegisteration.EmployerName);
        $("#AutoRegisterationStep2_EmiratesID").mask("784-XXXX-XXXXXXX-X");

        //$('#AutoRegisterationStep2_MobNo').mask("+XXX-XX-XXXXXXX", { autoclear: false });
        $('#AutoRegisterationStep2_MobNo').val("");
        AddCountryCodesOptionsToDropDown("AutoReg2_Mobile_CountryCodeDDL");

        //$('#AutoRegisterationStep2_LandLine').mask("+XXX-XX-XXXXXXX", { autoclear: false });
        $('#AutoRegisterationStep2_LandLine').val("");
        AddCountryCodesOptionsToDropDown("AutoReg2_LandLine_CountryCodeDDL");

        $('#AutoRegisterationStep2_FirstName').on('input', function (e) {
            isArabic(AutoRegisterationStep2_FirstName);
        });
        $('#AutoRegisterationStep2_SecondName').on('input', function (e) {
            isArabic(AutoRegisterationStep2_SecondName);
        });
        $('#AutoRegisterationStep2_ThirdName').on('input', function (e) {
            isArabic(AutoRegisterationStep2_ThirdName);
        });
        $('#AutoRegisterationStep2_FourthName').on('input', function (e) {
            isArabic(AutoRegisterationStep2_FourthName);
        });
    },

    HandleAutoRegisterationStep2_NextBtnClicked: function () {
        var Mobile = GetFormatedPhoneNoValue("AutoReg2_Mobile_CountryCodeDDL", "AutoRegisterationStep2_MobNo");
        var LandLine = GetFormatedPhoneNoValue("AutoReg2_LandLine_CountryCodeDDL", "AutoRegisterationStep2_LandLine");

        if ($.trim($("#AutoRegisterationStep2_FirstName").val()) == "") {
            AlertFunction("Please enter First Name", "من فضلك ادخل الأسم الأول ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#AutoRegisterationStep2_SecondName").val()) == "") {
            AlertFunction("Please enter Second Name", "من فضلك ادخل الأسم الثانى ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#AutoRegisterationStep2_ThirdName").val()) == "") {
            AlertFunction("Please enter Third Name", "من فضلك ادخل الأسم الثالث ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#AutoRegisterationStep2_FourthName").val()) == "") {
            AlertFunction("Please enter Fourth Name", "من فضلك ادخل الأسم الرابع ", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (Mobile == "") {
            AlertFunction("Please enter valid Mobile Number", "من فضلك ادخل رقم هاتف محمول صحيح", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#AutoRegisterationStep2_email").val()) == "") {
            AlertFunction("Please enter E-mail", "من فضلك ادخل البريد الالكتروني", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (!ValidateEmail($("#AutoRegisterationStep2_email").val())) {
            AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صحيح", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (!ValidateEmail($("#AutoRegisterationStep2_email").val())) {
            AlertFunction("Please enter valid E-mail", "من فضلك ادخل البريد الإلكترونى صحيح", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if (AutoRegisteration.IsValidEmail == false) {
            AlertFunction("Email already exists", "البريد الإلكترونى موجود مسبقا", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else if ($.trim($("#AutoRegisterationStep2_BirthDate").val()) == "") {
            AlertFunction("Please enter birth date", "من فضلك ادخل تاربخ الميلاد", "Error", "خطأ", "OK", "موافق");
            return;
        } 

        else if ( ConvertDateToUNIX($("#AutoRegisterationStep2_BirthDate").val()) >= ConvertDateToUNIX(todaysDate())  ) {
            AlertFunction("Birth date must be less than today's date", "تاربخ الميلاد يجب ان يكون قبل تاريخ اليوم", "Error", "خطأ", "OK", "موافق");
            return;
        } 


        else if ($("#AutoRegisterationStep2_PreferredLang").val() == "-1") {
            AlertFunction("Please select preferred language", "من فضلك اختر اللغة المفضلة", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            AutoRegisteration.AddGPSSAObj.IsPrimaryResponsible = true;
            AutoRegisteration.AddGPSSAObj.EmiratesId = FormatChar($("#AutoRegisterationStep2_EmiratesID").val());
            AutoRegisteration.AddGPSSAObj.FirstName = $("#AutoRegisterationStep2_FirstName").val();
            AutoRegisteration.AddGPSSAObj.SecondName = $("#AutoRegisterationStep2_SecondName").val();
            AutoRegisteration.AddGPSSAObj.ThirdName = $("#AutoRegisterationStep2_ThirdName").val();
            AutoRegisteration.AddGPSSAObj.FourthName = $("#AutoRegisterationStep2_FourthName").val();

            AutoRegisteration.AddGPSSAObj.Mobile = Mobile;
            //alert("AutoRegisteration.AddGPSSAObj.Mobile >>" + AutoRegisteration.AddGPSSAObj.Mobile);

            AutoRegisteration.AddGPSSAObj.Email = $("#AutoRegisterationStep2_email").val();
            AutoRegisteration.AddGPSSAObj.BirthDate = ConvertDateToUNIX($("#AutoRegisterationStep2_BirthDate").val());

            AutoRegisteration.AddGPSSAObj.LandLine = LandLine;
            // alert("AutoRegisteration.AddGPSSAObj.LandLine >>" + AutoRegisteration.AddGPSSAObj.LandLine);

            AutoRegisteration.AddGPSSAObj.PreferredLanguage = $("#AutoRegisterationStep2_PreferredLang").val();

            if ($("#AutoRegisterationStep2_PreferredContactMethod").val() == "-1") {
                AutoRegisteration.AddGPSSAObj.PreferredContactMethod = "";
            }
            else {
                AutoRegisteration.AddGPSSAObj.PreferredContactMethod = $("#AutoRegisterationStep2_PreferredContactMethod").val();
            }
            //AutoRegisteration.AddGPSSAObj.Mobile = AutoRegisteration.AddGPSSAObj.Mobile.replace("+", "00");

            var url = Globals.ServicesURI_Test + "add/smsnotification/mobile/#mobile#/language/#Lang#";
            url = url.replace("#mobile#", cleanMobileNumber(AutoRegisteration.AddGPSSAObj.Mobile)).replace("#Lang#", Language);

            CallWebService(url, 'AutoRegisteration.HandleRegistrationStep2CallSuccess(response)', "", "", "", true, true);
        }
    },

    HandleRegistrationStep2CallSuccess: function (response) {
        if (response != null) {
            var RspObj = response;//JSON.parse(response);
            if (RspObj.Message != null) {
                if (RspObj.Message.Code == 0) {
                    AutoRegisteration.Code = RspObj.Code;
                    AutoRegisteration.LoadAutoRegisterationStep3Page();
                }
                else {
                    HideLoadingSpinner();
                    AlertFunction(RspObj.Message.Body, RspObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
            else {
                HideLoadingSpinner();
                AlertFunction("There was an error in our system , please try again later", "حدث خطاء في النظام من فضلك اعد المحالولة لاحقا", "Error", "خطأ", "OK", "موافق");
            }
        }
        else {
            HideLoadingSpinner();
            AlertFunction("There was an error in our system , please try again later", "حدث خطاء في النظام من فضلك اعد المحالولة لاحقا", "Error", "خطأ", "OK", "موافق");
        }
    },

    LoadAutoRegisterationStep3Page: function () {
        LoadPageContent("AutoRegisterationStep3", 'AutoRegisteration.HandleAutoRegisterationStep3DocReady()', true, true, true, false, 'AutoRegisteration.LoadAutoRegisterationStep2Page()');
    },

    HandleAutoRegisterationStep3DocReady: function () {
        perepareDatePicker();
        BindEvents('#AutoRegisterationStep3_NextBtn', 'click', 'AutoRegisteration.HandleAutoRegisterationStep3_NextBtnClicked()');
        BindEvents('#AutoRegisterationStep3_BackBtn', 'click', 'AutoRegisteration.LoadAutoRegisterationStep2Page()');
        BindEvents('#AutoRegisterationStep3_Resend', 'click', 'AutoRegisteration.HandleAutoRegisterationStep3_ResendBtnClicked()');
        // $("#AutoRegistrationCode").html(AutoRegisteration.Code);
    },

    HandleAutoRegisterationStep3_NextBtnClicked: function () {
        if ($.trim($("#AutoRegisterationStep3_Code").val()) == "") {
            AlertFunction("Please enter SMS Verification Code", "من فضلك ادخل رقم التأكيد", "Error", "خطأ", "OK", "موافق");
            return;
        }
        else {
            if (AutoRegisteration.Code == $.trim($("#AutoRegisterationStep3_Code").val())) {
                AutoRegisteration.LoadAutoRegisterationStep4Page();
            }
            else {
                //if (AutoRegisteration.NoOfTrails < 4) {
                //    AutoRegisteration.NoOfTrails += 1;
                AlertFunction("Sorry, Code not matched", "عفوا, كود غير متطابق", "Error", "خطأ", "OK", "موافق");
                //}
                //else {
                //                AutoRegisteration.LockAutoRegisterScreen();
                //    }
            }
        }
    },

    LoadAutoRegisterationStep4Page: function () {
        LoadPageContent("AutoRegisterationStep4", 'AutoRegisteration.HandleAutoRegisterationStep4DocReady()', true, true, true, false, "AutoRegisteration.LoadAutoRegisterationStep3Page()");
    },

    HandleAutoRegisterationStep3_ResendBtnClicked: function () {
        var url = Globals.ServicesURI_Test + "add/smsnotification/mobile/#mobile#/language/#Lang#";
        url = url.replace("#mobile#", cleanMobileNumber(AutoRegisteration.AddGPSSAObj.Mobile)).replace("#Lang#", Language);
        CallWebService(url, 'AutoRegisteration.HandleRegistrationStep3CallSuccess(response)', "", "", "", true, true);

    },

    HandleRegistrationStep3CallSuccess: function (response) {
        if (response != null) {
            var RspObj = response;//JSON.parse(response);

            if (RspObj.Message != null) {
                if (RspObj.Message.Code == 0) {
                    AutoRegisteration.Code = RspObj.Code;
                    AutoRegisteration.NoOfResendTrails += 1;
                    $("#AutoRegistrationCode").html(AutoRegisteration.Code);
                }
                else {
                    AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
        }
    },

    HandleAutoRegisterationStep4DocReady: function () {
        perepareDatePicker();
        BindEvents('#AutoRegisterationStep4_NextBtn', 'click', 'AutoRegisteration.HandleAutoRegistrationFinishBtnClicked()');
        BindEvents('#AutoRegisterationStep4_BackBtn', 'click', 'AutoRegisteration.LoadAutoRegisterationStep3Page()');

        $('#AutoRegisterationStep4_Password').pStrength({
            'changeBackground': false,
            'onPasswordStrengthChanged': function (passwordStrength, strengthPercentage) {
                if ($(this).val()) {
                    $.fn.pStrength('changeBackground', this, passwordStrength);
                } else {
                    $.fn.pStrength('resetStyle', this);
                }
                $('#' + $(this).data('display')).html(strengthPercentage + '%');
            },
            'onValidatePassword': function (strengthPercentage) {
                $('#' + $(this).data('display')).html(
                    $('#' + $(this).data('display')).html()
                    );
            }
        });
    },

    LockAutoRegisterScreen: function () {
        localStorage.setItem("AutoRegisterationLock", true);
        var currentdate = new Date();
        var LockDate = currentdate.getTime() / 1000;
        localStorage.setItem("AutoRegisterationLockDate", LockDate);
        AutoRegisteration.LoadAutoRegisterationErrorPage();
    },

    LoadAutoRegisterationErrorPage: function () {
        LoadPageContent("AutoRegisterationError", 'AutoRegisteration.HandleAutoRegisterationErrorDocReady()', true, true, true, true);
    },

    HandleAutoRegistrationFinishBtnClicked: function () {
        try {

            if ($.trim($("#AutoRegisterationStep4_Username").val()) == "") {
                AlertFunction("Please enter Username", "من فضلك ادخل اسم المستخدم", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (AutoRegisteration.IsValideUsername == false) {
                AlertFunction("Username already exists", "اسم المستخدم موجود مسبقا", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AutoRegisterationStep4_Password").val()) == "") {
                AlertFunction("Please enter Password", "من فضلك ادخل كلمة المرور", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AutoRegisterationStep4_ConfirmPassword").val()) == "") {
                AlertFunction("Please enter Password Confirmation", "من فضلك ادخل تأكيد كلمة المرور", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if ($.trim($("#AutoRegisterationStep4_ConfirmPassword").val()) != $.trim($("#AutoRegisterationStep4_Password").val())) {
                AlertFunction("Sorry Password not matched", "عفوا كلمة المرور غير متطابقة", "Error", "خطأ", "OK", "موافق");
                return;
            }
            else if (ValidatePassLength($.trim($("#AutoRegisterationStep4_ConfirmPassword").val()))) {
                AlertFunction("Password must be more than 7 charachter", "يجب ان تكون كلمه المرور اكبر من 7 حروف", "Error", "خطأ", "OK", "موافق");
                return;
            }

            else {

                AutoRegisteration.AddGPSSAObj.UserName = $("#AutoRegisterationStep4_Username").val();
                AutoRegisteration.AddGPSSAObj.Password = $("#AutoRegisterationStep4_Password").val();

                var URL = Globals.ServicesURI_Test + "add/gpssauser/language/#Lang#";
                URL = URL.replace("#Lang#", Language);

                var data = JSON.stringify(AutoRegisteration.AddGPSSAObj);

                Log(data, "Add GPSSA USer");

                CallWebServiceWithOauth(URL, 'AutoRegisteration.HandleAddGPSSAUserSuccess(response)', "", data, "POST", true, true);
            }
        } catch (e) {

        }
    },

    HandleAddGPSSAUserSuccess: function (response) {
        try {
            if (response != null) {
                Log(response.Message.Code, "response.Message.Code");
                if (response.Message.Code == 0) {
                    AutoRegisteration.CleanAutoRegistrationData();
                    var msg;
                    if (Language == 'en')
                        msg = "You have been registered successfully to GPSSA portal, you can now login to GPSSA website with the username & password you created.";
                    else
                        msg = "تم التسجيل بنجاح في موقع الهيئة، يمكنك الآن الدخول إلى موقع الهيئة مع اسم المستخدم و كلمة المرور التي تم تسجيلهم";
                    //    AlertFunction(response.Message.Body + "<br/> CaseNumber = " + response.CaseNumber, response.Message.Body + "<br/>" + " رقم الحالة = " + response.CaseNumber, "Success", "نجاح", "OK", "موافق", "Login.LoginWithAnotherUserClicked();");
                    AlertFunction(msg, msg, "Success", "نجاح", "OK", "موافق", "Login.LoginWithAnotherUserClicked();");

                }
                else {
                    HideLoadingSpinner();
                    AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
            else {
                HideLoadingSpinner();
                AlertFunction("There was an error in our system , please try again later", "حدث خطاء في النظام من فضلك اعد المحالولة لاحقا", "Error", "خطأ", "OK", "موافق");
            }
        } catch (e) {

        }
    },

    HandleAutoRegisterationErrorDocReady: function () {
        perepareDatePicker();
        BindEvents('#AutoRegisterationError_Home', 'click', 'AutoRegisteration.HandleAutoRegisterationError_HomeBtnClicked()');
        try {
            CleanPage('AutoRegisteration,AutoRegisterationStep2,AutoRegisterationStep3,AutoRegisterationStep4');
        } catch (e) {

        }
    },

    HandleAutoRegisterationError_HomeBtnClicked: function () {
        LoadPageContent("StartUp", 'StartUp.HandleStartUpDocReady()', true, true, true, true);
    },

    CleanAutoRegistrationData: function () {
        CleanPage('AutoRegisteration,AutoRegisterationStep2,AutoRegisterationStep3,AutoRegisterationStep4');
        AutoRegisteration.NoOfTrails = 0;
        AutoRegisteration.NoOfResendTrails = 0;
        AutoRegisteration.EmployerRowID = '';
        AutoRegisteration.EmployerName = '';
        AutoRegisteration.Code = 0;
        AutoRegisteration.AddGPSSAObj = null;
    },

    HandleValidateUsername: function () {
        // var URI = "http://mgovws.gpssa.ae:8080/SPREST.svc/validate/username/language/#Lang#";
        var URI = Globals.ServicesURI_Test+"validate/username/language/#Lang#";
        var I_username = $("#AutoRegisterationStep4_Username").val();

        if(!I_username){
            return;
        }
        I_username=I_username.toLowerCase();

        URI = URI.replace("#Lang#", Language);

        var data = {
            UserName: I_username
        };

        var ReqObj = JSON.stringify(data);
        CallWebService(URI, "AutoRegisteration.HandleUsernameValidationCallSuccess(response)", "AutoRegisteration.HandleUsernameValidationCallFailure()", ReqObj, "POST", true, true);
    },

    HandleUsernameValidationCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AutoRegisteration.IsValideUsername = response.IsValid;
                if (response.IsValid == false) {
                    AlertFunction("Username already exists", "اسم المستخدم موجود مسبقا", "Error", "خطأ", "OK", "موافق");
                }
            }
            else {
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
                AutoRegisteration.IsValideUsername = false;
            }
        }
        else {
            AutoRegisteration.IsValideUsername = false;
        }
    },

    HandleUsernameValidationCallFailure: function () {
        AutoRegisteration.IsValideUsername = false;
    },

    ValidateEmailInDB: function (id) {

        var email = $("#" + id).val();

        if(!email){
            return;
        }
        email=email.toLowerCase();        
        // var URI = "http://mgovws.gpssa.ae:8080/SPREST.svc/validate/email/language/#Lang#";
        var URI = Globals.ServicesURI_Test+"validate/email/language/#Lang#";
        URI = URI.replace("#Lang#", Language);

        var data = {
            Email: email
        };
        
        var ReqObj = JSON.stringify(data);
        CallWebService(URI, "AutoRegisteration.HandleValidateEmailCallSuccess(response)", "AutoRegisteration.HandleValidateEmailCallFailure()", ReqObj, "POST", true, true);
    },

    HandleValidateEmailCallSuccess: function (response) {
        if (response != null) {
            if (response.Message.Code == 0) {
                AutoRegisteration.IsValidEmail = response.IsValid;
                if (response.IsValid == false) {
                    AlertFunction("Email already exists", "البريد الإلكترونى موجود مسبقا", "Error", "خطأ", "OK", "موافق");
                }
            }
            else {
                AutoRegisteration.IsValidEmail = false;
                AlertFunction(response.Message.Body, response.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
        else {
            AutoRegisteration.IsValidEmail = false;
        }
    },

    HandleValidateEmailCallFailure: function () {
        AutoRegisteration.IsValidEmail = false;
    }
};
