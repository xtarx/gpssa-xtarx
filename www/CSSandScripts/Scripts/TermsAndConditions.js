﻿var Terms = {

    TermsResp:'',

    LoadTermsContent: function () {

        if (localStorage.getItem("Terms" + Language) == null || localStorage.getItem("Terms" + Language) == undefined) {
            var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
            URL = URL.replace("#PageName#", 'Terms').replace("#Lang#", Language);

            CallWebService(URL, "Terms.HandleTermsCallSuccess(response)", "", "", "", false, true);
        }
        else {
            var Resp = localStorage.getItem("Terms" + Language);
            var Obj = JSON.parse(Resp);
            Terms.TermsResp = Obj.Page;

            var URL = Globals.ServicesURI_Test + "validate/item/module/pages/itemid/#PageName#/lastupdate/#LastUpdate#/language/#Lang#";
            URL = URL.replace("#PageName#", 'Terms').replace("#LastUpdate#", Terms.TermsResp.Modified).replace("#Lang#", Language);
            CallWebService(URL, "Terms.HandleValidateTermsCallSuccess(response)", "Terms.HandleValidateTermsCallFailure()", "", "", true, false, false);
        }

        //LoadPageContent("TermsAndConditions", 'Terms.HandleTermsDocReady()', true, false);
    },

    HandleTermsDocReady: function () {

        SetHeaderTitle("Terms & Conditions", "الشروط والأحكام");
        $("#TermsContent").html(Terms.TermsResp.Content);
    },

    HandleValidateTermsCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                HideLoadingSpinner();
                if (RespObj.IsValid == true) {
                    LoadPageContent("TermsAndConditions", 'Terms.HandleTermsDocReady()', false, true);
                }
                else {
                    localStorage.removeItem("Terms" + Language);
                    var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
                    URL = URL.replace("#PageName#", 'Terms').replace("#Lang#", Language);
                    CallWebService(URL, "Terms.HandleTermsCallSuccess(response)", "", "", "", true, false);
                }
            }
        }
    },

    HandleValidateTermsCallFailure: function () {
        var Resp = localStorage.getItem("Terms" + Language);
        var obj = JSON.parse(Resp);
        Terms.TermsResp = obj.Page;
        LoadPageContent("TermsAndConditions", 'Terms.HandleTermsDocReady()', false, true);
    },


    HandleTermsCallSuccess: function (response) {
        try {
            if (response != null) {
                HideLoadingSpinner();
                var TermsResp = response;//JSON.parse(response);
                if (TermsResp.Message.Code == 0) {
                    localStorage.setItem("Terms" + Language, JSON.stringify(response));
                    $("#TermsContent").html(TermsResp.Page.Content);
                    $("#TermsContent").height($(".ContentClass").height() - 60);
                }
                else {
                    AlertFunction(TermsResp.Message.Body, TermsResp.Message.Body, "Error", "خطأ", "OK", "موافق");
                }
            }
        } catch (e) {
        }
    }
};