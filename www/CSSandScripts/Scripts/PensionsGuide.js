﻿var PensionsGuide = {
    PensionsResp: "",

    LoadPensionsGuideContent: function () {
       localStorage.removeItem("PensionRegulation" + Language);

       if (localStorage.getItem("PensionRegulation" + Language) == null || localStorage.getItem("PensionRegulation" + Language) == undefined) {
        var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
        URL = URL.replace("#PageName#", 'PensionRegulation').replace("#Lang#", Language);

        CallWebService(URL, "PensionsGuide.HandleGetPensionsCallSuccess(response)", "", "", "", true, false);
    }
    else {
        var Resp = localStorage.getItem("PensionRegulation" + Language);
        var Obj = JSON.parse(Resp);
        PensionsGuide.PensionsResp = Obj.Page;

        var URL = Globals.ServicesURI_Test + "validate/item/module/pages/itemid/#PageName#/lastupdate/#LastUpdate#/language/#Lang#";
        URL = URL.replace("#PageName#", 'Pension Regulation').replace("#LastUpdate#", PensionsGuide.PensionsResp.Modified).replace("#Lang#", Language);
        CallWebService(URL, "PensionsGuide.HandleValidatePensionsGuideCallSuccess(response)", "PensionsGuide.HandleValidatePensionsGuideCallFailure()", "", "", true, false, false);
    }
},

HandleValidatePensionsGuideCallSuccess: function (response) {
    if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                if (RespObj.IsValid == true) {
                    LoadPageContent("PensionsGuide", 'PensionsGuide.HandleLoadPensionsGuideDocReady()', false, true);
                }
                else {
                    localStorage.removeItem("PensionRegulation" + Language);
                    var URL = Globals.ServicesURI_Test + "get/page/pagename/#PageName#/language/#Lang#";
                    URL = URL.replace("#PageName#", 'PensionRegulation').replace("#Lang#", Language);

                    CallWebService(URL, "PensionsGuide.HandleGetPensionsCallSuccess(response)", "", "", "", true, false);
                }
            }
        }
    },

    HandleValidatePensionsGuideCallFailure: function () {
        var Resp = localStorage.getItem("PensionRegulation" + Language);
        var obj = JSON.parse(Resp);
        PensionsGuide.PensionsResp = obj.Page;
        LoadPageContent("PensionsGuide", 'PensionsGuide.HandleLoadPensionsGuideDocReady()', false, true);
    },

    HandleGetPensionsCallSuccess: function (response) {
        if (response != null) {
            var RespObj = response;//JSON.parse(response);
            if (RespObj.Message.Code == 0) {
                localStorage.setItem("PensionRegulation" + Language, JSON.stringify(response));
                PensionsGuide.PensionsResp = RespObj.Page;
                LoadPageContent("PensionsGuide", 'PensionsGuide.HandleLoadPensionsGuideDocReady()', false, true);
            }
            else {
                AlertFunction(RespObj.Message.Body, RespObj.Message.Body, "Error", "خطأ", "OK", "موافق");
            }
        }
    },

    HandleLoadPensionsGuideDocReady: function () {
        SetHeaderTitle("Pensions Guide", "دليل المعاشات");
        $("#PensionsGuide").html(PensionsGuide.PensionsResp.Content);
    }
};